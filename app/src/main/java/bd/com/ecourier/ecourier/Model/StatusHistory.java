package bd.com.ecourier.ecourier.Model;

/**
 * Created by User on 21-Jan-18.
 */

public class StatusHistory {

    private String time, status, updaterId, updaterGroup, shippingAgentId, shippingAgentGroup, branchId;

    public StatusHistory(String time, String status, String updaterId, String updaterGroup, String shippingAgentId, String shippingAgentGroup, String branchId) {

        this.branchId = branchId;
        this.shippingAgentGroup = shippingAgentGroup;
        this.shippingAgentId = shippingAgentId;
        this.status = status;
        this.time = time;
        this.updaterGroup = updaterGroup;
        this.updaterId = updaterId;

    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUpdaterId() {
        return updaterId;
    }

    public void setUpdaterId(String updaterId) {
        this.updaterId = updaterId;
    }

    public String getUpdaterGroup() {
        return updaterGroup;
    }

    public void setUpdaterGroup(String updaterGroup) {
        this.updaterGroup = updaterGroup;
    }

    public String getShippingAgentId() {
        return shippingAgentId;
    }

    public void setShippingAgentId(String shippingAgentId) {
        this.shippingAgentId = shippingAgentId;
    }

    public String getShippingAgentGroup() {
        return shippingAgentGroup;
    }

    public void setShippingAgentGroup(String shippingAgentGroup) {
        this.shippingAgentGroup = shippingAgentGroup;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }
}
