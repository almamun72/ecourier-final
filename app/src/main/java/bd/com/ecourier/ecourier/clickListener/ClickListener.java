package bd.com.ecourier.ecourier.clickListener;

import android.view.View;

/**
 * Created by User on 23-Dec-17.
 */

public interface ClickListener {

    public void onClick(View view, int position);

    public void onLongClick(View view, int position);

}