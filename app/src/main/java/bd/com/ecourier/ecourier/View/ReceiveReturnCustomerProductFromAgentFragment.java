package bd.com.ecourier.ecourier.View;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bd.com.ecourier.ecourier.API;
import bd.com.ecourier.ecourier.Config;
import bd.com.ecourier.ecourier.Model.AgentConsignments;
import bd.com.ecourier.ecourier.Model.CustomerProduct;
import bd.com.ecourier.ecourier.Model.ManagerReceiveParcel;
import bd.com.ecourier.ecourier.Model.ParcelHoldCancel;
import bd.com.ecourier.ecourier.Presenter.CustomerProductListAdapter;
import bd.com.ecourier.ecourier.R;
import bd.com.ecourier.ecourier.clickListener.ClickListener;
import bd.com.ecourier.ecourier.clickListener.RecyclerTouchListener;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static android.graphics.Paint.ANTI_ALIAS_FLAG;

/**
 * Created by User on 11-Dec-17.
 */

public class ReceiveReturnCustomerProductFromAgentFragment extends Fragment {

    @BindView(R.id.swipe_down_to_refresh_list)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.recyclerView_list)
    RecyclerView recyclerView;
    @BindView(R.id.fab_scan)
    FloatingActionButton QRScan;
    @BindView(R.id.recyclerView_list_notice)
    TextView noListNotice;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    int i = 0;
    private CustomerProductListAdapter customerProductListAdapter;
    private View rootView;
    private ArrayList<String> qrScanCodes = new ArrayList<>();
    private ArrayList<String> consignments = new ArrayList<>();
    private String agentName;
    private ArrayList<CustomerProduct> customerProductArrayListForServer = new ArrayList<>();
    private ArrayList<CustomerProduct> customerProductArrayList;
    private ArrayList<String> customerProductECRListForServer = new ArrayList<>();
    private CustomerProduct customerProduct;
    private Gson gson = new Gson();
    private String accept = "Number of item : ";
    private IntentIntegrator intentIntegrator;

    public ReceiveReturnCustomerProductFromAgentFragment() {
    }

    public static Bitmap textAsBitmap(String text, float textSize, int textColor) {
        Paint paint = new Paint(ANTI_ALIAS_FLAG);
        paint.setTextSize(textSize);
        paint.setColor(textColor);
        paint.setTextAlign(Paint.Align.LEFT);
        float baseline = -paint.ascent(); // ascent() is negative
        int width = (int) (paint.measureText(text) + 0.0f); // round
        int height = (int) (baseline + paint.descent() + 0.0f);
        Bitmap image = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(image);
        canvas.drawText(text, 0, baseline, paint);
        return image;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {

            consignments = getArguments().getStringArrayList(API.KEY_JSON_CONSIGNMENTS);
            agentName = getArguments().getString(API.KEY_AGENT_DETAIL_AGENT_NAME);

        }

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.recycle_view_list_layout, container, false);
        ButterKnife.bind(this, rootView);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                if (consignments.size() > 0) {

                    customerProductListAdapter.clearList();
                    fetchListData();
                    displayAgentDetail();

                } else {
                    swipeRefreshLayout.setRefreshing(false);
                }

            }
        });

        displayAgentDetail();

        customerProductArrayList = new ArrayList<>();

        showFeed(customerProductArrayList);
        recyclerView.setVisibility(View.GONE);
        noListNotice.setVisibility(View.VISIBLE);

        if (getContext() != null)
            if (((MainActivity) getContext()).connectedTOInternet)
                fetchListData();

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, final int position) {

                customerProduct = customerProductArrayList.get(position);
                ImageView checkBox = view.findViewById(R.id.product_select_checkbox);
                ImageView cancelRequestedItem = view.findViewById(R.id.cancel_request);

                checkBox.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {

                        selectMerchantItem();
                        customerProductListAdapter.notifyItemChanged(position);

                    }
                });

                cancelRequestedItem.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        final Activity activity = getActivity();

                        if (activity != null) {

                            final AlertDialog.Builder builder1 = new AlertDialog.Builder(activity);
                            builder1.setCancelable(false);

                            LayoutInflater inflater = activity.getLayoutInflater();
                            View alertView = inflater.inflate(R.layout.select_spinner, null);
                            builder1.setView(alertView);

                            TextView title = (TextView) alertView.findViewById(R.id.alert_title);
                            title.setText(activity.getResources().getText(R.string.way_cancel_product));

                            final Spinner commentSpinner = alertView.findViewById(R.id.list_spinner);
                            commentSpinner.setVisibility(View.GONE);

                            builder1.setPositiveButton(
                                    activity.getResources().getString(R.string.yes),
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {


                                        }
                                    }).setNegativeButton(activity.getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // User cancelled the dialog
                                    dialog.cancel();
                                }
                            });

                            final AlertDialog alert11 = builder1.create();

                            alert11.setOnShowListener(new DialogInterface.OnShowListener() {
                                @Override
                                public void onShow(DialogInterface dialogInterface) {
                                    alert11.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.RED);
                                    alert11.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(activity.getResources().getColor(R.color.colorPrimary));
                                    alert11.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {


                                            ArrayList<String> temp = new ArrayList<>();

                                            temp.add(customerProduct.getECRNumber());
                                            ParcelHoldCancel parcelHoldCancel;
                                            parcelHoldCancel = new ParcelHoldCancel(temp, customerProduct.getLastState(), "");
                                            Gson gson = new Gson();
                                            updateStatus(gson.toJson(parcelHoldCancel), position);

                                            alert11.cancel();


                                        }
                                    });

                                }
                            });

                            alert11.show();

                        }

                    }
                });

            }

            @Override
            public void onLongClick(View view, int position) {

                /*customerProduct = customerProductArrayList.get(position);

                selectMerchantItem();
                customerProductListAdapter.notifyItemChanged(position);*/

            }
        }));

        intentIntegrator = IntentIntegrator.forSupportFragment(this);
        intentIntegrator.setCaptureLayout(R.layout.barcode_scanner);
        intentIntegrator.setResultDisplayDuration(0);

        return rootView;
    }

    private void displayAgentDetail() {

        if (getActivity() != null) {

            try {

                String data = getResources().getString(R.string.receive_return) + " " + agentName + " " + "(" + consignments.size() + ")";
                noListNotice.setText(data);

            } catch (Exception e) {

                Log.e("Error: ", " " + e);

            }


        }

    }

    private void selectMerchantItem() {

        customerProduct.setChecked(!customerProduct.isChecked());
        if (customerProduct.isChecked())
            customerProductArrayListForServer.add(customerProduct);
        else
            customerProductArrayListForServer.remove(customerProduct);

        if (customerProductArrayListForServer.size() == 0) {

            QRScan.setImageResource(R.drawable.cycle);

        } else {

            QRScan.setImageBitmap(textAsBitmap("" + customerProductArrayListForServer.size(), 40, Color.WHITE));

        }
    }

    private void fetchListData() {

        QRScan.setImageResource(R.drawable.cycle);
        customerProductArrayListForServer.clear();
        customerProductECRListForServer.clear();

        progressBar.setVisibility(View.VISIBLE);

        AgentConsignments agentConsignments;

        agentConsignments = new AgentConsignments(consignments);
        Gson gson = new Gson();
        final String json = gson.toJson(agentConsignments);

        RequestQueue queue = null;
        // Instantiate the RequestQueue.
        if (getContext() != null)
            queue = Volley.newRequestQueue(getContext());
        String url = API.baseUrl + API.managerParcelDetail;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        progressBar.setVisibility(View.GONE);

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String code = jsonObject.getString(API.KEY_CODE);

                            if (code.equals(API.VALUE_CODE_SUCCESS)) {

                                customerProductListAdapter.clearList();
                                customerProductListAdapter.listUpdated(dataFromServer(jsonObject));
                                recyclerView.setVisibility(View.VISIBLE);
                                swipeRefreshLayout.setRefreshing(false);
                                displayAgentDetail();

                            } else {

                                JSONObject error = jsonObject.getJSONObject(API.KEY_JSON_ERROR);

                                if (code.equals(API.VALUE_CODE_AUTHENTICATION_ERROR)) {

                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                } else if (code.equals(API.VALUE_CODE_PARAMETER_ERROR)) {
                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                swipeRefreshLayout.setRefreshing(false);

                try {

                    if (getContext() != null) {

                        if (error.getMessage().equals("com.android.volley.TimeoutError"))
                            Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_timeout), Toast.LENGTH_SHORT).show();

                        Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_unreachable), Toast.LENGTH_SHORT).show();
                    }

                    Log.v("Volly error : ", "" + error);

                } catch (Exception e) {

                    Log.v("Receive Return E : ", " " + e);

                }
            }

        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                if (getContext() != null) {
                    params.put(API.KEY_JSON_USER_AUTHENTICATION, ((MainActivity) getContext()).authenticationKey);
                    params.put(API.KEY_JSON_USER_GROUP, ((MainActivity) getContext()).userGroup);
                    params.put(API.KEY_USER_ID, ((MainActivity) getContext()).userID);
                    params.put(API.KEY_CONTENT_TYPE, API.VALUE_CONTENT_VALUE_JSON);
                }

                return params;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                String your_string_json = json; // put your json
                return your_string_json.getBytes();
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                Config.volley_time_out,
                Config.max_retries,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Add the request to the RequestQueue.
        if (queue != null)
            queue.add(stringRequest);

    }

    private CustomerProduct[] dataFromServer(JSONObject jsonObject) throws JSONException {

        JSONArray jsonArray = jsonObject.getJSONArray(API.KEY_JSON_DATA);

        CustomerProduct[] customerProducts = new CustomerProduct[jsonArray.length()];

        for (int i = 0; i < jsonArray.length(); i++) {

            JSONObject customerProduct = jsonArray.getJSONObject(i);

            customerProducts[i] = new CustomerProduct(customerProduct.getString(API.KEY_CUSTOMER_PRODUCT_ECR_NUMBER_LOWER_CASE), customerProduct.getString(API.KEY_CUSTOMER_PRODUCT_CUSTOMER_NAME), customerProduct.getString(API.KEY_CUSTOMER_PRODUCT_CUSTOMER_ADDRESS), customerProduct.getString(API.KEY_CUSTOMER_PRODUCT_PRICE), customerProduct.getString(API.KEY_CUSTOMER_PRODUCT_MERCHANT_NAME), customerProduct.getString(API.KEY_CUSTOMER_PRODUCT_CUSTOMER_PHONE), customerProduct.getString(API.KEY_CUSTOMER_PRODUCT_MERCHANT_PHONE));

            customerProducts[i].setLastState(customerProduct.getJSONArray(API.KEY_MERCHANT_PRODUCT_STATUS_HISTORY).getJSONObject(1).getString(API.KEY_JSON_STATUS));

        }

        return customerProducts;
    }

    private void showFeed(ArrayList<CustomerProduct> customerProductArrayList) {

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        customerProductListAdapter = new CustomerProductListAdapter(getActivity(), customerProductArrayList, Config.TAG_RECEIVE_RETURN_CUSTOMER_PRODUCT_FROM_AGENT_FRAGMENT, consignments);
        recyclerView.setAdapter(customerProductListAdapter);

    }

    @OnClick(R.id.fab_scan)
    public void onClick(View view) {

        if (customerProductArrayListForServer.size() == 0) {
            intentIntegrator.addExtra("PROMPT_MESSAGE", "Total : " + qrScanCodes.size());
            intentIntegrator.initiateScan();
        } else if (customerProductArrayListForServer.size() > 0) {
            customerProductECRListForServer.clear();
            for (int i = 0; i < customerProductArrayListForServer.size(); i++) {

                customerProductECRListForServer.add(customerProductArrayListForServer.get(i).getECRNumber());

            }

            if (getContext() != null) {

                AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                builder1.setMessage(accept + customerProductArrayListForServer.size());
                builder1.setCancelable(false);

                builder1.setPositiveButton(
                        "CONFIRM",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                            }
                        }).setNegativeButton("BACK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                        customerProductECRListForServer.clear();
                        dialog.cancel();
                    }
                });

                final AlertDialog alert11 = builder1.create();
                alert11.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface arg0) {
                        alert11.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.RED);
                        alert11.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getContext().getResources().getColor(R.color.colorPrimary));

                        alert11.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                ManagerReceiveParcel managerReceiveParcel = new ManagerReceiveParcel(customerProductECRListForServer, API.STATUS_MANAGER_RECEIVE_AGENT_RETURN);

                                managerReceive(gson.toJson(managerReceiveParcel));
                                alert11.dismiss();

                            }
                        });

                    }
                });

                alert11.show();

            }

        }

    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {

        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (scanResult != null) {

            if (resultCode == RESULT_OK) {
                // contents contains whatever the code was
                String contents = scanResult.getContents();

                // Format contains the type of code i.e. UPC, EAN, QRCode etc...
                //String format = intent.getStringExtra("SCAN_RESULT_FORMAT");

                // Handle successful scan. In this example add contents to ArrayList
                if (contents != null) {
                    for (int i = 0; i < customerProductArrayList.size(); i++) {

                        if (customerProductArrayList.get(i).getECRNumber().equals(contents)) {

                            if (customerProductArrayListForServer.size() > 0) {

                                Boolean found = false;

                                for (int j = 0; j < customerProductArrayListForServer.size(); j++) {

                                    if (customerProductArrayListForServer.get(j).getECRNumber().equals(contents)) {

                                        found = true;
                                        break;

                                    }

                                }

                                if (!found) {

                                    customerProduct = customerProductArrayList.get(i);
                                    selectMerchantItem();
                                    customerProductListAdapter.notifyItemChanged(i);
                                    qrScanCodes.add(contents);

                                }

                            } else {

                                customerProduct = customerProductArrayList.get(i);
                                selectMerchantItem();
                                customerProductListAdapter.notifyItemChanged(i);
                                qrScanCodes.add(contents);

                            }

                            break;

                        }

                    }

                }

                intentIntegrator.addExtra("PROMPT_MESSAGE", "Total : " + qrScanCodes.size());
                intentIntegrator.initiateScan();

            } else if (resultCode == RESULT_CANCELED) {

                qrScanCodes.clear();

            }


        } else {
            Toast toast = Toast.makeText(getContext(),
                    "No scan data received!", Toast.LENGTH_SHORT);
            toast.show();
        }

    }

    private void managerReceive(final String data) {

        final ProgressDialog dialog = ProgressDialog.show(getActivity(), "",
                getActivity().getResources().getString(R.string.loading), true);

        dialog.show();

        RequestQueue queue = null;
        // Instantiate the RequestQueue.
        if (getContext() != null)
            queue = Volley.newRequestQueue(getContext());
        String url = API.baseUrl + API.managerParcelUpdate;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        swipeRefreshLayout.setRefreshing(false);
                        dialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String code = jsonObject.getString(API.KEY_CODE);

                            if (code.equals(API.VALUE_CODE_SUCCESS)) {

                                JSONObject data = jsonObject.getJSONObject(API.KEY_JSON_DATA);

                                JSONArray updated = data.getJSONArray(API.KEY_JSON_UPDATED);
                                JSONArray failed = data.getJSONArray(API.KEY_JSON_FAILED);

                                if (updated.length() > 0) {
                                    Toast.makeText(getContext(), "SUCCESS", Toast.LENGTH_SHORT).show();

                                    for (int i = 0; i < customerProductArrayListForServer.size(); i++) {

                                        customerProductArrayList.remove(customerProductArrayListForServer.get(i));
                                        consignments.remove(customerProductArrayListForServer.get(i).getECRNumber());
                                        customerProductListAdapter.notifyDataSetChanged();
                                        displayAgentDetail();
                                        fetchListData();

                                    }
                                    QRScan.setImageResource(R.drawable.cycle);
                                    customerProductArrayListForServer.clear();
                                    customerProductECRListForServer.clear();
                                } else if (failed.length() > 0) {
                                    Toast.makeText(getContext(), "FAILED", Toast.LENGTH_SHORT).show();

                                }

                            } else {

                                JSONObject error = jsonObject.getJSONObject(API.KEY_JSON_ERROR);

                                if (code.equals(API.VALUE_CODE_AUTHENTICATION_ERROR)) {

                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                } else if (code.equals(API.VALUE_CODE_PARAMETER_ERROR)) {
                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (Exception e) {

                            Log.e("Receive return: ", " " + e);

                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                swipeRefreshLayout.setRefreshing(false);
                dialog.dismiss();
                try {

                    if (getContext() != null) {

                        if (error.getMessage().equals("com.android.volley.TimeoutError"))
                            Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_timeout), Toast.LENGTH_SHORT).show();

                        Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_unreachable), Toast.LENGTH_SHORT).show();
                    }

                    Log.v("Volly error : ", "" + error);

                } catch (Exception e) {

                    Log.v("Receive Return E : ", " " + e);

                }
            }

        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                if (getContext() != null) {

                    params.put(API.KEY_JSON_USER_AUTHENTICATION, ((MainActivity) getContext()).authenticationKey);
                    params.put(API.KEY_JSON_USER_GROUP, ((MainActivity) getContext()).userGroup);
                    params.put(API.KEY_USER_ID, ((MainActivity) getContext()).userID);
                    params.put(API.KEY_CONTENT_TYPE, API.VALUE_CONTENT_VALUE_JSON);

                }

                return params;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                String your_string_json = data; // put your json
                return your_string_json.getBytes();
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                Config.volley_time_out,
                Config.max_retries,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Add the request to the RequestQueue.
        if (queue != null)
            queue.add(stringRequest);

    }

    private void updateStatus(final String data, final int position) {

        final ProgressDialog dialog = ProgressDialog.show(getActivity(), "",
                getActivity().getResources().getString(R.string.loading), true);

        dialog.show();

        final Activity activity = getActivity();

        RequestQueue queue = null;
        // Instantiate the RequestQueue.
        if (activity != null)
            queue = Volley.newRequestQueue(activity);
        String url = API.baseUrl + API.agentParcelUpdate;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        swipeRefreshLayout.setRefreshing(false);
                        dialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String code = jsonObject.getString(API.KEY_CODE);

                            if (code.equals(API.VALUE_CODE_SUCCESS)) {

                                JSONObject data = jsonObject.getJSONObject(API.KEY_JSON_DATA);

                                JSONArray updated = data.getJSONArray(API.KEY_JSON_UPDATED);
                                JSONArray rejected = data.getJSONArray(API.KEY_JSON_REJECTED);
                                JSONArray failed = data.getJSONArray(API.KEY_JSON_FAILED);

                                if (updated.length() > 0) {
                                    Toast.makeText(activity, "SUCCESS", Toast.LENGTH_SHORT).show();
                                    customerProductArrayList.remove(position);
                                    customerProductListAdapter.notifyDataSetChanged();
                                    consignments.remove(position);
                                    displayAgentDetail();
                                    if (consignments.size() > 0)
                                        fetchListData();
                                    else {
                                        QRScan.setImageResource(R.drawable.cycle);
                                        customerProductArrayListForServer.clear();
                                        customerProductECRListForServer.clear();
                                    }

                                } else if (rejected.length() > 0) {
                                    Toast.makeText(activity, " " + rejected.getJSONObject(0).getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_SHORT).show();
                                } else if (failed.length() > 0) {
                                    Toast.makeText(activity, " " + failed.getJSONObject(0).getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_SHORT).show();

                                }

                            } else {

                                JSONObject error = jsonObject.getJSONObject(API.KEY_JSON_ERROR);

                                if (code.equals(API.VALUE_CODE_AUTHENTICATION_ERROR)) {

                                    Toast.makeText(activity, "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                } else if (code.equals(API.VALUE_CODE_PARAMETER_ERROR)) {
                                    Toast.makeText(activity, "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (Exception e) {

                            Log.e("Rec return: ", " " + e);

                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                swipeRefreshLayout.setRefreshing(false);
                dialog.dismiss();
                try {

                    if (activity != null) {

                        if (error.getMessage().equals("com.android.volley.TimeoutError"))
                            Toast.makeText(activity, activity.getResources().getString(R.string.server_timeout), Toast.LENGTH_SHORT).show();

                        Toast.makeText(activity, activity.getResources().getString(R.string.server_unreachable), Toast.LENGTH_SHORT).show();
                    }

                    Log.v("Volly error : ", "" + error);

                } catch (Exception e) {

                    Log.v("Distribute Way E : ", " " + e);

                }

            }

        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                if (activity != null) {

                    params.put(API.KEY_JSON_USER_AUTHENTICATION, ((MainActivity) activity).authenticationKey);
                    params.put(API.KEY_JSON_USER_GROUP, ((MainActivity) activity).userGroup);
                    params.put(API.KEY_USER_ID, ((MainActivity) activity).userID);
                    params.put(API.KEY_CONTENT_TYPE, API.VALUE_CONTENT_VALUE_JSON);

                }

                return params;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                String your_string_json = data; // put your json
                return your_string_json.getBytes();
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                Config.volley_time_out,
                Config.max_retries,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Add the request to the RequestQueue.
        if (queue != null)
            queue.add(stringRequest);

    }

}
