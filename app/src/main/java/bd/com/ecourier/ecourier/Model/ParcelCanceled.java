package bd.com.ecourier.ecourier.Model;

import java.util.ArrayList;

/**
 * Created by User on 26-Dec-17.
 */

public class ParcelCanceled {

    private String status, collected_amount, comments;
    private ArrayList<String> consignment_no;

    public ParcelCanceled(ArrayList<String> consignment_no, String status, String collected_amount, String comments) {

        this.consignment_no = consignment_no;
        this.status = status;
        this.collected_amount = collected_amount;
        this.comments = comments;

    }

    public ArrayList<String> getConsignment_no() {
        return consignment_no;
    }

    public void setConsignment_no(ArrayList<String> consignment_no) {
        this.consignment_no = consignment_no;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCollected_amount() {
        return collected_amount;
    }

    public void setCollected_amount(String collected_amount) {
        this.collected_amount = collected_amount;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }
}
