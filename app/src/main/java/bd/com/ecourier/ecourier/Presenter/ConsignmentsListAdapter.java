package bd.com.ecourier.ecourier.Presenter;

import android.app.Activity;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;

import bd.com.ecourier.ecourier.Model.Consignments;
import bd.com.ecourier.ecourier.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by User on 11-Dec-17.
 */

public class ConsignmentsListAdapter extends RecyclerView.Adapter<ConsignmentsListAdapter.ConsignmentsListAdapterHolder> {

    private LayoutInflater inflater;
    private Activity activity;
    private ArrayList<Consignments> consignmentsArrayList;
    private String fragmentTag, userType;

    public ConsignmentsListAdapter(Activity activity, ArrayList<Consignments> consignmentsArrayList, String fragmentTag) {

        inflater = LayoutInflater.from(activity);
        this.activity = activity;
        this.consignmentsArrayList = consignmentsArrayList;
        this.fragmentTag = fragmentTag;

    }

    @Override
    public ConsignmentsListAdapterHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ConsignmentsListAdapterHolder(inflater.inflate(R.layout.consignment, parent, false));
    }

    @Override
    public void onBindViewHolder(ConsignmentsListAdapterHolder holder, int position) {

        Consignments consignments = consignmentsArrayList.get(position);

        holder.ecrNumber.setText(consignments.getConsignment_no());
        holder.ecrNumber.setTextColor(consignments.getTextColor());

        if (consignments.isChecked()) {
            holder.checkBox.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.ic_check_box_black_24dp));
        } else {
            holder.checkBox.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.ic_check_box_outline_blank_black_24dp));
        }

        holder.backGround.setBackgroundColor(consignments.getColor());

    }

    @Override
    public int getItemCount() {
        return consignmentsArrayList.size();
    }

    public void listUpdated(Consignments[] consignments) {

        if (consignments.length > 0) {

            Collections.addAll(consignmentsArrayList, consignments);
            notifyDataSetChanged();
        }

    }

    public void clearList() {

        consignmentsArrayList.clear();
        notifyDataSetChanged();

    }

    class ConsignmentsListAdapterHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.ecr_number)
        TextView ecrNumber;
        @BindView(R.id.product_select_checkbox)
        ImageView checkBox;
        @BindView(R.id.linear_layout)
        LinearLayout backGround;

        ConsignmentsListAdapterHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick({R.id.product_select_checkbox})
        public void onClick(View view) {
        }

    }
}
