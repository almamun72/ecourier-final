package bd.com.ecourier.ecourier.Presenter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;

import bd.com.ecourier.ecourier.Config;
import bd.com.ecourier.ecourier.R;
import bd.com.ecourier.ecourier.View.ReceiveCancelFragment;
import bd.com.ecourier.ecourier.View.ReceiveCashFragment;
import bd.com.ecourier.ecourier.View.ReceiveDeliveryForAgentFragment;
import bd.com.ecourier.ecourier.View.ReceiveFulfillmentFragment;
import bd.com.ecourier.ecourier.View.ReceiveHoldFragment;
import bd.com.ecourier.ecourier.View.ReceiveReturnForAgentFragment;
import bd.com.ecourier.ecourier.View.ReceiveTransferForAgentFragment;

/**
 * Created by User on 17-Dec-17.
 */

public class ReceiveTabAdapter extends android.support.v4.app.FragmentPagerAdapter {

    public int fulfillmentCount, holdCount, cancelCount, returnCount, deliveryCount, transferCount, cash;
    private Context context;
    private String userType;

    public ReceiveTabAdapter(Context context, FragmentManager fm, String userType) {
        super(fm);
        this.context = context;
        this.userType = userType;

        fulfillmentCount = 0;
        holdCount = 0;
        cancelCount = 0;
        returnCount = 0;
        deliveryCount = 0;
        transferCount = 0;
        cash = 0;

    }

    @Override
    public Fragment getItem(int position) {

        if (userType.equals(Config.USER_TYPE_MANAGER)) {

            if (position == 0) {
                return new ReceiveFulfillmentFragment();
            }/* else if (position == 1) {
                return new ReceiveReturnFragment();
            }*/ else if (position == 1) {
                return new ReceiveHoldFragment();
            } else if (position == 2) {
                return new ReceiveCancelFragment();
            } else {
                return new ReceiveCashFragment();
            }

        } else {

            if (position == 0) {
                return new ReceiveDeliveryForAgentFragment();
            } else if (position == 1) {
                return new ReceiveReturnForAgentFragment();
            } else {
                return new ReceiveTransferForAgentFragment();
            }

        }

    }

    @Override
    public CharSequence getPageTitle(int position) {

        if (userType.equals(Config.USER_TYPE_MANAGER)) {

            if (position == 0) {
                return context.getString(R.string.receive_fulfillment) + " (" + fulfillmentCount + ")";
            } /*else if (position == 1) {
                return context.getString(R.string.receive_return) + " (" + returnCount + ")";
            }*/ else if (position == 1) {
                return context.getString(R.string.receive_hold) + " (" + holdCount + ")";
            } else if (position == 2) {
                return context.getString(R.string.receive_cancel) + " (" + cancelCount + ")";
            } else {
                return context.getString(R.string.receive_cash) + " (" + cash + ")";
            }

        } else {

            if (position == 0) {
                return context.getString(R.string.receive_delivery) + " (" + deliveryCount + ")";
            } else if (position == 1) {
                return context.getString(R.string.receive_return) + " (" + returnCount + ")";
            } else {
                return context.getString(R.string.receive_transfer) + " (" + transferCount + ")";
            }

        }


    }

    @Override
    public int getCount() {
        try {

            if (userType.equals(Config.USER_TYPE_MANAGER))
                return 4;
            else
                return 3;

        } catch (Exception e) {

            Log.e("Receive Tab: ", " " + e);

        }

        return 0;

    }

}
