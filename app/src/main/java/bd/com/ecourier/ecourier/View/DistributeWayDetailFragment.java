package bd.com.ecourier.ecourier.View;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bd.com.ecourier.ecourier.API;
import bd.com.ecourier.ecourier.Config;
import bd.com.ecourier.ecourier.Model.AgentConsignments;
import bd.com.ecourier.ecourier.Model.CustomerProduct;
import bd.com.ecourier.ecourier.Model.ParcelCanceled;
import bd.com.ecourier.ecourier.Model.ParcelDeliveredPartialDelivered;
import bd.com.ecourier.ecourier.Model.ParcelHoldCancel;
import bd.com.ecourier.ecourier.Model.ParcelTransfer;
import bd.com.ecourier.ecourier.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by User on 10-Dec-17.
 */

public class DistributeWayDetailFragment extends Fragment {

    @BindView(R.id.product_ecr_number)
    TextView ecrNumber;
    @BindView(R.id.product_customer_name)
    TextView customerName;
    @BindView(R.id.customer_address)
    TextView customerAddress;
    @BindView(R.id.product_price)
    TextView productPrice;
    @BindView(R.id.merchant_name)
    TextView merchantName;
    @BindView(R.id.no_of_items)
    TextView noOfItems;
    @BindView(R.id.button_holder)
    LinearLayout buttonHolder;
    @BindView(R.id.indeterminateBar)
    LinearLayout indeterminateBar;
    @BindView(R.id.hold_btn)
    LinearLayout holdBtn;

    private View rootView;
    private String json;

    private ArrayList<String> branchName, branchId;
    private ArrayList<ArrayList<String>> agentName, agentID;

    public DistributeWayDetailFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AgentConsignments agentConsignments;
        Gson gson = new Gson();

        if (getArguments() != null) {

            ArrayList<String> temp = new ArrayList<>();
            temp.add(getArguments().getString(API.KEY_JSON_CONSIGNMENTS));
            agentConsignments = new AgentConsignments(temp);
            json = gson.toJson(agentConsignments);

        }

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.agent_way_detail_fragment, container, false);
        ButterKnife.bind(this, rootView);

        indeterminateBar.setVisibility(View.GONE);

        if (getContext() != null) {

            if (((MainActivity) getContext()).connectedTOInternet) {
                fetchData();
                fetchAgentList();
            } else {
                buttonHolder.setVisibility(View.GONE);
            }

        }


        return rootView;
    }

    @OnClick({R.id.way_cancel, R.id.way_delivery, R.id.way_hold, R.id.way_partial_delivery, R.id.way_transfer})
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.way_cancel: {

                if (getContext() != null) {

                    final AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                    builder1.setCancelable(false);

                    LayoutInflater inflater = this.getLayoutInflater();
                    View alertView = inflater.inflate(R.layout.select_spinner_way_cancel, null);
                    builder1.setView(alertView);

                    TextView title = (TextView) alertView.findViewById(R.id.alert_title);
                    final EditText comment = alertView.findViewById(R.id.id_default_comment_EditText);
                    title.setText(getResources().getText(R.string.way_cancel_product));
                    comment.setVisibility(View.GONE);

                    final EditText collectedAmount = alertView.findViewById(R.id.id_collected_EditText);

                    final Spinner commentSpinner = alertView.findViewById(R.id.list_spinner);
                    ArrayAdapter<String> commentListAdapter;
                    commentListAdapter = new ArrayAdapter<String>(getContext(), R.layout.spinner_item, R.id.spinner_item_text, ((MainActivity) getContext()).defaultComments);
                    commentSpinner.setAdapter(commentListAdapter);

                    builder1.setPositiveButton(
                            getContext().getResources().getString(R.string.yes),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {


                                }
                            }).setNegativeButton(getContext().getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User cancelled the dialog
                            dialog.cancel();
                        }
                    });

                    commentSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                            if ((i + 1) == ((MainActivity) getContext()).defaultComments.size()) {
                                comment.setVisibility(View.VISIBLE);

                            } else {
                                comment.setVisibility(View.GONE);
                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                    final AlertDialog alert11 = builder1.create();

                    alert11.setOnShowListener(new DialogInterface.OnShowListener() {
                        @Override
                        public void onShow(DialogInterface dialogInterface) {
                            alert11.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.RED);
                            alert11.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getContext().getResources().getColor(R.color.colorPrimary));
                            alert11.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    if (commentSpinner.getSelectedItemPosition() != 0 && !collectedAmount.getText().toString().equals("")) {

                                        ArrayList<String> temp = new ArrayList<>();
                                        temp.add(ecrNumber.getText().toString());
                                        Gson gson = new Gson();

                                        if ((commentSpinner.getSelectedItemPosition() + 1)
                                                == ((MainActivity) getContext()).defaultComments.size()) {

                                            ParcelCanceled parcelHoldCancel = new ParcelCanceled(temp, API.STATUS_AGENT_WAY_CANCEL, collectedAmount.getText().toString(), comment.getText().toString());
                                            updateStatus(gson.toJson(parcelHoldCancel));

                                        } else {
                                            ParcelCanceled parcelHoldCancel = new ParcelCanceled(temp, API.STATUS_AGENT_WAY_CANCEL, collectedAmount.getText().toString(), ((MainActivity) getContext()).defaultComments.get(commentSpinner.getSelectedItemPosition()));
                                            updateStatus(gson.toJson(parcelHoldCancel));

                                        }

                                        alert11.cancel();

                                    } else {

                                        if (collectedAmount.getText().toString().equals(""))
                                            Toast.makeText(getContext(), getContext().getResources().getString(R.string.collected_amount_give), Toast.LENGTH_LONG).show();

                                        else
                                            Toast.makeText(getContext(), getContext().getResources().getString(R.string.select_reason), Toast.LENGTH_LONG).show();

                                    }


                                }
                            });

                        }
                    });

                    alert11.show();

                }

                break;
            }

            case R.id.way_delivery: {

                if (getContext() != null) {

                    AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                    builder1.setCancelable(false);

                    LayoutInflater inflater = this.getLayoutInflater();
                    View alertView = inflater.inflate(R.layout.agent_distribute_parcel_delivered_partial_delivered_dialog, null);
                    builder1.setView(alertView);

                    TextView name = alertView.findViewById(R.id.alert_customer_name);
                    TextView totalPrice = alertView.findViewById(R.id.alert_total_price);
                    final EditText itemDelivered = alertView.findViewById(R.id.items_delivered);
                    final EditText amountPicked = alertView.findViewById(R.id.alert_parcel_amount_picked);
                    final EditText otp = alertView.findViewById(R.id.alert_parcel_otp);
                    final EditText receiverName = alertView.findViewById(R.id.receiver_name);
                    EditText receiverPhone = alertView.findViewById(R.id.receiver_phone);

                    final TextInputLayout receiverNameLayout = alertView.findViewById(R.id.receiver_name_layout);
                    final TextInputLayout receiverPhoneLayout = alertView.findViewById(R.id.receiver_phone_layout);

                    RadioButton receivedByClient = alertView.findViewById(R.id.received_by_client);
                    RadioButton receivedByOther = alertView.findViewById(R.id.received_by_other);
                    RadioGroup radioGroup = alertView.findViewById(R.id.receiver_group);

                    receivedByClient.setChecked(true);

                    radioGroup.setVisibility(View.GONE);
                    receiverNameLayout.setVisibility(View.GONE);
                    receiverPhoneLayout.setVisibility(View.GONE);
                    receivedByOther.setVisibility(View.GONE);

                    receivedByClient.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            receiverNameLayout.setVisibility(View.GONE);
                            receiverPhoneLayout.setVisibility(View.GONE);

                        }
                    });

                    receivedByOther.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            receiverNameLayout.setVisibility(View.VISIBLE);
                            receiverPhoneLayout.setVisibility(View.VISIBLE);
                        }
                    });

                    receivedByClient.setText(String.format("Received by %s", customerName.getText().toString()));

                    otp.setVisibility(View.GONE);

                    name.setText(customerName.getText().toString());
                    totalPrice.setText(String.format("Product price : %s", productPrice.getText().toString()));

                    builder1.setPositiveButton(
                            getContext().getResources().getString(R.string.get_sms),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            }).setNegativeButton(getContext().getResources().getString(R.string.back), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User cancelled the dialog
                            dialog.cancel();
                        }
                    });


                    final AlertDialog alert11 = builder1.create();
                    alert11.setOnShowListener(new DialogInterface.OnShowListener() {
                        @Override
                        public void onShow(DialogInterface arg0) {
                            alert11.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.RED);
                            alert11.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getContext().getResources().getColor(R.color.colorPrimary));

                            alert11.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    String amount = "0";

                                    if (!itemDelivered.getText().toString().equals("")) {
                                        if (!amountPicked.getText().toString().equals(""))
                                            amount = amountPicked.getText().toString();

                                        resendOTP(itemDelivered.getText().toString(), amountPicked.getText().toString(), 1);
                                        /*ArrayList<String> temp = new ArrayList<>();

                                        temp.add(ecrNumber.getText().toString());
                                        ParcelDeliveredPartialDelivered parcelDeliveredPartialDelivered = new ParcelDeliveredPartialDelivered(temp, API.STATUS_AGENT_WAY_DELIVERED, otp.getText().toString(), amount, itemDelivered.getText().toString());
                                        Gson gson = new Gson();
                                        updateStatus(gson.toJson(parcelDeliveredPartialDelivered));*/

                                        alert11.cancel();

                                    } else {
                                        Toast.makeText(getContext(),
                                                getContext().getResources().getString(R.string.select_all_fields),
                                                Toast.LENGTH_LONG).show();
                                    }

                                }
                            });

                        }
                    });

                    alert11.show();

                }

                break;
            }

            case R.id.way_hold: {

                if (getContext() != null) {

                    final AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                    builder1.setCancelable(false);

                    LayoutInflater inflater = this.getLayoutInflater();
                    View alertView = inflater.inflate(R.layout.select_spinner, null);
                    builder1.setView(alertView);

                    TextView title = (TextView) alertView.findViewById(R.id.alert_title);
                    final EditText comment = alertView.findViewById(R.id.id_default_comment_EditText);
                    title.setText(getResources().getText(R.string.way_hold_product));
                    comment.setVisibility(View.GONE);

                    final Spinner commentSpinner = alertView.findViewById(R.id.list_spinner);
                    ArrayAdapter<String> commentListAdapter;
                    commentListAdapter = new ArrayAdapter<String>(getContext(), R.layout.spinner_item, R.id.spinner_item_text, ((MainActivity) getContext()).defaultComments);
                    commentSpinner.setAdapter(commentListAdapter);

                    builder1.setPositiveButton(
                            getContext().getResources().getString(R.string.yes),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {


                                }
                            }).setNegativeButton(getContext().getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User cancelled the dialog
                            dialog.cancel();
                        }
                    });

                    commentSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                            if ((i + 1) == ((MainActivity) getContext()).defaultComments.size()) {
                                comment.setVisibility(View.VISIBLE);

                            } else {
                                comment.setVisibility(View.GONE);
                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                    final AlertDialog alert11 = builder1.create();

                    alert11.setOnShowListener(new DialogInterface.OnShowListener() {
                        @Override
                        public void onShow(DialogInterface dialogInterface) {
                            alert11.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.RED);
                            alert11.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getContext().getResources().getColor(R.color.colorPrimary));
                            alert11.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    if (commentSpinner.getSelectedItemPosition() != 0) {

                                        ArrayList<String> temp = new ArrayList<>();
                                        temp.add(ecrNumber.getText().toString());
                                        Gson gson = new Gson();

                                        if ((commentSpinner.getSelectedItemPosition() + 1)
                                                == ((MainActivity) getContext()).defaultComments.size()) {

                                            ParcelHoldCancel parcelHoldCancel = new ParcelHoldCancel(temp, API.STATUS_AGENT_WAY_HOLD, comment.getText().toString());
                                            updateStatus(gson.toJson(parcelHoldCancel));

                                        } else {
                                            ParcelHoldCancel parcelHoldCancel = new ParcelHoldCancel(temp, API.STATUS_AGENT_WAY_HOLD, ((MainActivity) getContext()).defaultComments.get(commentSpinner.getSelectedItemPosition()));
                                            updateStatus(gson.toJson(parcelHoldCancel));

                                        }

                                        alert11.cancel();

                                    } else {
                                        Toast.makeText(getContext(), getContext().getResources().getString(R.string.select_reason), Toast.LENGTH_LONG).show();
                                    }


                                }
                            });

                        }
                    });

                    alert11.show();

                }

                break;
            }

            case R.id.way_partial_delivery: {

                if (getContext() != null) {

                    AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                    builder1.setCancelable(false);

                    LayoutInflater inflater = this.getLayoutInflater();
                    View alertView = inflater.inflate(R.layout.agent_distribute_parcel_delivered_partial_delivered_dialog, null);
                    builder1.setView(alertView);

                    TextView name = alertView.findViewById(R.id.alert_customer_name);
                    TextView totalPrice = alertView.findViewById(R.id.alert_total_price);
                    final EditText itemDelivered = alertView.findViewById(R.id.items_delivered);
                    final EditText amountPicked = alertView.findViewById(R.id.alert_parcel_amount_picked);
                    final EditText otp = alertView.findViewById(R.id.alert_parcel_otp);


                    final EditText receiverName = alertView.findViewById(R.id.receiver_name);
                    EditText receiverPhone = alertView.findViewById(R.id.receiver_phone);

                    final TextInputLayout receiverNameLayout = alertView.findViewById(R.id.receiver_name_layout);
                    final TextInputLayout receiverPhoneLayout = alertView.findViewById(R.id.receiver_phone_layout);

                    RadioButton receivedByClient = alertView.findViewById(R.id.received_by_client);
                    RadioButton receivedByOther = alertView.findViewById(R.id.received_by_other);
                    RadioGroup radioGroup = alertView.findViewById(R.id.receiver_group);

                    receivedByClient.setChecked(true);

                    radioGroup.setVisibility(View.GONE);
                    receiverNameLayout.setVisibility(View.GONE);
                    receiverPhoneLayout.setVisibility(View.GONE);
                    receivedByOther.setVisibility(View.GONE);

                    receivedByClient.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            receiverNameLayout.setVisibility(View.GONE);
                            receiverPhoneLayout.setVisibility(View.GONE);

                        }
                    });

                    receivedByOther.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            receiverNameLayout.setVisibility(View.VISIBLE);
                            receiverPhoneLayout.setVisibility(View.VISIBLE);
                        }
                    });

                    receivedByClient.setText(String.format("Received by %s", customerName.getText().toString()));

                    otp.setVisibility(View.GONE);

                    name.setText(customerName.getText().toString());
                    totalPrice.setText(String.format("Product price : %s", productPrice.getText().toString()));

                    builder1.setPositiveButton(
                            getContext().getResources().getString(R.string.get_sms),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            }).setNegativeButton(getContext().getResources().getString(R.string.back), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User cancelled the dialog
                            dialog.cancel();
                        }
                    });


                    final AlertDialog alert11 = builder1.create();
                    alert11.setOnShowListener(new DialogInterface.OnShowListener() {
                        @Override
                        public void onShow(DialogInterface arg0) {
                            alert11.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.RED);
                            alert11.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getContext().getResources().getColor(R.color.colorPrimary));

                            alert11.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    String amount = "0";

                                    if (!itemDelivered.getText().toString().equals("")) {
                                        if (!amountPicked.getText().toString().equals(""))
                                            amount = amountPicked.getText().toString();

                                        resendOTP(itemDelivered.getText().toString(), amountPicked.getText().toString(), 0);
                                        /*ArrayList<String> temp = new ArrayList<>();

                                        temp.add(ecrNumber.getText().toString());
                                        ParcelDeliveredPartialDelivered parcelDeliveredPartialDelivered = new ParcelDeliveredPartialDelivered(temp, API.STATUS_AGENT_WAY_DELIVERED, otp.getText().toString(), amount, itemDelivered.getText().toString());
                                        Gson gson = new Gson();
                                        updateStatus(gson.toJson(parcelDeliveredPartialDelivered));*/

                                        alert11.cancel();

                                    } else {
                                        Toast.makeText(getContext(),
                                                getContext().getResources().getString(R.string.select_all_fields),
                                                Toast.LENGTH_LONG).show();
                                    }

                                }
                            });

                        }
                    });

                    alert11.show();

                }

                break;
            }

            case R.id.way_transfer: {

                if (getContext() != null) {

                    final AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                    builder1.setCancelable(false);

                    LayoutInflater inflater = this.getLayoutInflater();
                    View alertView = inflater.inflate(R.layout.agent_distribute_transfer_parcel, null);
                    builder1.setView(alertView);

                    TextView name = alertView.findViewById(R.id.alert_customer_name);
                    final EditText comment = alertView.findViewById(R.id.id_default_comment_EditText);

                    comment.setVisibility(View.GONE);

                    final Spinner branchSpinner = alertView.findViewById(R.id.branch_list);
                    final Spinner agentSpinner = alertView.findViewById(R.id.agent_list);
                    final Spinner commentSpinner = alertView.findViewById(R.id.comment);

                    final ArrayAdapter<String> agentListAdapter, commentListAdapter, branchListAdapter;

                    branchListAdapter = new ArrayAdapter<String>(getContext(), R.layout.spinner_item, R.id.spinner_item_text, branchName);
                    commentListAdapter = new ArrayAdapter<String>(getContext(), R.layout.spinner_item, R.id.spinner_item_text, ((MainActivity) getContext()).defaultComments);
                    agentListAdapter = new ArrayAdapter<String>(getContext(), R.layout.spinner_item, R.id.spinner_item_text, agentName.get(0));

                    branchSpinner.setAdapter(branchListAdapter);
                    commentSpinner.setAdapter(commentListAdapter);
                    agentSpinner.setAdapter((agentListAdapter));

                    agentSpinner.setVisibility(View.GONE);

                    name.setText(customerName.getText().toString());

                    builder1.setPositiveButton(
                            getContext().getResources().getString(R.string.confirm_parcel),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            }).setNegativeButton(getContext().getResources().getString(R.string.back), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User cancelled the dialog
                            dialog.cancel();
                        }
                    });

                    branchSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                            agentListAdapter.clear();
                            agentListAdapter.addAll(agentName.get(i));
                            agentListAdapter.notifyDataSetChanged();
                            agentSpinner.setAdapter(agentListAdapter);
                            if (i > 0)
                                agentSpinner.setVisibility(View.VISIBLE);
                            else
                                agentSpinner.setVisibility(View.GONE);


                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                    commentSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                            if ((i + 1) == ((MainActivity) getContext()).defaultComments.size()) {
                                comment.setVisibility(View.VISIBLE);

                            } else {
                                comment.setVisibility(View.GONE);
                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                    final AlertDialog alert11 = builder1.create();
                    alert11.setOnShowListener(new DialogInterface.OnShowListener() {
                        @Override
                        public void onShow(DialogInterface arg0) {
                            alert11.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.RED);
                            alert11.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getContext().getResources().getColor(R.color.colorPrimary));
                            alert11.getButton(AlertDialog.BUTTON_NEUTRAL).setTextColor(Color.BLACK);

                            alert11.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    String amount = "0";

                                    if (agentSpinner.getSelectedItemPosition() != 0
                                            && commentSpinner.getSelectedItemPosition() != 0) {

                                        ArrayList<String> temp = new ArrayList<>();
                                        Gson gson = new Gson();
                                        temp.add(ecrNumber.getText().toString());

                                        if ((commentSpinner.getSelectedItemPosition() + 1)
                                                == ((MainActivity) getContext()).defaultComments.size()) {

                                            ParcelTransfer parcelTransfer = new ParcelTransfer(temp,
                                                    API.STATUS_AGENT_WAY_TRANSFER, agentID.get(branchSpinner.getSelectedItemPosition()).get(agentSpinner.getSelectedItemPosition()), comment.getText().toString());
                                            updateStatus(gson.toJson(parcelTransfer));


                                        } else {

                                            ParcelTransfer parcelTransfer = new ParcelTransfer(temp,
                                                    API.STATUS_AGENT_WAY_TRANSFER, agentID.get(branchSpinner.getSelectedItemPosition()).get(agentSpinner.getSelectedItemPosition()), ((MainActivity) getContext()).defaultComments.get(commentSpinner.getSelectedItemPosition()));
                                            updateStatus(gson.toJson(parcelTransfer));

                                        }

                                        alert11.cancel();

                                    } else {

                                        Toast.makeText(getContext(), getContext().getResources().getString(R.string.select_all_fields), Toast.LENGTH_LONG).show();
                                    }

                                }
                            });

                        }
                    });

                    alert11.show();

                }

                break;
            }

        }

    }

    private void fetchData() {

        RequestQueue queue = null;
        // Instantiate the RequestQueue.
        if (getContext() != null)
            queue = Volley.newRequestQueue(getContext());
        String url = API.baseUrl + API.agentParcelDetail;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String code = jsonObject.getString(API.KEY_CODE);

                            if (code.equals(API.VALUE_CODE_SUCCESS)) {

                                dataFromServer(jsonObject);

                            } else {

                                JSONObject error = jsonObject.getJSONObject(API.KEY_JSON_ERROR);

                                if (code.equals(API.VALUE_CODE_AUTHENTICATION_ERROR)) {

                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                } else if (code.equals(API.VALUE_CODE_PARAMETER_ERROR)) {
                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                try {

                    if (getContext() != null) {

                        if (error.getMessage().equals("com.android.volley.TimeoutError"))
                            Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_timeout), Toast.LENGTH_SHORT).show();

                        Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_unreachable), Toast.LENGTH_SHORT).show();
                    }

                    Log.v("Volly error : ", "" + error);

                } catch (Exception e) {

                    Log.v("Distribute Way E : ", " " + e);

                }
            }

        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                if (getContext() != null) {
                    params.put(API.KEY_JSON_USER_AUTHENTICATION, ((MainActivity) getContext()).authenticationKey);
                    params.put(API.KEY_JSON_USER_GROUP, ((MainActivity) getContext()).userGroup);
                    params.put(API.KEY_USER_ID, ((MainActivity) getContext()).userID);
                    params.put(API.KEY_CONTENT_TYPE, API.VALUE_CONTENT_VALUE_JSON);
                }

                return params;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                String your_string_json = json; // put your json
                return your_string_json.getBytes();
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                Config.volley_time_out,
                Config.max_retries,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Add the request to the RequestQueue.
        if (queue != null)
            queue.add(stringRequest);

    }

    private CustomerProduct[] dataFromServer(JSONObject jsonObject) throws JSONException {

        JSONArray jsonArray = jsonObject.getJSONArray(API.KEY_JSON_DATA);

        CustomerProduct[] customerProducts = new CustomerProduct[jsonArray.length()];

        for (int i = 0; i < jsonArray.length(); i++) {

            JSONObject customerProduct = jsonArray.getJSONObject(i);

            ecrNumber.setText(customerProduct.getString(API.KEY_CUSTOMER_PRODUCT_ECR_NUMBER_LOWER_CASE));
            customerName.setText(customerProduct.getString(API.KEY_CUSTOMER_PRODUCT_CUSTOMER_NAME));
            customerAddress.setText(customerProduct.getString(API.KEY_CUSTOMER_PRODUCT_CUSTOMER_ADDRESS));
            productPrice.setText(customerProduct.getString(API.KEY_CUSTOMER_PRODUCT_PRICE));
            merchantName.setText(customerProduct.getString(API.KEY_CUSTOMER_PRODUCT_MERCHANT_NAME));
            noOfItems.setText(String.format(" %s", customerProduct.getString(API.KEY_CUSTOMER_PRODUCT_NO_OF_ITEMS)));

        }

        buttonHolder.setVisibility(View.VISIBLE);

        return customerProducts;
    }

    private void agentListFromServer(JSONObject jsonObject) throws JSONException {

        branchName = new ArrayList<>();
        branchId = new ArrayList<>();
        agentName = new ArrayList<>();
        agentID = new ArrayList<>();

        branchName.add(getActivity().getResources().getString(R.string.select_branch));
        branchId.add(getActivity().getResources().getString(R.string.select_branch));
        agentName.add(new ArrayList<String>());
        agentID.add(new ArrayList<String>());

        JSONArray jsonArray = jsonObject.getJSONArray(API.KEY_JSON_DATA);

        for (int i = 0; i < jsonArray.length(); i++) {

            JSONObject branch = jsonArray.getJSONObject(i);
            branchId.add(branch.getString(API.KEY_BRANCH_ID));
            branchName.add(branch.getString(API.KEY_BRANCH_NAME));

            JSONArray agentListForBranch = branch.getJSONArray(API.KEY_AGENT_LIST);
            ArrayList<String> agentTempID = new ArrayList<>();
            ArrayList<String> agentTempName = new ArrayList<>();

            agentTempID.add("Select");
            agentTempName.add("Select Agent");

            for (int j = 0; j < agentListForBranch.length(); j++) {

                JSONObject agentInfo = agentListForBranch.getJSONObject(j);
                agentTempID.add(agentInfo.getString(API.KEY_AGENT_DETAIL_AGENT_ID));
                agentTempName.add(agentInfo.getString(API.KEY_AGENT_DETAIL_AGENT_NAME));

            }

            agentID.add(agentTempID);
            agentName.add(agentTempName);

        }

    }

    private void updateStatus(final String data) {

        indeterminateBar.setVisibility(View.VISIBLE);
        holdBtn.setVisibility(View.GONE);

        final ProgressDialog dialog = ProgressDialog.show(getActivity(), "",
                getActivity().getResources().getString(R.string.loading), true);

        dialog.show();

        RequestQueue queue = null;
        // Instantiate the RequestQueue.
        if (getContext() != null)
            queue = Volley.newRequestQueue(getContext());
        String url = API.baseUrl + API.agentParcelUpdate;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        indeterminateBar.setVisibility(View.GONE);
                        holdBtn.setVisibility(View.VISIBLE);
                        dialog.cancel();

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String code = jsonObject.getString(API.KEY_CODE);

                            if (code.equals(API.VALUE_CODE_SUCCESS)) {

                                JSONObject data = jsonObject.getJSONObject(API.KEY_JSON_DATA);

                                JSONArray updated = data.getJSONArray(API.KEY_JSON_UPDATED);
                                JSONArray rejected = data.getJSONArray(API.KEY_JSON_REJECTED);
                                JSONArray failed = data.getJSONArray(API.KEY_JSON_FAILED);

                                if (updated.length() > 0) {
                                    Toast.makeText(getContext(), "SUCCESS", Toast.LENGTH_SHORT).show();
                                    if (getActivity() != null) {
                                        if (getActivity().getSupportFragmentManager().getBackStackEntryCount() > 0)
                                            getActivity().getSupportFragmentManager().popBackStack();
                                    }
                                } else if (rejected.length() > 0) {
                                    Toast.makeText(getContext(), " " + rejected.getJSONObject(0).getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_SHORT).show();
                                } else if (failed.length() > 0) {
                                    Toast.makeText(getContext(), " " + failed.getJSONObject(0).getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_SHORT).show();

                                }

                            } else {

                                JSONObject error = jsonObject.getJSONObject(API.KEY_JSON_ERROR);

                                if (code.equals(API.VALUE_CODE_AUTHENTICATION_ERROR)) {

                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                } else if (code.equals(API.VALUE_CODE_PARAMETER_ERROR)) {
                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                indeterminateBar.setVisibility(View.GONE);
                holdBtn.setVisibility(View.VISIBLE);
                try {
                    dialog.cancel();

                    if (getContext() != null) {

                        if (error.getMessage().equals("com.android.volley.TimeoutError"))
                            Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_timeout), Toast.LENGTH_SHORT).show();

                        Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_unreachable), Toast.LENGTH_SHORT).show();
                    }

                    Log.v("Volly error : ", "" + error);

                } catch (Exception e) {

                    Log.v("Distribute Way E : ", " " + e);

                }
            }

        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                if (getContext() != null) {

                    params.put(API.KEY_JSON_USER_AUTHENTICATION, ((MainActivity) getContext()).authenticationKey);
                    params.put(API.KEY_JSON_USER_GROUP, ((MainActivity) getContext()).userGroup);
                    params.put(API.KEY_USER_ID, ((MainActivity) getContext()).userID);
                    params.put(API.KEY_CONTENT_TYPE, API.VALUE_CONTENT_VALUE_JSON);

                }

                return params;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                String your_string_json = data; // put your json
                return your_string_json.getBytes();
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                Config.volley_time_out,
                Config.max_retries,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Add the request to the RequestQueue.
        if (queue != null)
            queue.add(stringRequest);

    }

    private void updateStatus(final String data, final AlertDialog alertDialog) {

        indeterminateBar.setVisibility(View.VISIBLE);
        holdBtn.setVisibility(View.GONE);

        final ProgressDialog dialog = ProgressDialog.show(getActivity(), "",
                getActivity().getResources().getString(R.string.loading), true);

        dialog.show();

        RequestQueue queue = null;
        // Instantiate the RequestQueue.
        if (getContext() != null)
            queue = Volley.newRequestQueue(getContext());
        String url = API.baseUrl + API.agentParcelUpdate;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        indeterminateBar.setVisibility(View.GONE);
                        holdBtn.setVisibility(View.VISIBLE);
                        dialog.cancel();

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String code = jsonObject.getString(API.KEY_CODE);

                            if (code.equals(API.VALUE_CODE_SUCCESS)) {

                                JSONObject data = jsonObject.getJSONObject(API.KEY_JSON_DATA);

                                JSONArray updated = data.getJSONArray(API.KEY_JSON_UPDATED);
                                JSONArray rejected = data.getJSONArray(API.KEY_JSON_REJECTED);
                                JSONArray failed = data.getJSONArray(API.KEY_JSON_FAILED);

                                if (updated.length() > 0) {
                                    Toast.makeText(getContext(), "SUCCESS", Toast.LENGTH_SHORT).show();
                                    alertDialog.cancel();
                                    if ((MainActivity) getActivity() != null)
                                        ((MainActivity) getActivity()).getSupportFragmentManager().popBackStack();
                                } else if (rejected.length() > 0) {
                                    Toast.makeText(getContext(), " " + rejected.getJSONObject(0).getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_SHORT).show();
                                } else if (failed.length() > 0) {
                                    Toast.makeText(getContext(), " " + failed.getJSONObject(0).getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_SHORT).show();

                                }

                            } else {

                                JSONObject error = jsonObject.getJSONObject(API.KEY_JSON_ERROR);

                                if (code.equals(API.VALUE_CODE_AUTHENTICATION_ERROR)) {

                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                } else if (code.equals(API.VALUE_CODE_PARAMETER_ERROR)) {
                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                indeterminateBar.setVisibility(View.GONE);
                holdBtn.setVisibility(View.VISIBLE);
                try {
                    dialog.cancel();

                    if (getContext() != null) {

                        if (error.getMessage().equals("com.android.volley.TimeoutError"))
                            Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_timeout), Toast.LENGTH_SHORT).show();

                        Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_unreachable), Toast.LENGTH_SHORT).show();
                    }

                    Log.v("Volly error : ", "" + error);

                } catch (Exception e) {

                    Log.v("Distribute Way E : ", " " + e);

                }
            }

        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                if (getContext() != null) {

                    params.put(API.KEY_JSON_USER_AUTHENTICATION, ((MainActivity) getContext()).authenticationKey);
                    params.put(API.KEY_JSON_USER_GROUP, ((MainActivity) getContext()).userGroup);
                    params.put(API.KEY_USER_ID, ((MainActivity) getContext()).userID);
                    params.put(API.KEY_CONTENT_TYPE, API.VALUE_CONTENT_VALUE_JSON);

                }

                return params;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                String your_string_json = data; // put your json
                return your_string_json.getBytes();
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                Config.volley_time_out,
                Config.max_retries,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Add the request to the RequestQueue.
        if (queue != null)
            queue.add(stringRequest);

    }

    private void resendOTP(final String itemDelivered, final String collected, final int flag) {

        final ProgressDialog dialog = ProgressDialog.show(getActivity(), "",
                getActivity().getResources().getString(R.string.loading), true);

        dialog.show();

        RequestQueue queue = null;
        // Instantiate the RequestQueue.
        if (getContext() != null)
            queue = Volley.newRequestQueue(getContext());
        String url = API.baseUrl + API.resendOTP;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String code = jsonObject.getString(API.KEY_CODE);

                            if (code.equals(API.VALUE_CODE_SUCCESS)) {

                                //merchantProductListAdapter.listUpdated(dataFromServer(jsonObject));
                                Toast.makeText(getContext(), "Verification code send", Toast.LENGTH_SHORT).show();

                                if (getContext() != null) {

                                    AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                                    builder1.setCancelable(false);

                                    LayoutInflater inflater = getActivity().getLayoutInflater();
                                    View alertView = inflater.inflate(R.layout.agent_distribute_parcel_delivered_partial_delivered_dialog, null);
                                    builder1.setView(alertView);

                                    TextView name = alertView.findViewById(R.id.alert_customer_name);
                                    TextView totalPrice = alertView.findViewById(R.id.alert_total_price);
                                    final EditText itemDeliveredET = alertView.findViewById(R.id.items_delivered);
                                    final EditText amountPickedET = alertView.findViewById(R.id.alert_parcel_amount_picked);
                                    final EditText otp = alertView.findViewById(R.id.alert_parcel_otp);


                                    final TextInputEditText receiverName = alertView.findViewById(R.id.receiver_name);
                                    final TextInputEditText receiverPhone = alertView.findViewById(R.id.receiver_phone);

                                    final TextInputLayout receiverNameLayout = alertView.findViewById(R.id.receiver_name_layout);
                                    final TextInputLayout receiverPhoneLayout = alertView.findViewById(R.id.receiver_phone_layout);

                                    RadioButton receivedByClient = alertView.findViewById(R.id.received_by_client);
                                    final RadioButton receivedByOther = alertView.findViewById(R.id.received_by_other);
                                    RadioGroup radioGroup = alertView.findViewById(R.id.receiver_group);

                                    receivedByClient.setChecked(true);

                                    receiverNameLayout.setVisibility(View.GONE);
                                    receiverPhoneLayout.setVisibility(View.GONE);

                                    receivedByClient.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {

                                            receiverNameLayout.setVisibility(View.GONE);
                                            receiverPhoneLayout.setVisibility(View.GONE);

                                        }
                                    });

                                    receivedByOther.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            receiverNameLayout.setVisibility(View.VISIBLE);
                                            receiverPhoneLayout.setVisibility(View.VISIBLE);
                                        }
                                    });

                                    receivedByClient.setText(String.format("Received by %s", customerName.getText().toString()));

                                    itemDeliveredET.setText(itemDelivered);
                                    itemDeliveredET.setEnabled(false);

                                    amountPickedET.setText(collected);
                                    amountPickedET.setEnabled(false);

                                    name.setText(customerName.getText().toString());
                                    totalPrice.setText(productPrice.getText().toString());

                                    builder1.setPositiveButton(
                                            getContext().getResources().getString(R.string.confirm_parcel),
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {

                                                }
                                            }).setNegativeButton(getContext().getResources().getString(R.string.back), new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            // User cancelled the dialog
                                            dialog.cancel();
                                        }
                                    }).setNeutralButton(getContext().getResources().getString(R.string.resend_sms), new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            // User cancelled the dialog

                                        }
                                    });


                                    final AlertDialog alert11 = builder1.create();
                                    alert11.setOnShowListener(new DialogInterface.OnShowListener() {
                                        @Override
                                        public void onShow(DialogInterface arg0) {
                                            alert11.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.RED);
                                            alert11.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getContext().getResources().getColor(R.color.colorPrimary));
                                            alert11.getButton(AlertDialog.BUTTON_NEUTRAL).setTextColor(Color.BLACK);

                                            alert11.getButton(AlertDialog.BUTTON_NEUTRAL).setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {

                                                    resendOTP(itemDelivered, collected, flag);

                                                    alert11.cancel();

                                                }
                                            });

                                            alert11.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    String amount = "0", comments = "";
                                                    boolean hasError = false;

                                                    if (receivedByOther.isChecked()) {

                                                        if (receiverName.getText().toString().equals("")) {

                                                            //receiverName.setError("Enter receiver name");
                                                            hasError = true;

                                                        }

                                                        if (receiverPhone.getText().toString().equals("")) {

                                                            //receiverPhone.setError("Enter receiver phone");
                                                            hasError = true;

                                                        }

                                                        comments = "Receiver name : " + receiverName.getText().toString() + " Receiver phone : " + receiverPhone.getText().toString();

                                                    }

                                                    if (!itemDeliveredET.getText().toString().equals("") && !otp.getText().toString().equals("") && !hasError) {

                                                        ArrayList<String> temp = new ArrayList<>();

                                                        temp.add(ecrNumber.getText().toString());
                                                        ParcelDeliveredPartialDelivered parcelDeliveredPartialDelivered;
                                                        if (flag == 1) {
                                                            parcelDeliveredPartialDelivered = new ParcelDeliveredPartialDelivered(temp, API.STATUS_AGENT_WAY_DELIVERED, otp.getText().toString(), amountPickedET.getText().toString(), itemDeliveredET.getText().toString(), comments);
                                                        } else {
                                                            parcelDeliveredPartialDelivered = new ParcelDeliveredPartialDelivered(temp, API.STATUS_AGENT_WAY_PARTIAL_DELIVERY, otp.getText().toString(), amountPickedET.getText().toString(), itemDeliveredET.getText().toString(), comments);
                                                        }

                                                        Gson gson = new Gson();
                                                        updateStatus(gson.toJson(parcelDeliveredPartialDelivered), alert11);

                                                        /*alert11.cancel();*/
                                                    } else {

                                                        Toast.makeText(getContext(), getContext().getResources().getString(R.string.select_all_fields), Toast.LENGTH_LONG).show();
                                                    }

                                                }
                                            });

                                        }
                                    });

                                    alert11.show();

                                }


                            } else {

                                JSONObject error = jsonObject.getJSONObject(API.KEY_JSON_ERROR);

                                if (code.equals(API.VALUE_CODE_AUTHENTICATION_ERROR)) {

                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                } else if (code.equals(API.VALUE_CODE_PARAMETER_ERROR)) {
                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.cancel();
                try {

                    if (getContext() != null) {

                        if (error.getMessage().equals("com.android.volley.TimeoutError"))
                            Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_timeout), Toast.LENGTH_SHORT).show();

                        Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_unreachable), Toast.LENGTH_SHORT).show();
                    }

                    Log.v("Volly error : ", "" + error);

                } catch (Exception e) {

                    Log.v("Distribute Way E : ", " " + e);

                }
            }

        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                if (getContext() != null) {

                    params.put(API.KEY_JSON_USER_AUTHENTICATION, ((MainActivity) getContext()).authenticationKey);
                    params.put(API.KEY_JSON_USER_GROUP, ((MainActivity) getContext()).userGroup);
                    params.put(API.KEY_USER_ID, ((MainActivity) getContext()).userID);
                    params.put(API.KEY_CONTENT_TYPE, API.VALUE_CONTENT_VALUE);

                }

                return params;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put(API.KEY_OTP_TYPE, API.VALUE_OTP_TYPE_DELIVERY);
                params.put(API.KEY_ORDER_ID, ecrNumber.getText().toString());
                params.put(API.KEY_CUSTOMER_PRODUCT_COLLECTED_AMOUNT, collected);
                params.put(API.KEY_ITEM_DELIVERED, itemDelivered);
                return params;
            }


        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                Config.volley_time_out,
                Config.max_retries,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Add the request to the RequestQueue.
        if (queue != null)
            queue.add(stringRequest);

    }

    private void fetchAgentList() {

        RequestQueue queue = null;
        // Instantiate the RequestQueue.
        if (getContext() != null)
            queue = Volley.newRequestQueue(getContext());
        String url = API.baseUrl + API.agentAllAgentList;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String code = jsonObject.getString(API.KEY_CODE);

                            if (code.equals(API.VALUE_CODE_SUCCESS)) {

                                agentListFromServer(jsonObject);

                            } else {

                                JSONObject error = jsonObject.getJSONObject(API.KEY_JSON_ERROR);

                                if (code.equals(API.VALUE_CODE_AUTHENTICATION_ERROR)) {

                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                } else if (code.equals(API.VALUE_CODE_PARAMETER_ERROR)) {
                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                try {

                    if (getContext() != null) {

                        if (error.getMessage().equals("com.android.volley.TimeoutError"))
                            Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_timeout), Toast.LENGTH_SHORT).show();

                        Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_unreachable), Toast.LENGTH_SHORT).show();
                    }

                    Log.v("Volly error : ", "" + error);

                } catch (Exception e) {

                    Log.v("Distribute Way E : ", " " + e);

                }
            }

        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                if (getContext() != null) {
                    params.put(API.KEY_JSON_USER_AUTHENTICATION, ((MainActivity) getContext()).authenticationKey);
                    params.put(API.KEY_JSON_USER_GROUP, ((MainActivity) getContext()).userGroup);
                    params.put(API.KEY_USER_ID, ((MainActivity) getContext()).userID);
                    params.put(API.KEY_CONTENT_TYPE, API.VALUE_CONTENT_VALUE_JSON);
                }

                return params;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                String your_string_json = json; // put your json
                return your_string_json.getBytes();
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                Config.volley_time_out,
                Config.max_retries,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Add the request to the RequestQueue.
        if (queue != null)
            queue.add(stringRequest);

    }

}
