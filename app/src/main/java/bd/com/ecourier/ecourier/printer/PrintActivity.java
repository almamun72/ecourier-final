package bd.com.ecourier.ecourier.printer;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.print.sdk.PrinterConstants.Connect;
import com.android.print.sdk.PrinterInstance;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bd.com.ecourier.ecourier.API;
import bd.com.ecourier.ecourier.Config;
import bd.com.ecourier.ecourier.Model.AgentConsignments;
import bd.com.ecourier.ecourier.Model.CustomerProduct;
import bd.com.ecourier.ecourier.R;

//import com.android.print.demo.utils.PrintUtils;
//import com.google.zxing.BarcodeFormat;
/*import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;*/

public class PrintActivity extends Activity implements OnClickListener {
    // Intent request codes
    public static final int CONNECT_DEVICE = 1;
    public static final int ENABLE_BT = 2;
    private static boolean isConnected;
    Bitmap bp;
    private Context mContext;
    private int offset = 0;
    private int currIndex = 0;
    private IPrinterOpertion myOpertion;
    private PrinterInstance mPrinter;
    private Button connectButton;
    /*private Button printText;
    private Button printBarCode;*/
    private boolean is58mm = true;
    private boolean isStylus = false;
    private ProgressDialog dialog;
    private FloatingActionButton printButton;
    private String json;

    private String userType, userID, authenticationKey;
    private TextView ecrNumber, customerName, customerAddress, productPrice, merchantName;

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Connect.SUCCESS:
                    isConnected = true;
                    mPrinter = myOpertion.getPrinter();
                    break;
                case Connect.FAILED:
                    isConnected = false;
                    Toast.makeText(mContext, "connect failed...", Toast.LENGTH_SHORT).show();
                    break;
                case Connect.CLOSED:
                    isConnected = false;
                    Toast.makeText(mContext, "connect close...", Toast.LENGTH_SHORT).show();
                    break;
                default:
                    break;
            }

            updateButtonState();

            if (dialog != null && dialog.isShowing()) {
                dialog.dismiss();
            }
        }

    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.print_activity);

        mContext = this;

        AgentConsignments agentConsignments;
        Gson gson = new Gson();

        userType = getSharedPreferences(Config.SP_ECourier_APP, MODE_PRIVATE)
                .getString(Config.SP_USER_TYPE, "");
        userID = getSharedPreferences(Config.SP_ECourier_APP, MODE_PRIVATE)
                .getString(API.KEY_USER_ID, "");
        authenticationKey = getSharedPreferences(Config.SP_ECourier_APP, MODE_PRIVATE)
                .getString(API.KEY_JSON_USER_AUTHENTICATION, "");

        ecrNumber = findViewById(R.id.product_ecr_number);
        customerName = findViewById(R.id.product_customer_name);
        customerAddress = findViewById(R.id.customer_address);
        productPrice = findViewById(R.id.product_price);
        merchantName = findViewById(R.id.merchant_name);

        ArrayList<String> temp = new ArrayList<>();
        if (getIntent().getExtras() != null) {

            temp.add(getIntent().getExtras().getString(API.KEY_JSON_CONSIGNMENTS));
            agentConsignments = new AgentConsignments(temp);
            json = gson.toJson(agentConsignments);

            fetchData();

        }

        InitView();

    }

    private void InitView() {
        connectButton = (Button) findViewById(R.id.connect);
        connectButton.setOnClickListener(this);

        /*printText = (Button) findViewById(R.id.btnPrintText);
        printText.setOnClickListener(this);
        printBarCode = (Button) findViewById(R.id.btnPrintBarCode);
        printBarCode.setOnClickListener(this);*/
        printButton = findViewById(R.id.btnPrintText);
        printButton.setOnClickListener(this);
        printButton.setVisibility(View.GONE);

        dialog = new ProgressDialog(mContext);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setTitle("Connecting...");
        dialog.setMessage("Please Wait...");
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);

		/*String text="1234567890"; // Whatever you need to encode in the QR code
        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
		try {
			BitMatrix bitMatrix = multiFormatWriter.encode(text, BarcodeFormat.QR_CODE,200,200);
			BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
			bp = barcodeEncoder.createBitmap(bitMatrix);

		} catch (WriterException e) {
			e.printStackTrace();
		}*/

//		setTitleTextColor(0);
    }

    private void updateButtonState() {
        if (!isConnected) {
            String connStr = getResources().getString(R.string.connect);
            switch (currIndex) {
                case 0:
                    connStr = getResources().getString(R.string.connect);
                    break;
                default:
                    break;
            }
            connectButton.setText(connStr);
            printButton.setVisibility(View.GONE);

        } else {
            connectButton.setText(R.string.disconnect);
            printButton.setVisibility(View.VISIBLE);

        }

        /*printText.setEnabled(isConnected);
        printBarCode.setEnabled(isConnected);*/
    }

    @Override
    protected void onActivityResult(final int requestCode, int resultCode, final Intent data) {
        switch (requestCode) {
            case CONNECT_DEVICE:
                if (resultCode == Activity.RESULT_OK) {
                    dialog.show();
                    new Thread(new Runnable() {
                        public void run() {
                            myOpertion.open(data);
                        }
                    }).start();
                }
                break;
            case ENABLE_BT:
                if (resultCode == Activity.RESULT_OK) {
                    myOpertion.chooseDevice();
                } else {
                    Toast.makeText(this, R.string.bt_not_enabled, Toast.LENGTH_SHORT).show();
                }
        }
    }

    private void openConn() {
        if (!isConnected) {
            switch (currIndex) {
                case 0: // bluetooth
                    myOpertion = new BluetoothOperation(PrintActivity.this, mHandler);
                    break;
                default:
                    break;
            }
            myOpertion.chooseDevice();
        } else {
            myOpertion.close();
            myOpertion = null;
            mPrinter = null;
        }
    }

    @Override
    public void onClick(View view) {
        if (view == connectButton) {
            openConn();
        }/* else if(view == btnBluetooth){
            onPageSelected(view);
		}*/ else if (view == printButton) {
            Log.v("Print", "" + "print");
//            PrintUtils.printImage(mContext.getResources(), mPrinter, isStylus);
            PrintUtils.printCustomImage(mContext.getResources(), mPrinter, isStylus, is58mm);

//            PrintUtils.printText(mContext.getResources(), mPrinter);
            PrintUtils.printTable(mContext.getResources(), mPrinter, is58mm, merchantName.getText().toString(), merchantName.getText().toString(), customerName.getText().toString(), customerAddress.getText().toString());
            PrintUtils.printBarCode(mPrinter, ecrNumber.getText().toString());

        } /*else if (view == printTable) {
            PrintUtils.printTable(mContext.getResources(), mPrinter, is58mm);
		} else if (view == printImage) {
			PrintUtils.printImage(bp, mPrinter, isStylus);
			//PrintUtils.printCustomImage(mContext.getResources(), mPrinter, isStylus, is58mm);
		} else if (view == printNote){
			PrintUtils.printNote(mContext.getResources(), mPrinter, is58mm);
		} else if (view == printBarCode) {
            PrintUtils.printBarCode(mPrinter, "123456789");
        }*/
    }

    private void fetchData() {

        RequestQueue queue = null;
        // Instantiate the RequestQueue.
        if (getApplicationContext() != null)
            queue = Volley.newRequestQueue(getApplicationContext());
        String url = API.baseUrl + API.agentParcelDetail;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String code = jsonObject.getString(API.KEY_CODE);

                            if (code.equals(API.VALUE_CODE_SUCCESS)) {

                                dataFromServer(jsonObject);

                            } else {

                                JSONObject error = jsonObject.getJSONObject(API.KEY_JSON_ERROR);

                                if (code.equals(API.VALUE_CODE_AUTHENTICATION_ERROR)) {

                                    Toast.makeText(getApplicationContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                } else if (code.equals(API.VALUE_CODE_PARAMETER_ERROR)) {
                                    Toast.makeText(getApplicationContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                try {

                    if (error.getMessage().equals("com.android.volley.TimeoutError"))
                        Toast.makeText(PrintActivity.this, getResources().getString(R.string.server_timeout), Toast.LENGTH_SHORT).show();

                    Toast.makeText(PrintActivity.this, getResources().getString(R.string.server_unreachable), Toast.LENGTH_SHORT).show();


                    Log.v("Volly error : ", "" + error);

                } catch (Exception e) {

                    Log.v("Distribute Way E : ", " " + e);

                }
            }

        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                if (getApplicationContext() != null) {
                    params.put(API.KEY_JSON_USER_AUTHENTICATION, authenticationKey);
                    params.put(API.KEY_JSON_USER_GROUP, userType);
                    params.put(API.KEY_USER_ID, userID);
                    params.put(API.KEY_CONTENT_TYPE, API.VALUE_CONTENT_VALUE_JSON);
                }

                return params;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                String your_string_json = json; // put your json
                return your_string_json.getBytes();
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                Config.volley_time_out,
                Config.max_retries,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Add the request to the RequestQueue.
        if (queue != null)
            queue.add(stringRequest);

    }

    private void dataFromServer(JSONObject jsonObject) throws JSONException {

        JSONArray jsonArray = jsonObject.getJSONArray(API.KEY_JSON_DATA);

        CustomerProduct[] customerProducts = new CustomerProduct[jsonArray.length()];

        for (int i = 0; i < jsonArray.length(); i++) {

            JSONObject customerProduct = jsonArray.getJSONObject(i);

            ecrNumber.setText(customerProduct.getString(API.KEY_CUSTOMER_PRODUCT_ECR_NUMBER_LOWER_CASE));
            customerName.setText(customerProduct.getString(API.KEY_CUSTOMER_PRODUCT_CUSTOMER_NAME));
            customerAddress.setText(customerProduct.getString(API.KEY_CUSTOMER_PRODUCT_CUSTOMER_ADDRESS));
            productPrice.setText(String.format("%s%s", customerProduct.getString(API.KEY_CUSTOMER_PRODUCT_PRICE), getApplicationContext().getResources().getString(R.string.taka)));
            merchantName.setText(customerProduct.getString(API.KEY_CUSTOMER_PRODUCT_MERCHANT_NAME));

        }

    }

}
