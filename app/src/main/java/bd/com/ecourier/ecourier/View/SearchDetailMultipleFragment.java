package bd.com.ecourier.ecourier.View;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bd.com.ecourier.ecourier.API;
import bd.com.ecourier.ecourier.Config;
import bd.com.ecourier.ecourier.Model.AgentConsignments;
import bd.com.ecourier.ecourier.Model.CustomerProduct;
import bd.com.ecourier.ecourier.Presenter.CustomerProductListAdapter;
import bd.com.ecourier.ecourier.R;
import bd.com.ecourier.ecourier.clickListener.ClickListener;
import bd.com.ecourier.ecourier.clickListener.RecyclerTouchListener;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by User on 10-Dec-17.
 */

public class SearchDetailMultipleFragment extends Fragment {

    @BindView(R.id.swipe_down_to_refresh_list)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.recyclerView_list)
    RecyclerView recyclerView;
    @BindView(R.id.fab_scan)
    FloatingActionButton QRScan;
    @BindView(R.id.recyclerView_list_notice)
    TextView noListNotice;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;


    private CustomerProductListAdapter customerProductListAdapter;
    private View rootView;

    private CustomerProduct customerProduct;
    private ArrayList<CustomerProduct> customerProductArrayList;

    private String json;

    public SearchDetailMultipleFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {

            json = getArguments().getString(API.KEY_JSON_SEARCH_PAYLOAD);

        }

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.recycle_view_list_layout, container, false);
        ButterKnife.bind(this, rootView);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                /*refreshList();*/

            }
        });

        customerProductArrayList = new ArrayList<>();

        showFeed(customerProductArrayList);
        noListNotice.setVisibility(View.GONE);
        QRScan.hide();

        if (getContext() != null)
            if (((MainActivity) getContext()).connectedTOInternet)
                fetchData();

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, final int position) {

                customerProduct = customerProductArrayList.get(position);
                LinearLayout customerProductInfoBody = view.findViewById(R.id.customer_product_info_body);

                final ArrayList<String> temp = new ArrayList<>();
                final Gson gson = new Gson();

                temp.clear();
                temp.add(customerProduct.getECRNumber());

                AgentConsignments agentConsignments = new AgentConsignments(temp);

                final Bundle bundle = new Bundle();
                bundle.putString(API.KEY_JSON_SEARCH_PAYLOAD, gson.toJson(agentConsignments));

                customerProductInfoBody.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {

                        SearchDetailFragment searchDetailFragment = new SearchDetailFragment();
                        searchDetailFragment.setArguments(bundle);

                        ((MainActivity) getActivity()).getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.content_main, searchDetailFragment)
                                .addToBackStack(null)
                                .commit();

                    }
                });

            }

            @Override
            public void onLongClick(View view, int position) {


            }
        }));

        return rootView;
    }

    private void showFeed(ArrayList<CustomerProduct> customerProductArrayList) {

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        customerProductListAdapter = new CustomerProductListAdapter(getActivity(), customerProductArrayList, Config.TAG_SEARCH_DETAIL_MULTIPLE_FRAGMENT);
        recyclerView.setAdapter(customerProductListAdapter);

    }

    private void fetchData() {

        String url = null;
        RequestQueue queue = null;
        // Instantiate the RequestQueue.
        if (getContext() != null) {
            queue = Volley.newRequestQueue(getContext());
            if (((MainActivity) getContext()).userGroup.equals(API.VALUE_USER_GROUP_MANAGER))
                url = API.baseUrl + API.managerParcelDetail;
            else
                url = API.baseUrl + API.agentParcelDetail;
        }

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        progressBar.setVisibility(View.GONE);

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String code = jsonObject.getString(API.KEY_CODE);

                            if (code.equals(API.VALUE_CODE_SUCCESS)) {

                                customerProductListAdapter.listUpdated(dataFromServer(jsonObject));

                                JSONArray jsonArray = jsonObject.getJSONArray(API.KEY_JSON_DATA);
                                if (jsonArray.length() > 0)
                                    fetchData();

                            } else {

                                JSONObject error = jsonObject.getJSONObject(API.KEY_JSON_ERROR);

                                if (code.equals(API.VALUE_CODE_AUTHENTICATION_ERROR)) {

                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                } else if (code.equals(API.VALUE_CODE_PARAMETER_ERROR)) {
                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                try {

                    if (getContext() != null) {

                        if (error.getMessage().equals("com.android.volley.TimeoutError"))
                            Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_timeout), Toast.LENGTH_SHORT).show();

                        Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_unreachable), Toast.LENGTH_SHORT).show();
                    }

                    Log.v("Volly error : ", "" + error);

                } catch (Exception e) {

                    Log.v("Search detail E : ", " " + e);

                }
            }

        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                if (getContext() != null) {
                    params.put(API.KEY_JSON_USER_AUTHENTICATION, ((MainActivity) getContext()).authenticationKey);
                    params.put(API.KEY_JSON_USER_GROUP, ((MainActivity) getContext()).userGroup);
                    params.put(API.KEY_USER_ID, ((MainActivity) getContext()).userID);
                    params.put(API.KEY_CONTENT_TYPE, API.VALUE_CONTENT_VALUE_JSON);
                }

                return params;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                String your_string_json = json; // put your json
                return your_string_json.getBytes();
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                Config.volley_time_out,
                Config.max_retries,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Add the request to the RequestQueue.
        if (queue != null)
            queue.add(stringRequest);

    }

    private CustomerProduct[] dataFromServer(JSONObject jsonObject) throws JSONException {

        JSONArray jsonArray = jsonObject.getJSONArray(API.KEY_JSON_DATA);

        CustomerProduct[] customerProducts = new CustomerProduct[jsonArray.length()];

        for (int i = 0; i < jsonArray.length(); i++) {

            JSONObject customerProduct = jsonArray.getJSONObject(i);

            customerProducts[i] = new CustomerProduct(customerProduct.getString(API.KEY_CUSTOMER_PRODUCT_ECR_NUMBER_LOWER_CASE), customerProduct.getString(API.KEY_CUSTOMER_PRODUCT_CUSTOMER_NAME), customerProduct.getString(API.KEY_CUSTOMER_PRODUCT_CUSTOMER_ADDRESS), customerProduct.getString(API.KEY_CUSTOMER_PRODUCT_PRICE), customerProduct.getString(API.KEY_CUSTOMER_PRODUCT_MERCHANT_NAME), customerProduct.getString(API.KEY_CUSTOMER_PRODUCT_CUSTOMER_PHONE), customerProduct.getString(API.KEY_CUSTOMER_PRODUCT_MERCHANT_PHONE));

        }

        return customerProducts;
    }

}
