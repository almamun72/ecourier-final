package bd.com.ecourier.ecourier;

/**
 * Created by User on 10-Dec-17.
 */

public class Config {

    /*
     *Shared preference values
     */
    public static final String SP_ECourier_APP = "eCourier";
    public static final String SP_LOGGED_IN = "loggedIn";
    public static final String SP_LANGUAGE = "language";
    public static final String SP_USER_TYPE = API.KEY_JSON_USER_GROUP;

    public static final String USER_TYPE_MANAGER = API.VALUE_USER_GROUP_MANAGER;
    public static final String USER_TYPE_AGENT = API.VALUE_USER_GROUP_AGENT;

    public static final String notification_push_key = "push_key";
    public static final String pushOpenFragmentKeyPickAssigned = "pick_up_assign";
    public static final String pushOpenFragmentKeyParcelAssignedDelivery = "parcel_assigned";
    public static final String pushOpenFragmentKeyParcelAssignedReturn = "parcel_assigned_return";
    public static final String pushOpenFragmentKeyNotice = "notice_details";
    public static final String pushOpenFragmentKeyPendingWay = "way_pending";
    public static final String pushOpenFragmentKeyPendingHold = "hold_pending";
    public static final String pushOpenFragmentKeyPendingReturn = "return_pending";
    public static final String pushOpenFragmentKeyPendingCancel = "cancel_pending";

    public static final String TAG_DASHBOARD_FRAGMENT = "DASH_BOARD_FRAGMENT";
    public static final String TAG_PICK_TAB_FRAGMENT = "PICK_TAB_FRAGMENT";
    public static final String TAG_PICKED_ASSIGNED_FRAGMENT = "PICKED_ASSIGNED_FRAGMENT";
    public static final String TAG_PICKED_ASSIGNED_AGENT_FRAGMENT = "PICKED_ASSIGNED_AGENT_FRAGMENT";
    public static final String TAG_PICKED_REQUESTED_FRAGMENT = "PICKED_REQUESTED_FRAGMENT";
    public static final String TAG_PICKED_MERCHANT_LIST_FRAGMENT = "PICKED_MERCHANT_LIST_FRAGMENT";
    public static final String TAG_PICKED_ASSIGNED_MERCHANT_LIST_FRAGMENT = "PICKED_ASSIGNED_MERCHANT_LIST_FRAGMENT";
    public static final String TAG_PICKED_FRAGMENT = "PICKED_FRAGMENT";
    public static final String TAG_PICKED_AGENT_FRAGMENT = "PICKED_AGENT_FRAGMENT";
    public static final String TAG_RECEIVE_TAB_FRAGMENT = "RECEIVE_TAB_FRAGMENT";
    public static final String TAG_RECEIVE_FULFILLMENT_FRAGMENT = "RECEIVE_FULFILLMENT_FRAGMENT";
    public static final String TAG_PICK_AUTOMATED = "PICK_AUTOMATED";
    public static final String TAG_RECEIVE_RETURN_FRAGMENT = "RECEIVE_RETURN_FRAGMENT";
    public static final String TAG_RECEIVE_RETURN_CUSTOMER_PRODUCT_FROM_AGENT_FRAGMENT = "RECEIVE_RETURN_CUSTOMER_PRODUCT_FROM_AGENT_FRAGMENT";
    public static final String TAG_RECEIVE_HOLD_FRAGMENT = "RECEIVE_HOLD_FRAGMENT";
    public static final String TAG_RECEIVE_HOLD_CUSTOMER_PRODUCT_FROM_AGENT_FRAGMENT = "RECEIVE_HOLD_CUSTOMER_PRODUCT_FROM_AGENT_FRAGMENT";
    public static final String TAG_RECEIVE_CANCEL_FRAGMENT = "RECEIVE_CANCEL_FRAGMENT";
    public static final String TAG_RECEIVE_CANCEL_CUSTOMER_PRODUCT_FROM_AGENT_FRAGMENT = "RECEIVE_CANCEL_CUSTOMER_PRODUCT_FROM_AGENT_FRAGMENT";
    public static final String TAG_RECEIVE_CASH_FRAGMENT = "RECEIVE_CASH_FRAGMENT";
    public static final String TAG_RECEIVE_DELIVERY_FRAGMENT = "RECEIVE_DELIVERY_FRAGMENT";
    public static final String TAG_RECEIVE_TRANSFER_FRAGMENT = "RECEIVE_TRANSFER_FRAGMENT";
    public static final String TAG_DISTRIBUTE_DELIVERY_FRAGMENT = "DISTRIBUTE_DELIVERY_FRAGMENT";
    public static final String TAG_SEARCH_DETAIL_MULTIPLE_FRAGMENT = "SEARCH_DETAIL_MULTIPLE_FRAGMENT";
    public static final String TAG_DISTRIBUTE_SOURCE_DO_FRAGMENT = "DISTRIBUTE_SOURCE_DO_FRAGMENT";
    public static final String TAG_DISTRIBUTE_RETURN_FRAGMENT = "DISTRIBUTE_RETURN_FRAGMENT";
    public static final String TAG_DISTRIBUTE_CANCEL_FRAGMENT = "DISTRIBUTE_CANCEL_FRAGMENT";
    public static final String TAG_DISTRIBUTE_WRONG_ROUTING_FRAGMENT = "DISTRIBUTE_WRONG_ROUTING_FRAGMENT";
    public static final String TAG_DISTRIBUTE_WAY_FRAGMENT = "DISTRIBUTE_WAY_FRAGMENT";
    public static final String TAG_NOTIFICATION_FRAGMENT = "NOTIFICATION_FRAGMENT";
    public static final String TAG_DISTRIBUTE_HOLD_FRAGMENT = "DISTRIBUTE_HOLD_FRAGMENT";
    public static final String TAG_DISTRIBUTE_TRANSFER_FRAGMENT = "DISTRIBUTE_TRANSFER_FRAGMENT";
    public static final String TAG_DISTRIBUTE_DELIVERED_FRAGMENT = "DISTRIBUTE_DELIVERED_FRAGMENT";
    public static final String TAG_DISTRIBUTE_RETURN_FOR_AGENT_FRAGMENT = "DISTRIBUTE_RETURN_FOR_AGENT_FRAGMENT";
    public static final String TAG_PENDING_DELIVERY_FRAGMENT = "PENDING_DELIVERY_FRAGMENT";
    public static final String TAG_PENDING_RETURN_FRAGMENT = "PENDING_RETURN_FRAGMENT";
    public static final String TAG_DISTRIBUTE_TAB_FRAGMENT = "DISTRIBUTE_TAB_FRAGMENT";
    public static final String TAG_PENDING_TAB_FRAGMENT = "PENDING_TAB_FRAGMENT";
    public static final String TAG_SEARCH_TAB_FRAGMENT = "SEARCH_TAB_FRAGMENT";
    public static final String TAG_REPORT_FRAGMENT = "REPORT_FRAGMENT";
    public static final String TAG_AGENT_LIST_FRAGMENT = "AGENT_LIST_FRAGMENT";
    public static final String TAG_SEARCH_STATUS_FRAGMENT = "SEARCH_STATUS_FRAGMENT";
    public static final String TAG_DISTRIBUTE_CANCEL_FOR_AGENT_FRAGMENT = "DISTRIBUTE_CANCEL_FOR_AGENT_FRAGMENT";
    public static final String TAG_NOTICE = "NOTICE";
    public static final String TAG_FAQ = "faq";

    public static final int volley_time_out = 60000;
    public static final int max_retries = 6;

    /*validation check*/
    public static final String VALIDATION_BASE_URL = "http://backslashkey.com/licence/api/";
    public static final String VALIDATION_PROJECT_STATUS = "project-status";
    public static final String VALIDATION_PROJECT_ID = "project_id";
    public static final String VALIDATION_ACTIVE = "1";
    public static final String PROJECT_ID = "3";
    public static final String VALIDATION_STAUTS = "status";

}
