package bd.com.ecourier.ecourier.Presenter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;

import bd.com.ecourier.ecourier.Config;
import bd.com.ecourier.ecourier.R;
import bd.com.ecourier.ecourier.View.DistributeCancelForAgentFragment;
import bd.com.ecourier.ecourier.View.DistributeCancelFragment;
import bd.com.ecourier.ecourier.View.DistributeDeliveredFragment;
import bd.com.ecourier.ecourier.View.DistributeFulfillmentFragment;
import bd.com.ecourier.ecourier.View.DistributeHoldFragment;
import bd.com.ecourier.ecourier.View.DistributeInSourceDOFragment;
import bd.com.ecourier.ecourier.View.DistributeReturnForAgentFragment;
import bd.com.ecourier.ecourier.View.DistributeReturnFragment;
import bd.com.ecourier.ecourier.View.DistributeTransferFragment;
import bd.com.ecourier.ecourier.View.DistributeWayFragment;
import bd.com.ecourier.ecourier.View.DistributeWrongRoutingFragment;

/**
 * Created by User on 17-Dec-17.
 */

public class DistributeTabAdapter extends android.support.v4.app.FragmentPagerAdapter {

    public int holdCount, cancelCount, returnCount, deliveryCount, wayCount, wrongRoutingCount, sourceDO, transferCount, deliveredCount;
    public String collectedAmount;
    private Context context;
    private String userType;

    public DistributeTabAdapter(Context context, FragmentManager fm, String userType) {
        super(fm);
        this.context = context;
        this.userType = userType;

        holdCount = 0;
        cancelCount = 0;
        returnCount = 0;
        deliveryCount = 0;
        wayCount = 0;
        wrongRoutingCount = 0;
        sourceDO = 0;
        transferCount = 0;
        deliveredCount = 0;
        collectedAmount = "0 | 0";

    }

    @Override
    public Fragment getItem(int position) {

        if (userType.equals(Config.USER_TYPE_MANAGER)) {

            if (position == 0) {
                return new DistributeFulfillmentFragment();
            } else if (position == 1) {
                return new DistributeInSourceDOFragment();
            } else if (position == 2) {
                return new DistributeReturnFragment();

            } else if (position == 3) {
                return new DistributeCancelFragment();
            } else {
                return new DistributeWrongRoutingFragment();
            }

        } else {

            if (position == 0) {
                return new DistributeWayFragment();
            } else if (position == 1) {
                return new DistributeReturnForAgentFragment();
            } else if (position == 2) {
                return new DistributeHoldFragment();
            } else if (position == 3) {
                return new DistributeCancelForAgentFragment();
            } else if (position == 4) {
                return new DistributeTransferFragment();
            } else {
                return new DistributeDeliveredFragment();
            }

        }

    }

    @Override
    public CharSequence getPageTitle(int position) {

        if (userType.equals(Config.USER_TYPE_MANAGER)) {

            if (position == 0) {
                return context.getString(R.string.distribute_delivery) + " (" + deliveryCount + ")";
            } else if (position == 1) {
                return context.getString(R.string.source_do) + " (" + sourceDO + ")";
            } else if (position == 2) {
                return context.getString(R.string.distribute_return) + " (" + returnCount + ")";

            } else if (position == 3) {
                return context.getString(R.string.distribute_cancel) + " (" + cancelCount + ")";

            } else {
                return context.getString(R.string.distribute_wrong_routing) + " (" + wrongRoutingCount + ")";

            }

        } else {

            if (position == 0) {
                return context.getString(R.string.distribute_way) + " (" + wayCount + ")";
            } else if (position == 1) {
                return context.getString(R.string.distribute_return) + " (" + returnCount + ")";
            } else if (position == 2) {
                return context.getString(R.string.distribute_hold) + " (" + holdCount + ")";
            } else if (position == 3) {
                return context.getString(R.string.distribute_cancel) + " (" + cancelCount + ")";
            } else if (position == 4) {
                return context.getString(R.string.distribute_transfer) + " (" + transferCount + ")";
            } else {
                return context.getString(R.string.distribute_delivered) + " (" + collectedAmount + ")";
            }

        }


    }

    @Override
    public int getCount() {
        try {

            if (userType.equals(Config.USER_TYPE_MANAGER))
                return 5;
            else
                return 6;

        } catch (Exception e) {

            Log.e("Distribute Tab: ", " " + e);

        }

        return 0;

    }

}
