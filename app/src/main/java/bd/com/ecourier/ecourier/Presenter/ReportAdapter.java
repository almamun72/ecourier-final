package bd.com.ecourier.ecourier.Presenter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;

import bd.com.ecourier.ecourier.Model.Agent;
import bd.com.ecourier.ecourier.R;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by User on 11-Dec-17.
 */

public class ReportAdapter extends RecyclerView.Adapter<ReportAdapter.ReportAdapterHolder> {

    private LayoutInflater inflater;
    private Activity activity;
    private ArrayList<Agent> agentArrayList;
    private String fragmentTag;

    public ReportAdapter(Activity activity, ArrayList<Agent> agentArrayList, String fragmentTag) {

        inflater = LayoutInflater.from(activity);
        this.activity = activity;
        this.agentArrayList = agentArrayList;
        this.fragmentTag = fragmentTag;

    }

    @Override
    public ReportAdapterHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ReportAdapterHolder(inflater.inflate(R.layout.agent_info_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ReportAdapterHolder holder, int position) {

        Agent agent = agentArrayList.get(position);

        holder.name.setText(String.format("%s%s", activity.getResources().getString(R.string.agent_name), agent.getName()));
        holder.agentName = agent.getName();
        holder.id.setText(String.format("%s%s", activity.getResources().getString(R.string.agent_id), agent.getId()));
        holder.agentId = agent.getId();
        holder.productCount.setText(String.format("%s%s", activity.getResources().getString(R.string.total_product), String.valueOf(agent.getProductCount())));
        holder.phoneNumber = agent.getPhoneNumber();
        holder.consignments = agent.getConsignments();

        holder.collectedPrice.setText(String.format("%s%s", activity.getResources().getString(R.string.total_collected), agent.getTotalCollectedPrice()));
        holder.totalPrice.setText(String.format("%s%s", activity.getResources().getString(R.string.total), agent.getTotalProductPrice()));

    }

    @Override
    public int getItemCount() {
        return agentArrayList.size();
    }

    public void listUpdated(Agent[] agents) {

        agentArrayList.clear();
        Collections.addAll(agentArrayList, agents);
        notifyDataSetChanged();

    }

    class ReportAdapterHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.agent_name)
        TextView name;
        @BindView(R.id.agent_id)
        TextView id;
        @BindView(R.id.agent_product_count)
        TextView productCount;
        @BindView(R.id.agent_call_btn)
        ImageView callBtn;
        @BindView(R.id.agent_list_body)
        RelativeLayout agentListBody;
        @BindView(R.id.collected_price)
        TextView collectedPrice;
        @BindView(R.id.total_price)
        TextView totalPrice;

        private String phoneNumber, agentId, agentName;
        private ArrayList<String> consignments;

        ReportAdapterHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }


    }
}
