package bd.com.ecourier.ecourier.View;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bd.com.ecourier.ecourier.API;
import bd.com.ecourier.ecourier.Config;
import bd.com.ecourier.ecourier.Model.AgentConsignments;
import bd.com.ecourier.ecourier.Model.CustomerProductCashInfo;
import bd.com.ecourier.ecourier.Presenter.CustomerProductCashInfoListAdapter;
import bd.com.ecourier.ecourier.R;
import bd.com.ecourier.ecourier.clickListener.ClickListener;
import bd.com.ecourier.ecourier.clickListener.RecyclerTouchListener;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static android.graphics.Paint.ANTI_ALIAS_FLAG;

/**
 * Created by User on 18-Dec-17.
 */

public class ReceiveCashFromAgentFragment extends Fragment {

    @BindView(R.id.swipe_down_to_refresh_list)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.recyclerView_list)
    RecyclerView recyclerView;
    @BindView(R.id.fab_scan)
    FloatingActionButton QRScan;
    @BindView(R.id.recyclerView_list_notice)
    TextView noListNotice;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    private CustomerProductCashInfoListAdapter customerProductCashInfoListAdapter;
    private View rootView;
    private ArrayList<String> qrScanCodes = new ArrayList<>();

    private ArrayList<CustomerProductCashInfo> customerProductCashInfoArrayListForServer = new ArrayList<>();
    private ArrayList<CustomerProductCashInfo> customerProductCashInfoArrayList;
    private ArrayList<String> customerProductCashInfoECRArrayListForServer = new ArrayList<>();
    private CustomerProductCashInfo customerProductCashInfo;
    private Gson gson = new Gson();
    private ArrayList<String> consignments = new ArrayList<>();

    private IntentIntegrator intentIntegrator;

    public ReceiveCashFromAgentFragment() {
    }

    public static Bitmap textAsBitmap(String text, float textSize, int textColor) {
        Paint paint = new Paint(ANTI_ALIAS_FLAG);
        paint.setTextSize(textSize);
        paint.setColor(textColor);
        paint.setTextAlign(Paint.Align.LEFT);
        float baseline = -paint.ascent(); // ascent() is negative
        int width = (int) (paint.measureText(text) + 0.0f); // round
        int height = (int) (baseline + paint.descent() + 0.0f);
        Bitmap image = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(image);
        canvas.drawText(text, 0, baseline, paint);
        return image;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {

            consignments = getArguments().getStringArrayList(API.KEY_JSON_CONSIGNMENTS);

        }

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.recycle_view_list_layout, container, false);
        ButterKnife.bind(this, rootView);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                fetchListData();

            }
        });

        customerProductCashInfoArrayList = new ArrayList<>();

        showFeed(customerProductCashInfoArrayList);
        recyclerView.setVisibility(View.GONE);
        noListNotice.setVisibility(View.VISIBLE);

        if (getContext() != null)
            if (((MainActivity) getContext()).connectedTOInternet)
                fetchListData();

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, final int position) {

                customerProductCashInfo = customerProductCashInfoArrayList.get(position);
                ImageView checkBox = view.findViewById(R.id.checkbox);

                checkBox.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {

                        selectMerchantItem();
                        customerProductCashInfoListAdapter.notifyItemChanged(position);

                    }
                });

            }

            @Override
            public void onLongClick(View view, int position) {

                /*customerProductCashInfo = customerProductCashInfoArrayList.get(position);

                selectMerchantItem();
                customerProductCashInfoListAdapter.notifyItemChanged(position);*/

            }
        }));

        intentIntegrator = IntentIntegrator.forSupportFragment(this);
        intentIntegrator.setCaptureLayout(R.layout.barcode_scanner);
        intentIntegrator.setResultDisplayDuration(0);

        return rootView;
    }

    private void selectMerchantItem() {

        customerProductCashInfo.setChecked(!customerProductCashInfo.isChecked());
        if (customerProductCashInfo.isChecked())
            customerProductCashInfoArrayListForServer.add(customerProductCashInfo);
        else
            customerProductCashInfoArrayListForServer.remove(customerProductCashInfo);

        if (customerProductCashInfoArrayListForServer.size() == 0) {
            QRScan.setImageResource(R.drawable.cycle);

        } else {

            QRScan.show();
            QRScan.setImageBitmap(textAsBitmap("" + customerProductCashInfoArrayListForServer.size(), 40, Color.WHITE));

        }

    }

    private void fetchListData() {

        progressBar.setVisibility(View.VISIBLE);

        customerProductCashInfoECRArrayListForServer.clear();
        customerProductCashInfoArrayListForServer.clear();

        AgentConsignments agentConsignments = new AgentConsignments(consignments);
        Gson gson = new Gson();
        final String json = gson.toJson(agentConsignments);

        RequestQueue queue = null;
        // Instantiate the RequestQueue.
        if (getContext() != null)
            queue = Volley.newRequestQueue(getContext());
        String url = API.baseUrl + API.managerParcelDetail;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        progressBar.setVisibility(View.GONE);

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String code = jsonObject.getString(API.KEY_CODE);

                            if (code.equals(API.VALUE_CODE_SUCCESS)) {

                                customerProductCashInfoListAdapter.listUpdated(dataFromServer(jsonObject));
                                recyclerView.setVisibility(View.VISIBLE);
                                noListNotice.setVisibility(View.GONE);
                                swipeRefreshLayout.setRefreshing(false);

                            } else {

                                JSONObject error = jsonObject.getJSONObject(API.KEY_JSON_ERROR);

                                if (code.equals(API.VALUE_CODE_AUTHENTICATION_ERROR)) {

                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                } else if (code.equals(API.VALUE_CODE_PARAMETER_ERROR)) {
                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                swipeRefreshLayout.setRefreshing(false);

                try {

                    if (getContext() != null) {

                        if (error.getMessage().equals("com.android.volley.TimeoutError"))
                            Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_timeout), Toast.LENGTH_SHORT).show();

                        Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_unreachable), Toast.LENGTH_SHORT).show();
                    }

                    Log.v("Volly error : ", "" + error);

                } catch (Exception e) {

                    Log.v("Receive Cash Agent E : ", " " + e);

                }
            }

        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                if (getContext() != null) {
                    params.put(API.KEY_JSON_USER_AUTHENTICATION, ((MainActivity) getContext()).authenticationKey);
                    params.put(API.KEY_JSON_USER_GROUP, ((MainActivity) getContext()).userGroup);
                    params.put(API.KEY_USER_ID, ((MainActivity) getContext()).userID);
                    params.put(API.KEY_CONTENT_TYPE, API.VALUE_CONTENT_VALUE_JSON);
                }

                return params;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                String your_string_json = json; // put your json
                return your_string_json.getBytes();
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                Config.volley_time_out,
                Config.max_retries,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Add the request to the RequestQueue.
        if (queue != null)
            queue.add(stringRequest);

    }

    private CustomerProductCashInfo[] dataFromServer(JSONObject jsonObject) throws JSONException {

        JSONArray jsonArray = jsonObject.getJSONArray(API.KEY_JSON_DATA);

        CustomerProductCashInfo[] customerProductCashInfos = new CustomerProductCashInfo[jsonArray.length()];

        for (int i = 0; i < jsonArray.length(); i++) {

            JSONObject customerProduct = jsonArray.getJSONObject(i);

            customerProductCashInfos[i] = new CustomerProductCashInfo(customerProduct.getString(API.KEY_CUSTOMER_PRODUCT_ECR_NUMBER_LOWER_CASE), customerProduct.getString(API.KEY_CUSTOMER_PRODUCT_CUSTOMER_NAME), customerProduct.getString(API.KEY_CUSTOMER_PRODUCT_PRODUCT_PRICE), customerProduct.getString(API.KEY_CUSTOMER_PRODUCT_COLLECTED_AMOUNT), customerProduct.getString(API.KEY_CUSTOMER_PRODUCT_CUSTOMER_PHONE), customerProduct.getString(API.KEY_CUSTOMER_PRODUCT_PAYMENT_TYPE));

        }

        return customerProductCashInfos;
    }

    private void showFeed(ArrayList<CustomerProductCashInfo> customerProductCashInfos) {

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        customerProductCashInfoListAdapter = new CustomerProductCashInfoListAdapter(getActivity(), customerProductCashInfos, Config.TAG_DISTRIBUTE_CANCEL_FOR_AGENT_FRAGMENT);
        recyclerView.setAdapter(customerProductCashInfoListAdapter);

    }

    @OnClick(R.id.fab_scan)
    public void Onclick(View view) {

        if (customerProductCashInfoArrayListForServer.size() == 0) {
            intentIntegrator.addExtra("PROMPT_MESSAGE", "Total : " + qrScanCodes.size());
            intentIntegrator.initiateScan();
        } else if (customerProductCashInfoArrayListForServer.size() > 0) {

            customerProductCashInfoECRArrayListForServer.clear();

            for (int i = 0; i < customerProductCashInfoArrayListForServer.size(); i++) {

                customerProductCashInfoECRArrayListForServer.add(customerProductCashInfoArrayListForServer.get(i).getECRNumber());

            }

            if (getContext() != null) {

                AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                builder1.setCancelable(false);
                builder1.setMessage(getContext().getResources().getString(R.string.no_of_item) + customerProductCashInfoECRArrayListForServer.size());

                builder1.setPositiveButton(
                        getContext().getResources().getString(R.string.confirm_parcel),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                            }
                        }).setNegativeButton(getContext().getResources().getString(R.string.back), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                        customerProductCashInfoECRArrayListForServer.clear();
                        dialog.cancel();
                    }
                });

                final AlertDialog alert11 = builder1.create();
                alert11.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface arg0) {
                        alert11.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.RED);
                        alert11.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getContext().getResources().getColor(R.color.colorPrimary));

                        alert11.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                JSONObject consignment_detail = new JSONObject();
                                JSONArray arr = new JSONArray();

                                for (int i = 0; i < customerProductCashInfoECRArrayListForServer.size(); i++) {

                                    for (int j = 0; j < customerProductCashInfoArrayList.size(); j++) {

                                        if (customerProductCashInfoArrayList.get(j).getECRNumber().equals(customerProductCashInfoArrayListForServer.get(i).getECRNumber())) {

                                            try {
                                                JSONObject consignment_and_amount = new JSONObject();

                                                consignment_and_amount.put("consignment_no",
                                                        customerProductCashInfoArrayList.get(j).getECRNumber());

                                                consignment_and_amount.put("amount",
                                                        customerProductCashInfoArrayList.get(j).getCollectedPrice());

                                                arr.put(consignment_and_amount);

                                                break;

                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }

                                        }

                                    }

                                }

                                try {
                                    consignment_detail.put("consignment_detail", (Object) arr);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                updateStatus(consignment_detail.toString());
                                alert11.dismiss();

                            }

                        });

                    }
                });

                alert11.show();

            }
        }

    }

    private void updateStatus(final String data) {

        final ProgressDialog dialog = ProgressDialog.show(getActivity(), "",
                getActivity().getResources().getString(R.string.loading), true);

        dialog.show();

        RequestQueue queue = null;
        // Instantiate the RequestQueue.
        if (getContext() != null)
            queue = Volley.newRequestQueue(getContext());
        String url = API.baseUrl + API.managerPickCash;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        dialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String code = jsonObject.getString(API.KEY_CODE);


                            if (code.equals(API.VALUE_CODE_SUCCESS)) {

                                JSONObject data = jsonObject.getJSONObject(API.KEY_JSON_DATA);

                                JSONArray updated = data.getJSONArray(API.KEY_JSON_UPDATED);
                                JSONArray failed = data.getJSONArray(API.KEY_JSON_FAILED);

                                if (updated.length() > 0) {
                                    Toast.makeText(getContext(), "SUCCESS", Toast.LENGTH_SHORT).show();

                                    for (int i = 0; i < customerProductCashInfoArrayListForServer.size(); i++) {

                                        for (int j = 0; j < consignments.size(); j++) {

                                            if (customerProductCashInfoArrayListForServer.get(i)
                                                    .getECRNumber().equals(consignments.get(j))) {

                                                consignments.remove(j);
                                                break;

                                            }

                                        }

                                    }

                                    customerProductCashInfoArrayListForServer.clear();
                                    customerProductCashInfoECRArrayListForServer.clear();
                                    QRScan.setImageResource(R.drawable.cycle);
                                    fetchListData();
                                } else if (failed.length() > 0) {
                                    Toast.makeText(getContext(), "FAILED", Toast.LENGTH_SHORT).show();

                                }

                            } else {

                                swipeRefreshLayout.setRefreshing(false);
                                dialog.dismiss();

                                JSONObject error = jsonObject.getJSONObject(API.KEY_JSON_ERROR);

                                if (code.equals(API.VALUE_CODE_AUTHENTICATION_ERROR)) {

                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                } else if (code.equals(API.VALUE_CODE_PARAMETER_ERROR)) {
                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                try {
                    if (getContext() != null) {

                        swipeRefreshLayout.setRefreshing(false);
                        dialog.dismiss();

                        if (error.getMessage().equals("com.android.volley.TimeoutError"))
                            Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_timeout), Toast.LENGTH_SHORT).show();

                        Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_unreachable), Toast.LENGTH_SHORT).show();
                    }

                    Log.v("Volly error : ", "" + error);

                } catch (Exception e) {

                    Log.v("Receive cash E : ", " " + e);

                }
            }

        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                if (getContext() != null) {

                    params.put(API.KEY_JSON_USER_AUTHENTICATION, ((MainActivity) getContext()).authenticationKey);
                    params.put(API.KEY_JSON_USER_GROUP, ((MainActivity) getContext()).userGroup);
                    params.put(API.KEY_USER_ID, ((MainActivity) getContext()).userID);
                    params.put(API.KEY_CONTENT_TYPE, API.VALUE_CONTENT_VALUE_JSON);

                }

                return params;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                String your_string_json = data; // put your json
                return your_string_json.getBytes();
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                Config.volley_time_out,
                Config.max_retries,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Add the request to the RequestQueue.
        if (queue != null)
            queue.add(stringRequest);

    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {

        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (scanResult != null) {

            if (resultCode == RESULT_OK) {
                // contents contains whatever the code was
                String contents = scanResult.getContents();

                // Format contains the type of code i.e. UPC, EAN, QRCode etc...
                //String format = intent.getStringExtra("SCAN_RESULT_FORMAT");

                // Handle successful scan. In this example add contents to ArrayList
                if (contents != null) {
                    for (int i = 0; i < customerProductCashInfoArrayList.size(); i++) {

                        if (customerProductCashInfoArrayList.get(i).getECRNumber().equals(contents)) {

                            if (customerProductCashInfoArrayListForServer.size() > 0) {

                                Boolean found = false;

                                for (int j = 0; j < customerProductCashInfoArrayListForServer.size(); j++) {

                                    if (customerProductCashInfoArrayListForServer.get(j).getECRNumber().equals(contents)) {

                                        found = true;
                                        break;

                                    }

                                }

                                if (!found) {

                                    customerProductCashInfo = customerProductCashInfoArrayList.get(i);
                                    selectMerchantItem();
                                    customerProductCashInfoListAdapter.notifyItemChanged(i);
                                    qrScanCodes.add(contents);

                                }

                            } else {

                                customerProductCashInfo = customerProductCashInfoArrayList.get(i);
                                selectMerchantItem();
                                customerProductCashInfoListAdapter.notifyItemChanged(i);
                                qrScanCodes.add(contents);

                            }

                            break;

                        }

                    }

                }

                intentIntegrator.addExtra("PROMPT_MESSAGE", "Total : " + qrScanCodes.size());
                intentIntegrator.initiateScan();

            } else if (resultCode == RESULT_CANCELED) {

                qrScanCodes.clear();

            }


        } else {
            Toast toast = Toast.makeText(getContext(),
                    "No scan data received!", Toast.LENGTH_SHORT);
            toast.show();
        }

    }

}
