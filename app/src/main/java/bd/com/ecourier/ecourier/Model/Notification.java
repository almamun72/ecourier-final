package bd.com.ecourier.ecourier.Model;

/**
 * Created by User on 02-Jan-18.
 */

public class Notification {

    private String title, description;

    public Notification(String title, String description) {

        this.title = title;
        this.description = description;
        ;

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
