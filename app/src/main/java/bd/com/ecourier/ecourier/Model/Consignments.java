package bd.com.ecourier.ecourier.Model;

import android.graphics.Color;

/**
 * Created by User on 25-Dec-17.
 */

public class Consignments {

    private String consignment_no;
    private boolean isChecked;
    private int colorUnselected = Color.WHITE, colorSelected, textColor, colorWhite = Color.WHITE, colorBlack = Color.BLACK, color;

    public Consignments(String consignment_no) {

        this.consignment_no = consignment_no;
        isChecked = false;
        color = colorUnselected;
        textColor = colorBlack;
        colorSelected = Color.parseColor("#25b472");

    }

    public String getConsignment_no() {
        return consignment_no;
    }

    public void setConsignment_no(String consignment_no) {
        this.consignment_no = consignment_no;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;

        if (checked) {

            color = colorSelected;
            textColor = colorWhite;

        } else {

            color = colorUnselected;
            textColor = colorBlack;
        }
    }

    public int getTextColor() {
        return textColor;
    }

    public void setTextColor(int textColor) {
        this.textColor = textColor;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }
}
