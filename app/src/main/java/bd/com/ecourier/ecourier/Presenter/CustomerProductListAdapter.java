package bd.com.ecourier.ecourier.Presenter;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;

import bd.com.ecourier.ecourier.API;
import bd.com.ecourier.ecourier.Config;
import bd.com.ecourier.ecourier.Model.CustomerProduct;
import bd.com.ecourier.ecourier.R;
import bd.com.ecourier.ecourier.View.DistributeReturnDetailFragment;
import bd.com.ecourier.ecourier.View.DistributeWayDetailFragment;
import bd.com.ecourier.ecourier.View.MainActivity;
import bd.com.ecourier.ecourier.printer.PrintActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by User on 11-Dec-17.
 */

public class CustomerProductListAdapter extends RecyclerView.Adapter<CustomerProductListAdapter.CustomerProductListAdapterHolder> {

    private LayoutInflater inflater;
    private Activity activity;
    private ArrayList<CustomerProduct> customerProductArrayList;
    private ArrayList<String> consignments;
    private String fragmentTag, userType;

    public CustomerProductListAdapter(Activity activity, ArrayList<CustomerProduct> customerProductArrayList, String fragmentTag) {

        inflater = LayoutInflater.from(activity);
        this.activity = activity;
        this.customerProductArrayList = customerProductArrayList;
        this.fragmentTag = fragmentTag;

    }

    public CustomerProductListAdapter(Activity activity, ArrayList<CustomerProduct> customerProductArrayList, String fragmentTag, ArrayList<String> consignments) {

        inflater = LayoutInflater.from(activity);
        this.activity = activity;
        this.customerProductArrayList = customerProductArrayList;
        this.fragmentTag = fragmentTag;
        this.consignments = consignments;

    }

    @Override
    public CustomerProductListAdapterHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CustomerProductListAdapterHolder(inflater.inflate(R.layout.deliver_product_items, parent, false));
    }

    @Override
    public void onBindViewHolder(CustomerProductListAdapterHolder holder, int position) {

        CustomerProduct customerProduct = customerProductArrayList.get(position);

        holder.ecrNumber.setText(customerProduct.getECRNumber());
        holder.ecrNumber.setTextColor(customerProduct.getTextColor());
        holder.ecrNumber.setVisibility(View.GONE);

        holder.customerName.setText(customerProduct.getCustomerName());
        holder.customerName.setTextColor(customerProduct.getTextColor());

        holder.customerAddress.setText(customerProduct.getCustomerAddress());
        holder.customerAddress.setTextColor(customerProduct.getTextColor());
        holder.address = customerProduct.getCustomerAddress();


        holder.productPrice.setText(String.format("%s%s%s%s", activity.getResources().getString(R.string.product_price), " ", customerProduct.getPrice(), activity.getResources().getString(R.string.taka)));
        holder.productPrice.setTextColor(customerProduct.getTextColor());

        holder.merchantName.setText(customerProduct.getMerchantName());
        holder.merchantName.setTextColor(customerProduct.getTextColor());

        holder.customerPhoneNumber = customerProduct.getCustomerPhoneNumber();
        holder.merchantPhoneNumber = customerProduct.getMerchantPhoneNumber();

        /*holder.productChecked.setChecked(customerProduct.isChecked());*/

        if (customerProduct.isChecked()) {
            holder.productChecked.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.ic_check_box_black_24dp));
        } else {
            holder.productChecked.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.ic_check_box_outline_blank_black_24dp));
        }

        holder.customerView.setBackgroundColor(customerProduct.getColor());
        holder.customerAddress.setMaxLines(customerProduct.getMaxLine());

        holder.cancelRequest.setVisibility(View.GONE);
        holder.customerOptions.setVisibility(View.GONE);
        holder.detail.setVisibility(View.GONE);
        holder.print.setVisibility(View.GONE);
        holder.productLastStatus.setVisibility(View.GONE);

        holder.merchantInfo.setVisibility(View.GONE);

        if (fragmentTag.equals(Config.TAG_DISTRIBUTE_DELIVERED_FRAGMENT) || fragmentTag.equals(Config.TAG_DISTRIBUTE_HOLD_FRAGMENT) || fragmentTag.equals(Config.TAG_DISTRIBUTE_TRANSFER_FRAGMENT) || fragmentTag.equals(Config.TAG_DISTRIBUTE_CANCEL_FOR_AGENT_FRAGMENT) || fragmentTag.equals(Config.TAG_DISTRIBUTE_TRANSFER_FRAGMENT) || fragmentTag.equals(Config.TAG_PENDING_DELIVERY_FRAGMENT) || fragmentTag.equals(Config.TAG_DISTRIBUTE_TRANSFER_FRAGMENT) || fragmentTag.equals(Config.TAG_PENDING_RETURN_FRAGMENT) || fragmentTag.equals(Config.TAG_DISTRIBUTE_WAY_FRAGMENT) || fragmentTag.equals(Config.TAG_DISTRIBUTE_RETURN_FOR_AGENT_FRAGMENT)) {

            holder.productChecked.setVisibility(View.GONE);
            holder.productLastStatus.setVisibility(View.GONE);
        } else {
            holder.productChecked.setVisibility(View.VISIBLE);

        }

        if (fragmentTag.equals(Config.TAG_RECEIVE_CANCEL_CUSTOMER_PRODUCT_FROM_AGENT_FRAGMENT) || fragmentTag.equals(Config.TAG_RECEIVE_HOLD_CUSTOMER_PRODUCT_FROM_AGENT_FRAGMENT) || fragmentTag.equals(Config.TAG_RECEIVE_RETURN_CUSTOMER_PRODUCT_FROM_AGENT_FRAGMENT) || fragmentTag.equals(Config.TAG_DISTRIBUTE_SOURCE_DO_FRAGMENT)) {

            holder.customerOptions.setVisibility(View.VISIBLE);
            holder.cancelRequest.setVisibility(View.VISIBLE);
            holder.detail.setVisibility(View.GONE);
            holder.print.setVisibility(View.GONE);

            if (fragmentTag.equals(Config.TAG_RECEIVE_HOLD_CUSTOMER_PRODUCT_FROM_AGENT_FRAGMENT)) {

                holder.productLastStatus.setVisibility(View.VISIBLE);

                if (customerProduct.getLastState().equals("S21"))
                    holder.productLastStatus.setText(String.format("%s: %s", activity.getResources().getString(R.string.From), activity.getResources().getString(R.string.distribute_delivery)));
                else
                    holder.productLastStatus.setText(String.format("%s: %s", activity.getResources().getString(R.string.From), activity.getResources().getString(R.string.receive_return)));

            } else {
                holder.productLastStatus.setVisibility(View.GONE);
            }

            holder.lastStatus = customerProduct.getLastState();

        } else if (fragmentTag.equals(Config.TAG_DISTRIBUTE_WAY_FRAGMENT) || fragmentTag.equals(Config.TAG_DISTRIBUTE_RETURN_FOR_AGENT_FRAGMENT)) {

            holder.customerOptions.setVisibility(View.VISIBLE);
            holder.cancelRequest.setVisibility(View.GONE);
            holder.detail.setVisibility(View.VISIBLE);
            holder.print.setVisibility(View.VISIBLE);
            holder.productLastStatus.setVisibility(View.GONE);

        } else if (fragmentTag.equals(Config.TAG_RECEIVE_FULFILLMENT_FRAGMENT)) {

            holder.customerOptions.setVisibility(View.VISIBLE);
            holder.cancelRequest.setVisibility(View.GONE);
            holder.detail.setVisibility(View.GONE);
            holder.print.setVisibility(View.VISIBLE);
            holder.productLastStatus.setVisibility(View.GONE);

        }

        if (fragmentTag.equals(Config.TAG_SEARCH_DETAIL_MULTIPLE_FRAGMENT)) {

            holder.productChecked.setVisibility(View.GONE);
            holder.ecrNumber.setVisibility(View.VISIBLE);
            holder.merchantInfo.setVisibility(View.VISIBLE);

        }

    }

    @Override
    public int getItemCount() {
        return customerProductArrayList.size();
    }

    public void listUpdated(CustomerProduct[] customerProducts) {

        if (customerProducts.length > 0) {

            Collections.addAll(customerProductArrayList, customerProducts);
            notifyDataSetChanged();
        }

    }

    public void clearList() {

        customerProductArrayList.clear();
        notifyDataSetChanged();

    }

    class CustomerProductListAdapterHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.product_ecr_number)
        TextView ecrNumber;
        @BindView(R.id.product_customer_name)
        TextView customerName;
        @BindView(R.id.customer_address)
        TextView customerAddress;
        @BindView(R.id.product_price)
        TextView productPrice;
        @BindView(R.id.merchant_name)
        TextView merchantName;
        @BindView(R.id.product_last_status)
        TextView productLastStatus;
        @BindView(R.id.product_select_checkbox)
        ImageView productChecked;
        @BindView(R.id.way_detail)
        ImageView detail;
        @BindView(R.id.parcel_print)
        ImageView print;
        @BindView(R.id.cancel_request)
        ImageView cancelRequest;
        @BindView(R.id.customer_view)
        RelativeLayout customerView;
        @BindView(R.id.customer_options)
        LinearLayout customerOptions;
        @BindView(R.id.merchant_info)
        RelativeLayout merchantInfo;
        @BindView(R.id.customer_phone_number)
        ImageView callCustomer;
        @BindView(R.id.merchant_phone_number)
        ImageView callMerchant;
        @BindView(R.id.location_icon)
        ImageView mapIcon;

        private String customerPhoneNumber, merchantPhoneNumber, lastStatus, address;

        CustomerProductListAdapterHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick({R.id.customer_product_info_body, R.id.product_select_checkbox, R.id.way_detail, R.id.cancel_request, R.id.customer_phone_number, R.id.merchant_phone_number, R.id.location_icon, R.id.parcel_print})
        public void onClick(View view) {

            Bundle bundle = new Bundle();

            switch (view.getId()) {

                case (R.id.customer_product_info_body): {

                    if (!fragmentTag.equals(Config.TAG_SEARCH_DETAIL_MULTIPLE_FRAGMENT)) {

                        if (customerAddress.getMaxLines() == 3) {
                            customerAddress.setMaxLines(8);
                            ecrNumber.setVisibility(View.VISIBLE);
                            merchantInfo.setVisibility(View.VISIBLE);
                        } else {
                            customerAddress.setMaxLines(3);
                            ecrNumber.setVisibility(View.GONE);
                            merchantInfo.setVisibility(View.GONE);
                        }

                    }

                    break;

                }

                case (R.id.way_detail): {

                    bundle.putString(API.KEY_JSON_CONSIGNMENTS, ecrNumber.getText().toString());

                    if (fragmentTag.equals(Config.TAG_DISTRIBUTE_WAY_FRAGMENT)) {

                        DistributeWayDetailFragment distributeWayDetailFragment = new DistributeWayDetailFragment();
                        distributeWayDetailFragment.setArguments(bundle);

                        ((MainActivity) activity).getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.content_main, distributeWayDetailFragment)
                                .addToBackStack(null)
                                .commit();

                    } else if (fragmentTag.equals(Config.TAG_DISTRIBUTE_RETURN_FOR_AGENT_FRAGMENT)) {

                        DistributeReturnDetailFragment distributeReturnDetailFragment = new DistributeReturnDetailFragment();
                        distributeReturnDetailFragment.setArguments(bundle);

                        ((MainActivity) activity).getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.content_main, distributeReturnDetailFragment)
                                .addToBackStack(null)
                                .commit();

                    }

                    break;

                }

                case R.id.customer_phone_number: {

                    int REQUEST_PHONE_CALL = 1;

                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + customerPhoneNumber));

                    if (ContextCompat.checkSelfPermission(activity,
                            Manifest.permission.CALL_PHONE)
                            != PackageManager.PERMISSION_GRANTED) {

                        // Permission is not granted
                        // Should we show an explanation?
                        if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                                Manifest.permission.CALL_PHONE)) {

                            // Show an explanation to the user *asynchronously* -- don't block
                            // this thread waiting for the user's response! After the user
                            // sees the explanation, try again to request the permission.

                            ActivityCompat.requestPermissions(activity,
                                    new String[]{Manifest.permission.CALL_PHONE},
                                    REQUEST_PHONE_CALL);

                        } else {

                            // No explanation needed; request the permission
                            ActivityCompat.requestPermissions(activity,
                                    new String[]{Manifest.permission.CALL_PHONE},
                                    REQUEST_PHONE_CALL);

                            // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                            // app-defined int constant. The callback method gets the
                            // result of the request.
                        }
                    } else {
                        // Permission has already been granted
                        activity.startActivity(callIntent);


                    }

                    break;
                }

                case R.id.merchant_phone_number: {

                    int REQUEST_PHONE_CALL = 1;

                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + customerPhoneNumber));

                    if (ContextCompat.checkSelfPermission(activity,
                            Manifest.permission.CALL_PHONE)
                            != PackageManager.PERMISSION_GRANTED) {

                        // Permission is not granted
                        // Should we show an explanation?
                        if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                                Manifest.permission.CALL_PHONE)) {

                            // Show an explanation to the user *asynchronously* -- don't block
                            // this thread waiting for the user's response! After the user
                            // sees the explanation, try again to request the permission.

                            ActivityCompat.requestPermissions(activity,
                                    new String[]{Manifest.permission.CALL_PHONE},
                                    REQUEST_PHONE_CALL);

                        } else {

                            // No explanation needed; request the permission
                            ActivityCompat.requestPermissions(activity,
                                    new String[]{Manifest.permission.CALL_PHONE},
                                    REQUEST_PHONE_CALL);

                            // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                            // app-defined int constant. The callback method gets the
                            // result of the request.
                        }
                    } else {
                        // Permission has already been granted
                        activity.startActivity(callIntent);


                    }

                    break;
                }

                case R.id.location_icon: {

                    //Uri gmmIntentUri = Uri.parse("geo:0,0?q=1600 Amphitheatre Parkway, Mountain+View, California");
                    Uri gmmIntentUri = Uri.parse("geo:0,0?q=" + Uri.encode(address));
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    activity.startActivity(mapIntent);

                    break;
                }

                case R.id.parcel_print: {

                    Intent printIntent = new Intent(activity, PrintActivity.class);
                    printIntent.putExtra(API.KEY_JSON_CONSIGNMENTS, ecrNumber.getText().toString());
                    activity.startActivity(printIntent);

                    break;
                }

            }

        }


    }
}
