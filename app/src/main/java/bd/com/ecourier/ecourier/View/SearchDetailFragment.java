package bd.com.ecourier.ecourier.View;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bd.com.ecourier.ecourier.API;
import bd.com.ecourier.ecourier.Config;
import bd.com.ecourier.ecourier.Model.StatusHistory;
import bd.com.ecourier.ecourier.Presenter.SearchStatusAdapter;
import bd.com.ecourier.ecourier.R;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by User on 10-Dec-17.
 */

public class SearchDetailFragment extends Fragment {

    @BindView(R.id.product_ecr_number)
    TextView ecrNumber;
    @BindView(R.id.product_customer_name)
    TextView customerName;
    @BindView(R.id.customer_address)
    TextView customerAddress;
    @BindView(R.id.product_price)
    TextView productPrice;
    @BindView(R.id.merchant_name)
    TextView merchantName;
    @BindView(R.id.recyclerView_list)
    RecyclerView recyclerView;

    private View rootView;
    private ArrayList<StatusHistory> statusHistoryArrayList = new ArrayList<>();
    private SearchStatusAdapter searchStatusAdapter;
    private String json;

    public SearchDetailFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {

            json = getArguments().getString(API.KEY_JSON_SEARCH_PAYLOAD);

        }

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.search_detail_fragment, container, false);
        ButterKnife.bind(this, rootView);

        if (getContext() != null)
            if (((MainActivity) getContext()).connectedTOInternet)
                fetchData();

        showFeed(statusHistoryArrayList);

        return rootView;
    }

    private void showFeed(ArrayList<StatusHistory> statusHistoryArrayList) {

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        searchStatusAdapter = new SearchStatusAdapter(getActivity(), statusHistoryArrayList, Config.TAG_SEARCH_STATUS_FRAGMENT);
        recyclerView.setAdapter(searchStatusAdapter);

    }

    private void fetchData() {

        RequestQueue queue = null;
        // Instantiate the RequestQueue.
        if (getContext() != null)
            queue = Volley.newRequestQueue(getContext());
        String url = API.baseUrl + API.agentParcelDetail;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String code = jsonObject.getString(API.KEY_CODE);

                            if (code.equals(API.VALUE_CODE_SUCCESS)) {

                                dataFromServer(jsonObject);

                            } else {

                                JSONObject error = jsonObject.getJSONObject(API.KEY_JSON_ERROR);

                                if (code.equals(API.VALUE_CODE_AUTHENTICATION_ERROR)) {

                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                } else if (code.equals(API.VALUE_CODE_PARAMETER_ERROR)) {
                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                try {

                    if (getContext() != null) {

                        if (error.getMessage().equals("com.android.volley.TimeoutError"))
                            Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_timeout), Toast.LENGTH_SHORT).show();

                        Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_unreachable), Toast.LENGTH_SHORT).show();
                    }

                    Log.v("Volly error : ", "" + error);

                } catch (Exception e) {

                    Log.v("Search detail E : ", " " + e);

                }
            }

        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                if (getContext() != null) {
                    params.put(API.KEY_JSON_USER_AUTHENTICATION, ((MainActivity) getContext()).authenticationKey);
                    params.put(API.KEY_JSON_USER_GROUP, ((MainActivity) getContext()).userGroup);
                    params.put(API.KEY_USER_ID, ((MainActivity) getContext()).userID);
                    params.put(API.KEY_CONTENT_TYPE, API.VALUE_CONTENT_VALUE_JSON);
                }

                return params;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                String your_string_json = json; // put your json
                return your_string_json.getBytes();
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                Config.volley_time_out,
                Config.max_retries,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Add the request to the RequestQueue.
        if (queue != null)
            queue.add(stringRequest);

    }

    private void dataFromServer(JSONObject jsonObject) throws JSONException {

        JSONArray jsonArray = jsonObject.getJSONArray(API.KEY_JSON_DATA);

        JSONObject productDetail = jsonArray.getJSONObject(0);

        ecrNumber.setText(productDetail.getString(API.KEY_CUSTOMER_PRODUCT_ECR_NUMBER_LOWER_CASE));
        customerName.setText(productDetail.getString(API.KEY_CUSTOMER_PRODUCT_CUSTOMER_NAME));
        customerAddress.setText(productDetail.getString(API.KEY_CUSTOMER_PRODUCT_CUSTOMER_ADDRESS));
        productPrice.setText(productDetail.getString(API.KEY_CUSTOMER_PRODUCT_PRICE));
        merchantName.setText(productDetail.getString(API.KEY_CUSTOMER_PRODUCT_MERCHANT_NAME));

        JSONArray statusHistory = null;

        if (productDetail.has(API.KEY_MERCHANT_PRODUCT_STATUS_HISTORY)) {

            statusHistory = productDetail.getJSONArray(API.KEY_MERCHANT_PRODUCT_STATUS_HISTORY);
            StatusHistory[] statusHistories = new StatusHistory[statusHistory.length()];

            for (int i = 0; i < statusHistory.length(); i++) {

                JSONObject history = statusHistory.getJSONObject(i);

                statusHistories[i] = new StatusHistory(history.getString(API.KEY_STATUS_HISTORY_TIME), history.getString(API.KEY_STATUS_HISTORY_STATUS), history.getString(API.KEY_STATUS_HISTORY_UPDATER_ID), history.getString(API.KEY_STATUS_HISTORY_UPDATER_GROUP), history.getString(API.KEY_STATUS_HISTORY_SHIPPING_AGENT_ID), history.getString(API.KEY_COMMENT), history.getString(API.KEY_STATUS_HISTORY_BRANCH_ID));

            }

            searchStatusAdapter.listUpdated(statusHistories);

        }

    }

}
