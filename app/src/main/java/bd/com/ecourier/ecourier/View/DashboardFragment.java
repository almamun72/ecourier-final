package bd.com.ecourier.ecourier.View;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import bd.com.ecourier.ecourier.API;
import bd.com.ecourier.ecourier.Config;
import bd.com.ecourier.ecourier.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by User on 10-Dec-17.
 */

public class DashboardFragment extends Fragment {

    @BindView(R.id.dashboard_pending)
    RelativeLayout pending;
    @BindView(R.id.pending_image)
    ImageView pendingImage;
    @BindView(R.id.pending_text)
    TextView pendingText;
    @BindView(R.id.pick_request)
    TextView pickRequested;
    @BindView(R.id.pick_assign)
    TextView pickAssigned;
    @BindView(R.id.pick_picked)
    TextView pickPicked;
    @BindView(R.id.requested_holder)
    RelativeLayout requestedHolder;

    @BindView(R.id.receive_return_holder)
    RelativeLayout receiveReturnHolder;
    @BindView(R.id.receive_cash)
    TextView receiveCash;
    @BindView(R.id.receive_fulfillment)
    TextView receiveFulfillment;
    @BindView(R.id.receive_return)
    TextView receiveReturn;
    @BindView(R.id.receive_hold)
    TextView receiveHold;
    @BindView(R.id.receive_cancel)
    TextView receiveCancel;

    @BindView(R.id.receive_cash_text)
    TextView receiveCashText;
    @BindView(R.id.receive_fulfillment_text)
    TextView receiveFulfillmentText;

    @BindView(R.id.receive_hold_holder)
    RelativeLayout receiveHoldHolder;
    @BindView(R.id.receive_cancel_holder)
    RelativeLayout receiveCancelHolder;

    @BindView(R.id.distribute_delivery)
    TextView distributeDelivery;
    @BindView(R.id.distribute_return)
    TextView distributeReturn;
    @BindView(R.id.distribute_cancel)
    TextView distributeCancel;
    @BindView(R.id.distribute_wr)
    TextView distributeWR;
    @BindView(R.id.distribute_sdo)
    TextView distributeSDo;

    @BindView(R.id.distribute_sdo_text)
    TextView distributeSDOText;
    @BindView(R.id.distribute_wr_text)
    TextView distributeWRText;

    private String userType, userID, authenticationKey;

    public DashboardFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getActivity() != null) {
            userType = (getActivity()).getSharedPreferences(Config.SP_ECourier_APP, MODE_PRIVATE)
                    .getString(Config.SP_USER_TYPE, "");
            userID = (getActivity()).getSharedPreferences(Config.SP_ECourier_APP, MODE_PRIVATE)
                    .getString(API.KEY_USER_ID, "");
            authenticationKey = (getActivity()).getSharedPreferences(Config.SP_ECourier_APP, MODE_PRIVATE)
                    .getString(API.KEY_JSON_USER_AUTHENTICATION, "");


        }

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.dashboard, container, false);
        ButterKnife.bind(this, rootView);

        if (getActivity() != null) {

            getActivity().setTitle(getResources().getString(R.string.dashboard));

            if (userType.equals(API.VALUE_USER_GROUP_AGENT)) {
                pendingImage.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_notifications_white_24dp));
                pendingText.setText(getActivity().getResources().getString(R.string.notification));
                requestedHolder.setVisibility(View.GONE);
                receiveCancelHolder.setVisibility(View.GONE);
                receiveHoldHolder.setVisibility(View.GONE);
                receiveCashText.setText("D:");
                receiveFulfillmentText.setText("T:");
                distributeSDOText.setText("W:");
                distributeWRText.setText("H:");
            } else {
                receiveReturnHolder.setVisibility(View.GONE);
            }

            fetchPickListTotal();
            fetchParcelListTotal();

        }

        return rootView;

    }

    @OnClick({R.id.dashboard_pick, R.id.dashboard_distribute, R.id.dashboard_pending, R.id.dashboard_receive, R.id.notice, R.id.faq, R.id.search, R.id.report})
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.dashboard_pick: {

                if (getActivity() != null) {

                    getActivity().getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.content_main, ((MainActivity) getActivity()).instanceOfPickTab(), Config.TAG_PICK_TAB_FRAGMENT)
                            .addToBackStack(null)
                            .commit();
                }

                break;

            }

            case R.id.dashboard_receive: {

                if (getActivity() != null)
                    getActivity().getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.content_main, ((MainActivity) getActivity()).instanceOfReceiveTab(), Config.TAG_RECEIVE_TAB_FRAGMENT)
                            .addToBackStack(null)
                            .commit();

                break;

            }

            case R.id.dashboard_distribute: {

                if (getActivity() != null)
                    getActivity().getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.content_main, ((MainActivity) getActivity()).instanceOfDistributeTab(), Config.TAG_DISTRIBUTE_TAB_FRAGMENT)
                            .addToBackStack(null)
                            .commit();

                break;

            }

            case R.id.dashboard_pending: {

                if (getActivity() != null) {

                    if (userType.equals(API.VALUE_USER_GROUP_AGENT)) {

                        getActivity().getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.content_main, new NotificationFragment(), Config.TAG_NOTIFICATION_FRAGMENT)
                                .addToBackStack(null)
                                .commit();

                    } else {

                        getActivity().getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.content_main, ((MainActivity) getActivity()).instanceOfPendingTab(), Config.TAG_PENDING_TAB_FRAGMENT)
                                .addToBackStack(null)
                                .commit();

                    }

                }

                break;

            }

            case R.id.notice: {

                if (getActivity() != null)
                    getActivity().getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.content_main, new NoticeFragment(), Config.TAG_PENDING_TAB_FRAGMENT)
                            .addToBackStack(null)
                            .commit();

                break;

            }

            case R.id.faq: {

                if (getActivity() != null)
                    getActivity().getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.content_main, new FAQFragment(), Config.TAG_PENDING_TAB_FRAGMENT)
                            .addToBackStack(null)
                            .commit();

                break;

            }

            case R.id.search: {

                if (getActivity() != null)
                    getActivity().getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.content_main, new SearchFragment(), Config.TAG_SEARCH_TAB_FRAGMENT)
                            .addToBackStack(null)
                            .commit();

                break;

            }

            case R.id.report: {

                if (getActivity() != null) {

                    if (userType.equals(API.VALUE_USER_GROUP_AGENT)) {

                        getActivity().getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.content_main, new AgentReportFragment(), Config.TAG_REPORT_FRAGMENT)
                                .addToBackStack(null)
                                .commit();

                    } else {

                        getActivity().getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.content_main, new ReportFragment(), Config.TAG_REPORT_FRAGMENT)
                                .addToBackStack(null)
                                .commit();
                    }
                }

                break;

            }

        }

    }

    private void fetchPickListTotal() {

        String url = null;
        RequestQueue queue = null;
        // Instantiate the RequestQueue.
        if (getContext() != null) {

            queue = Volley.newRequestQueue(getContext());
            if (userType.equals(API.VALUE_USER_GROUP_MANAGER))
                url = API.baseUrl + API.managerPickSummery;
            else
                url = API.baseUrl + API.agentPickSummery;

        }

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String code = jsonObject.getString(API.KEY_CODE);

                            if (code.equals(API.VALUE_CODE_SUCCESS)) {

                                dataFromServerPick(jsonObject);

                            } else {

                                JSONObject error = jsonObject.getJSONObject(API.KEY_JSON_ERROR);

                                try {

                                    if (code.equals(API.VALUE_CODE_AUTHENTICATION_ERROR)) {

                                        //Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                    } else if (code.equals(API.VALUE_CODE_PARAMETER_ERROR)) {
                                        Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                    }
                                } catch (Exception e) {

                                    Log.e("Dashboard : ", " " + e);

                                }


                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {

                    if (getContext() != null) {

                        if (error.getMessage().equals("com.android.volley.TimeoutError"))
                            Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_timeout), Toast.LENGTH_SHORT).show();

                        Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_unreachable), Toast.LENGTH_SHORT).show();
                    }

                    Log.v("Volly error : ", "" + error);

                } catch (Exception e) {

                    Log.v("Picked Tab E : ", " " + e);

                }
            }

        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                if (getContext() != null) {
                    params.put(API.KEY_JSON_USER_AUTHENTICATION, authenticationKey);
                    params.put(API.KEY_JSON_USER_GROUP, userType);
                    params.put(API.KEY_USER_ID, userID);
                    params.put(API.KEY_CONTENT_TYPE, API.VALUE_CONTENT_VALUE);
                }

                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                Config.volley_time_out,
                Config.max_retries,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Add the request to the RequestQueue.
        if (queue != null)
            queue.add(stringRequest);

    }

    private void dataFromServerPick(JSONObject jsonObject) throws JSONException {

        JSONArray dataArray = jsonObject.getJSONArray(API.KEY_JSON_DATA);

        for (int i = 0; i < dataArray.length(); i++) {

            if (userType.equals(API.VALUE_USER_GROUP_MANAGER)) {
                JSONObject summeryValueManager = dataArray.getJSONObject(i);

                if (summeryValueManager.getString(API.KEY_JSON_STATUS).equals(API.VALUE_JSON_STATUS_ASSIGNED))

                    pickAssigned.setText(summeryValueManager.getString(API.KEY_JSON_TOTAL));

                else if (summeryValueManager.getString(API.KEY_JSON_STATUS).equals(API.VALUE_JSON_STATUS_PICKED))

                    pickPicked.setText(summeryValueManager.getString(API.KEY_JSON_TOTAL));

                else if (summeryValueManager.getString(API.KEY_JSON_STATUS).equals(API.VALUE_JSON_STATUS_REQUESTED))

                    pickRequested.setText(summeryValueManager.getString(API.KEY_JSON_TOTAL));

            } else {

                JSONObject summeryValueAgent = dataArray.getJSONObject(i);

                if (summeryValueAgent.getString(API.KEY_JSON_STATUS).equals(API.VALUE_JSON_STATUS_ASSIGNED))

                    pickAssigned.setText(summeryValueAgent.getString(API.KEY_JSON_TOTAL));

                else if (summeryValueAgent.getString(API.KEY_JSON_STATUS).equals(API.VALUE_JSON_STATUS_PICKED))

                    pickPicked.setText(summeryValueAgent.getString(API.KEY_JSON_TOTAL));

            }

        }

    }

    private void fetchParcelListTotal() {

        String url = null;
        RequestQueue queue = null;
        // Instantiate the RequestQueue.
        if (getContext() != null) {

            queue = Volley.newRequestQueue(getContext());
            if (userType.equals(API.VALUE_USER_GROUP_MANAGER))
                url = API.baseUrl + API.managerParcelSummery;
            else
                url = API.baseUrl + API.agentParcelSummery;

        }

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String code = jsonObject.getString(API.KEY_CODE);

                            if (code.equals(API.VALUE_CODE_SUCCESS)) {

                                dataFromServerParcel(jsonObject);

                            } else {

                                JSONObject error = jsonObject.getJSONObject(API.KEY_JSON_ERROR);

                                try {

                                    if (code.equals(API.VALUE_CODE_AUTHENTICATION_ERROR)) {

                                        //Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                    } else if (code.equals(API.VALUE_CODE_PARAMETER_ERROR)) {
                                        Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                    }
                                } catch (Exception e) {

                                    Log.e("Dashboard : ", " " + e);

                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {

                    if (getContext() != null) {

                        if (error.getMessage().equals("com.android.volley.TimeoutError"))
                            Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_timeout), Toast.LENGTH_SHORT).show();

                        Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_unreachable), Toast.LENGTH_SHORT).show();
                    }

                    Log.v("Volly error : ", "" + error);

                } catch (Exception e) {

                    Log.v("Receive tab E : ", " " + e);

                }
            }

        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                if (getContext() != null) {
                    params.put(API.KEY_JSON_USER_AUTHENTICATION, authenticationKey);
                    params.put(API.KEY_JSON_USER_GROUP, userType);
                    params.put(API.KEY_USER_ID, userID);
                    params.put(API.KEY_CONTENT_TYPE, API.VALUE_CONTENT_VALUE);
                }

                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                Config.volley_time_out,
                Config.max_retries,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Add the request to the RequestQueue.
        if (queue != null)
            queue.add(stringRequest);

    }

    private void dataFromServerParcel(JSONObject jsonObject) throws JSONException {

        JSONObject dataObj = jsonObject.getJSONObject(API.KEY_JSON_DATA);
        JSONArray receiveStatusArray = dataObj.getJSONArray(API.KEY_JSON_RECEIVE_STATUS),
                distributeStatusArray = dataObj.getJSONArray(API.KEY_JSON_DISTRIBUTE_STATUS);

        if (dataObj.has(API.VALUE_JSON_STATUS_CASH)) {
            if (userType.equals(API.VALUE_USER_GROUP_MANAGER))
                receiveCash.setText(dataObj.getString(API.VALUE_JSON_STATUS_CASH));
            else
                distributeDelivery.setText(dataObj.getString(API.VALUE_JSON_STATUS_CASH));
        }

        for (int i = 0; i < receiveStatusArray.length(); i++) {

            JSONObject summeryValueReceive = receiveStatusArray.getJSONObject(i);

            if (userType.equals(API.VALUE_USER_GROUP_MANAGER)) {

                if (summeryValueReceive.getString(API.KEY_JSON_STATUS).equals(API.VALUE_JSON_STATUS_FULFILLMENT))
                    receiveFulfillment.setText(summeryValueReceive.getString(API.KEY_JSON_TOTAL));
                else if (summeryValueReceive.getString(API.KEY_JSON_STATUS).equals(API.VALUE_JSON_STATUS_CANCEL))
                    receiveCancel.setText(summeryValueReceive.getString(API.KEY_JSON_TOTAL));
                else if (summeryValueReceive.getString(API.KEY_JSON_STATUS).equals(API.VALUE_JSON_STATUS_HOLD))
                    receiveHold.setText(summeryValueReceive.getString(API.KEY_JSON_TOTAL));
                else if (summeryValueReceive.getString(API.KEY_JSON_STATUS).equals(API.VALUE_JSON_STATUS_RETURN))
                    receiveReturn.setText(summeryValueReceive.getString(API.KEY_JSON_TOTAL));

            } else {

                if (summeryValueReceive.getString(API.KEY_JSON_STATUS).equals(API.VALUE_JSON_STATUS_DELIVERY))
                    receiveCash.setText(summeryValueReceive.getString(API.KEY_JSON_TOTAL));
                else if (summeryValueReceive.getString(API.KEY_JSON_STATUS).equals(API.VALUE_JSON_STATUS_RETURN))
                    receiveReturn.setText(summeryValueReceive.getString(API.KEY_JSON_TOTAL));
                else if (summeryValueReceive.getString(API.KEY_JSON_STATUS).equals(API.VALUE_JSON_STATUS_TRANSFER))
                    receiveFulfillment.setText(summeryValueReceive.getString(API.KEY_JSON_TOTAL));

            }

        }

        for (int i = 0; i < distributeStatusArray.length(); i++) {
            JSONObject summeryValueDistribute = distributeStatusArray.getJSONObject(i);

            if (userType.equals(API.VALUE_USER_GROUP_MANAGER)) {

                if (summeryValueDistribute.getString(API.KEY_JSON_STATUS).equals(API.VALUE_JSON_STATUS_CANCEL))
                    distributeCancel.setText(summeryValueDistribute.getString(API.KEY_JSON_TOTAL));
                else if (summeryValueDistribute.getString(API.KEY_JSON_STATUS).equals(API.VALUE_JSON_STATUS_DELIVERY))
                    distributeDelivery.setText(summeryValueDistribute.getString(API.KEY_JSON_TOTAL));
                else if (summeryValueDistribute.getString(API.KEY_JSON_STATUS).equals(API.VALUE_JSON_STATUS_RETURN))
                    distributeReturn.setText(summeryValueDistribute.getString(API.KEY_JSON_TOTAL));
                else if (summeryValueDistribute.getString(API.KEY_JSON_STATUS).equals(API.VALUE_JSON_STATUS_WRONG_ROUTING))
                    distributeWR.setText(summeryValueDistribute.getString(API.KEY_JSON_TOTAL));
                else if (summeryValueDistribute.getString(API.KEY_JSON_STATUS).equals(API.VALUE_JSON_STATUS_SOURCE_DO))
                    distributeSDo.setText(summeryValueDistribute.getString(API.KEY_JSON_TOTAL));

            } else {

                if (summeryValueDistribute.getString(API.KEY_JSON_STATUS).equals(API.VALUE_JSON_STATUS_DELIVERY))
                    distributeSDo.setText(summeryValueDistribute.getString(API.KEY_JSON_TOTAL));
                else if (summeryValueDistribute.getString(API.KEY_JSON_STATUS).equals(API.VALUE_JSON_STATUS_RETURN))
                    distributeReturn.setText(summeryValueDistribute.getString(API.KEY_JSON_TOTAL));
                else if (summeryValueDistribute.getString(API.KEY_JSON_STATUS).equals(API.VALUE_JSON_STATUS_CANCEL))
                    distributeCancel.setText(summeryValueDistribute.getString(API.KEY_JSON_TOTAL));
                else if (summeryValueDistribute.getString(API.KEY_JSON_STATUS).equals(API.VALUE_JSON_STATUS_HOLD))
                    distributeWR.setText(summeryValueDistribute.getString(API.KEY_JSON_TOTAL));

            }

        }

    }

}
