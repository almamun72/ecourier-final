package bd.com.ecourier.ecourier.View;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bd.com.ecourier.ecourier.API;
import bd.com.ecourier.ecourier.Config;
import bd.com.ecourier.ecourier.Model.MerchantProduct;
import bd.com.ecourier.ecourier.Presenter.MerchantProductListAdapter;
import bd.com.ecourier.ecourier.R;
import bd.com.ecourier.ecourier.clickListener.ClickListener;
import bd.com.ecourier.ecourier.clickListener.RecyclerTouchListener;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by User on 10-Dec-17.
 */

public class PickAssignedAgentFragment extends Fragment {

    @BindView(R.id.swipe_down_to_refresh_list)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.recyclerView_list)
    RecyclerView recyclerView;
    @BindView(R.id.fab_scan)
    FloatingActionButton QRScan;
    @BindView(R.id.recyclerView_list_notice)
    TextView noListNotice;
    MerchantProduct merchantProduct;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    private MerchantProductListAdapter merchantProductListAdapter;
    private ArrayList<MerchantProduct> merchantProductArrayList;
    private ArrayList<MerchantProduct> merchantProductArrayListForServer;
    private ArrayList<String> consignments;
    private View rootView;
    private ArrayList<String> qrScanCodes = new ArrayList<>();

    public PickAssignedAgentFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.recycle_view_list_layout, container, false);
        ButterKnife.bind(this, rootView);

        QRScan.setVisibility(View.GONE);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                merchantProductListAdapter.clearList();
                fetchListData();

            }
        });

        merchantProductArrayListForServer = new ArrayList<>();
        merchantProductArrayList = new ArrayList<>();

        showFeed(merchantProductArrayList);
        recyclerView.setVisibility(View.GONE);
        noListNotice.setVisibility(View.VISIBLE);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {

                ImageView pickParcel = view.findViewById(R.id.pick_parcel);
                final MerchantProduct merchantProduct = merchantProductArrayList.get(position);

                pickParcel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (getContext() != null) {

                            if (merchantProduct.getConsignments().size() > 0) {

                                Bundle bundle = new Bundle();
                                bundle.putStringArrayList(API.KEY_JSON_CONSIGNMENTS, merchantProduct.getConsignments());
                                bundle.putString(API.KEY_MERCHANT_PRODUCT_PICKUP_ID, merchantProduct.getId());
                                bundle.putString(API.KEY_CUSTOMER_PRODUCT_MERCHANT_NAME, merchantProduct.getName());
                                bundle.putString(API.KEY_CUSTOMER_PRODUCT_COLLECTED_AMOUNT, merchantProduct.getNoOfParcel());
                                AutomatedPickFragment automatedPickFragment = new AutomatedPickFragment();
                                automatedPickFragment.setArguments(bundle);

                                ((MainActivity) getContext()).getSupportFragmentManager()
                                        .beginTransaction()
                                        .replace(R.id.content_main, automatedPickFragment)
                                        .addToBackStack(null)
                                        .commit();


                            } else {

                                AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                                builder1.setCancelable(false);

                                View alertView = inflater.inflate(R.layout.agent_pick_parcel_confirm_dialog, null);
                                builder1.setView(alertView);

                                final TextView merchantName = alertView.findViewById(R.id.alert_merchant_name);
                                TextView requestedAmount = alertView.findViewById(R.id.alert_merchant_requested_amount);
                                final EditText amountPicked = alertView.findViewById(R.id.alert_parcel_amount_picked);
                                final EditText otp = alertView.findViewById(R.id.alert_parcel_otp_picked);
                                otp.setVisibility(View.GONE);

                                merchantName.setText(merchantProduct.getName());
                                requestedAmount.setText(String.format("%s %s", getContext().getResources().getString(R.string.pick_request), merchantProduct.getNoOfParcel()));

                                builder1.setPositiveButton(
                                        getContext().getResources().getString(R.string.get_sms),
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {

                                            }
                                        }).setNegativeButton(getContext().getResources().getString(R.string.back), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        // User cancelled the dialog
                                        dialog.cancel();
                                    }
                                })/*.setNeutralButton(getContext().getResources().getString(R.string.resend_sms), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        // User cancelled the dialog

                                    }
                                })*/;


                                final AlertDialog alert11 = builder1.create();
                                alert11.setOnShowListener(new DialogInterface.OnShowListener() {
                                    @Override
                                    public void onShow(DialogInterface arg0) {
                                        alert11.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.RED);
                                        alert11.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getContext().getResources().getColor(R.color.colorPrimary));
                                        alert11.getButton(AlertDialog.BUTTON_NEUTRAL).setTextColor(Color.BLACK);

                                        alert11.getButton(AlertDialog.BUTTON_NEUTRAL).setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {

                                                //resendOTP(merchantProduct);

                                            }
                                        });

                                        alert11.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {

                                                if (!amountPicked.getText().toString().equals("")) {
                                                    resendOTP(merchantProduct, amountPicked.getText().toString());
                                                    alert11.cancel();
                                                } else {
                                                    if (getActivity() != null)
                                                        Toast.makeText(getActivity(), "Please enter amount picked", Toast.LENGTH_SHORT).show();
                                                }

                                            }
                                        });

                                    }
                                });

                                alert11.show();

                            }


                        }

                    }
                });


            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        if (getContext() != null)
            if (((MainActivity) getContext()).connectedTOInternet) {
                merchantProductListAdapter.clearList();
                fetchListData();
            }

        return rootView;
    }

    private void fetchListData() {

        progressBar.setVisibility(View.VISIBLE);
        swipeRefreshLayout.setEnabled(false);

        RequestQueue queue = null;
        // Instantiate the RequestQueue.
        if (getContext() != null)
            queue = Volley.newRequestQueue(getContext());
        String url = API.baseUrl + API.agentPickAssigned + API.LIMIT + API.DATA_LIMIT + API.OFFSET + merchantProductArrayList.size();

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        progressBar.setVisibility(View.GONE);

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String code = jsonObject.getString(API.KEY_CODE);

                            if (code.equals(API.VALUE_CODE_SUCCESS)) {

                                merchantProductListAdapter.listUpdated(dataFromServer(jsonObject));
                                recyclerView.setVisibility(View.VISIBLE);
                                noListNotice.setVisibility(View.GONE);
                                swipeRefreshLayout.setRefreshing(false);

                                JSONArray jsonArray = jsonObject.getJSONArray(API.KEY_JSON_DATA);
                                if (jsonArray.length() > 0)
                                    fetchListData();
                                else
                                    swipeRefreshLayout.setEnabled(true);

                            } else {

                                JSONObject error = jsonObject.getJSONObject(API.KEY_JSON_ERROR);

                                if (code.equals(API.VALUE_CODE_AUTHENTICATION_ERROR)) {

                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                } else if (code.equals(API.VALUE_CODE_PARAMETER_ERROR)) {
                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                swipeRefreshLayout.setRefreshing(false);
                swipeRefreshLayout.setEnabled(true);

                try {

                    if (getContext() != null) {

                        if (error.getMessage().equals("com.android.volley.TimeoutError"))
                            Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_timeout), Toast.LENGTH_SHORT).show();

                        Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_unreachable), Toast.LENGTH_SHORT).show();
                    }

                    Log.v("Volly error : ", "" + error);

                } catch (Exception e) {

                    Log.v("Pick assign E : ", " " + e);

                }
            }

        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                if (getContext() != null) {
                    params.put(API.KEY_JSON_USER_AUTHENTICATION, ((MainActivity) getContext()).authenticationKey);
                    params.put(API.KEY_JSON_USER_GROUP, ((MainActivity) getContext()).userGroup);
                    params.put(API.KEY_USER_ID, ((MainActivity) getContext()).userID);
                    params.put(API.KEY_CONTENT_TYPE, API.VALUE_CONTENT_VALUE);
                }


                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                Config.volley_time_out,
                Config.max_retries,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Add the request to the RequestQueue.
        if (queue != null)
            queue.add(stringRequest);

    }

    private MerchantProduct[] dataFromServer(JSONObject jsonObject) throws JSONException {

        JSONArray jsonArray = jsonObject.getJSONArray(API.KEY_JSON_DATA);

        MerchantProduct[] merchantProducts = new MerchantProduct[jsonArray.length()];

        for (int i = 0; i < jsonArray.length(); i++) {

            JSONObject merchantProduct = jsonArray.getJSONObject(i);
            consignments = new ArrayList<>();

            merchantProducts[i] = new MerchantProduct(merchantProduct.getString(API.KEY_MERCHANT_PRODUCT_PICKUP_ID), merchantProduct.getString(API.KEY_MERCHANT_PRODUCT_MERCHANT_NAME), merchantProduct.getString(API.KEY_MERCHANT_PRODUCT_MERCHANT_ADDRESS), merchantProduct.getString(API.KEY_MERCHANT_PRODUCT_NO_OF_PARCEL), merchantProduct.getString(API.KEY_MERCHANT_PRODUCT_MERCHANT_PHONE));

            if (merchantProduct.has(API.KEY_JSON_CONSIGNMENTS)) {

                JSONArray consignmentDataArray = merchantProduct.getJSONArray(API.KEY_JSON_CONSIGNMENTS);

                for (int j = 0; j < consignmentDataArray.length(); j++) {

                    consignments.add(consignmentDataArray.getString(j));

                }

                merchantProducts[i].setConsignments(consignments);
            }

        }

        return merchantProducts;
    }

    private void showFeed(ArrayList<MerchantProduct> merchantProducts) {

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        merchantProductListAdapter = new MerchantProductListAdapter(getActivity(), merchantProducts, Config.TAG_PICKED_ASSIGNED_AGENT_FRAGMENT);
        recyclerView.setAdapter(merchantProductListAdapter);

    }

    private void resendOTP(final MerchantProduct merchantProduct, final String amountPickedSt) {

        final ProgressDialog dialog = ProgressDialog.show(getActivity(), "",
                getActivity().getResources().getString(R.string.loading), true);

        dialog.show();

        RequestQueue queue = null;
        // Instantiate the RequestQueue.
        if (getContext() != null)
            queue = Volley.newRequestQueue(getContext());
        String url = API.baseUrl + API.resendOTP;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        dialog.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String code = jsonObject.getString(API.KEY_CODE);

                            if (code.equals(API.VALUE_CODE_SUCCESS)) {

                                Toast.makeText(getContext(), "Verification code send.", Toast.LENGTH_SHORT).show();

                                AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                                builder1.setCancelable(false);

                                LayoutInflater inflater = getActivity().getLayoutInflater();
                                View alertView = inflater.inflate(R.layout.agent_pick_parcel_confirm_dialog, null);
                                builder1.setView(alertView);

                                final TextView merchantName = alertView.findViewById(R.id.alert_merchant_name);
                                TextView requestedAmount = alertView.findViewById(R.id.alert_merchant_requested_amount);
                                final EditText amountPicked = alertView.findViewById(R.id.alert_parcel_amount_picked);
                                final EditText otp = alertView.findViewById(R.id.alert_parcel_otp_picked);
                                amountPicked.setEnabled(false);
                                amountPicked.setText(amountPickedSt);

                                merchantName.setText(merchantProduct.getName());
                                requestedAmount.setText(String.format("%s %s", getContext().getResources().getString(R.string.pick_request), merchantProduct.getNoOfParcel()));

                                builder1.setPositiveButton(
                                        getContext().getResources().getString(R.string.confirm_parcel),
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {

                                            }
                                        }).setNegativeButton(getContext().getResources().getString(R.string.back), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        // User cancelled the dialog
                                        dialog.cancel();
                                    }
                                }).setNeutralButton(getContext().getResources().getString(R.string.resend_sms), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        // User cancelled the dialog

                                    }
                                });


                                final AlertDialog alert11 = builder1.create();
                                alert11.setOnShowListener(new DialogInterface.OnShowListener() {
                                    @Override
                                    public void onShow(DialogInterface arg0) {
                                        alert11.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.RED);
                                        alert11.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getContext().getResources().getColor(R.color.colorPrimary));
                                        alert11.getButton(AlertDialog.BUTTON_NEUTRAL).setTextColor(Color.BLACK);

                                        alert11.getButton(AlertDialog.BUTTON_NEUTRAL).setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {

                                                resendOTP(merchantProduct, amountPicked.getText().toString());

                                            }
                                        });

                                        alert11.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {

                                                if (!otp.getText().toString().equals("")) {
                                                    updateStatus(String.valueOf(merchantProduct.getId()), API.STATUS_AGENT_CONFIRM_PICK, otp.getText().toString(), amountPicked.getText().toString(), alert11);

                                                } else {
                                                    if (getActivity() != null)
                                                        Toast.makeText(getActivity(), "Please enter verification code", Toast.LENGTH_SHORT).show();
                                                }

                                            }
                                        });

                                    }
                                });

                                alert11.show();



                            } else {

                                JSONObject error = jsonObject.getJSONObject(API.KEY_JSON_ERROR);

                                if (code.equals(API.VALUE_CODE_AUTHENTICATION_ERROR)) {

                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                } else if (code.equals(API.VALUE_CODE_PARAMETER_ERROR)) {
                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.cancel();
                try {

                    if (getContext() != null) {

                        if (error.getMessage().equals("com.android.volley.TimeoutError"))
                            Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_timeout), Toast.LENGTH_SHORT).show();

                        Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_unreachable), Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    Log.v("Pick Assigned Error : ", "" + e);

                }

                Log.v("Volly error : ", "" + error);
            }

        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                if (getContext() != null) {

                    params.put(API.KEY_JSON_USER_AUTHENTICATION, ((MainActivity) getContext()).authenticationKey);
                    params.put(API.KEY_JSON_USER_GROUP, ((MainActivity) getContext()).userGroup);
                    params.put(API.KEY_USER_ID, ((MainActivity) getContext()).userID);
                    params.put(API.KEY_CONTENT_TYPE, API.VALUE_CONTENT_VALUE);

                }

                return params;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put(API.KEY_OTP_TYPE, API.VALUE_OTP_TYPE_PICK);
                params.put(API.KEY_ORDER_ID, merchantProduct.getId());
                params.put(API.KEY_ORDER, amountPickedSt);
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                Config.volley_time_out,
                Config.max_retries,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Add the request to the RequestQueue.
        if (queue != null)
            queue.add(stringRequest);

    }

    private void updateStatus(final String id, final String status, final String smsCode, final String orders, final AlertDialog alertDialog) {

        final ProgressDialog dialog = ProgressDialog.show(getActivity(), "",
                getActivity().getResources().getString(R.string.loading), true);

        dialog.show();

        RequestQueue queue = null;
        // Instantiate the RequestQueue.
        if (getContext() != null)
            queue = Volley.newRequestQueue(getContext());
        String url = API.baseUrl + API.agentPickUpUpdate;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        swipeRefreshLayout.setRefreshing(false);
                        dialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String code = jsonObject.getString(API.KEY_CODE);

                            if (code.equals(API.VALUE_CODE_SUCCESS)) {
                                alertDialog.cancel();
                                JSONObject data = jsonObject.getJSONObject(API.KEY_JSON_DATA);

                                String updated = data.getString(API.KEY_JSON_MESSAGE);

                                if (updated.length() > 0) {
                                    Toast.makeText(getContext(), " " + updated, Toast.LENGTH_SHORT).show();
                                    merchantProductListAdapter.clearList();
                                    fetchListData();
                                    ((MainActivity) getContext()).refreshPickTabTitle();

                                }

                            } else {

                                JSONObject error = jsonObject.getJSONObject(API.KEY_JSON_ERROR);

                                if (code.equals(API.VALUE_CODE_AUTHENTICATION_ERROR)) {

                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                } else if (code.equals(API.VALUE_CODE_PARAMETER_ERROR)) {
                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                swipeRefreshLayout.setRefreshing(false);
                alertDialog.cancel();
                dialog.dismiss();
                try {

                    if (getContext() != null) {

                        if (error.getMessage().equals("com.android.volley.TimeoutError"))
                            Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_timeout), Toast.LENGTH_SHORT).show();

                        Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_unreachable), Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    Log.v("Pick Assigned Error : ", "" + e);

                }

                Log.v("Volly error : ", "" + error);
            }

        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                if (getContext() != null) {

                    params.put(API.KEY_JSON_USER_AUTHENTICATION, ((MainActivity) getContext()).authenticationKey);
                    params.put(API.KEY_JSON_USER_GROUP, ((MainActivity) getContext()).userGroup);
                    params.put(API.KEY_USER_ID, ((MainActivity) getContext()).userID);
                    params.put(API.KEY_CONTENT_TYPE, API.VALUE_CONTENT_VALUE);

                }

                return params;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put(API.KEY_MERCHANT_PRODUCT_PICKUP_ID, id);
                params.put(API.KEY_JSON_STATUS, status);
                params.put(API.KEY_JSON_SMS_CODE, smsCode);
                params.put(API.KEY_JSON_ORDER, orders);
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                Config.volley_time_out,
                Config.max_retries,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Add the request to the RequestQueue.
        if (queue != null)
            queue.add(stringRequest);

    }

}
