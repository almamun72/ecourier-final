package bd.com.ecourier.ecourier.Model;

import android.graphics.Color;

import java.util.ArrayList;

/**
 * Created by User on 11-Dec-17.
 */

public class MerchantProduct {

    private String name;
    private String address;
    private String noOfParcel;
    private String phoneNumber;
    private String id;
    private ArrayList<String> consignments;
    private boolean isChecked = false;
    private int maxLine, color, min = 3, max = 5, colorUnselected = Color.WHITE, colorSelected, textColor, colorWhite = Color.WHITE, colorBlack = Color.BLACK;

    public MerchantProduct(String id, String name, String address, String noOfParcel, String phoneNumber) {

        this.id = id;
        this.name = name;
        this.address = address;
        this.noOfParcel = noOfParcel;
        this.phoneNumber = phoneNumber;
        maxLine = min;
        color = colorUnselected;
        textColor = colorBlack;
        colorSelected = Color.parseColor("#25b472");

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getNoOfParcel() {
        return noOfParcel;
    }

    public void setNoOfParcel(String noOfParcel) {
        this.noOfParcel = noOfParcel;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
        if (checked) {

            color = colorSelected;
            textColor = colorWhite;

        } else {

            color = colorUnselected;
            textColor = colorBlack;

        }

    }

    public int getMaxLine() {
        return maxLine;
    }

    public void setMaxLine(int maxLine) {
        this.maxLine = maxLine;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public int getTextColor() {
        return textColor;
    }

    public void setTextColor(int textColor) {
        this.textColor = textColor;
    }

    public ArrayList<String> getConsignments() {
        return consignments;
    }

    public void setConsignments(ArrayList<String> consignments) {
        this.consignments = new ArrayList<>();
        this.consignments = consignments;
    }
}
