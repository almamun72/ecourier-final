package bd.com.ecourier.ecourier.View;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import bd.com.ecourier.ecourier.API;
import bd.com.ecourier.ecourier.Config;
import bd.com.ecourier.ecourier.Presenter.ReceiveTabAdapter;
import bd.com.ecourier.ecourier.R;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by User on 10-Dec-17.
 */

public class ReceiveTabFragment extends Fragment {

    // Find the view pager that will allow the user to swipe between fragments
    @BindView(R.id.tab_view_pager)
    ViewPager viewPager;
    // Find the tab layout that shows the tabs
    @BindView(R.id.tab_bar)
    TabLayout tabLayout;

    private String userType;
    private int tabPosition = 0;
    private int offScreenPagingLimit = 3;
    private ReceiveTabAdapter adapter;

    public ReceiveTabFragment() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tab_layout, container, false);
        ButterKnife.bind(this, rootView);

        if (getActivity() != null) {

            getActivity().setTitle(getResources().getString(R.string.receive));
            userType = ((MainActivity) getActivity()).userGroup;

        }

        // Create an adapter that knows which fragment should be shown on each page
        adapter = new ReceiveTabAdapter(getContext(), getChildFragmentManager(), userType);

        // Set the adapter onto the view pager
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(offScreenPagingLimit);

        // Connect the tab layout with the view pager. This will
        //   1. Update the tab layout when the view pager is swiped
        //   2. Update the view pager when a tab is selected
        //   3. Set the tab layout's tab names with the view pager's adapter's titles
        //      by calling onPageTitle()
        tabLayout.setupWithViewPager(viewPager);
        if (getActivity() != null) {

            if (((MainActivity) getActivity()).connectedTOInternet)
                fetchListTotal();

        }

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                tabPosition = tab.getPosition();

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        return rootView;
    }

    @Override
    public void onResume() {

        if (getArguments() != null) {

            if (getArguments().getBoolean("onResume", true)) {

                ReceiveTabAdapter adapter = new ReceiveTabAdapter(getContext(), getChildFragmentManager(), userType);
                viewPager.setAdapter(adapter);
                viewPager.setOffscreenPageLimit(offScreenPagingLimit);
                tabLayout.getTabAt(getArguments().getInt("tabPosition", tabPosition)).select();
                if (getActivity() != null) {

                    if (((MainActivity) getActivity()).connectedTOInternet)
                        fetchListTotal();

                }

            }

        }

        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();

        if (getArguments() != null) {

            getArguments().putInt("tabPosition", tabPosition);
            getArguments().putBoolean("onResume", true);

        }

    }

    private void fetchListTotal() {

        String url = null;
        RequestQueue queue = null;
        // Instantiate the RequestQueue.
        if (getContext() != null) {

            queue = Volley.newRequestQueue(getContext());
            if (userType.equals(API.VALUE_USER_GROUP_MANAGER))
                url = API.baseUrl + API.managerParcelSummery;
            else
                url = API.baseUrl + API.agentParcelSummery;

        }

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String code = jsonObject.getString(API.KEY_CODE);

                            if (code.equals(API.VALUE_CODE_SUCCESS)) {

                                dataFromServer(jsonObject);
                                adapter.notifyDataSetChanged();

                            } else {

                                JSONObject error = jsonObject.getJSONObject(API.KEY_JSON_ERROR);

                                if (code.equals(API.VALUE_CODE_AUTHENTICATION_ERROR)) {

                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                } else if (code.equals(API.VALUE_CODE_PARAMETER_ERROR)) {
                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {

                    if (getContext() != null) {

                        if (error.getMessage().equals("com.android.volley.TimeoutError"))
                            Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_timeout), Toast.LENGTH_SHORT).show();

                        Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_unreachable), Toast.LENGTH_SHORT).show();
                    }

                    Log.v("Volly error : ", "" + error);

                } catch (Exception e) {

                    Log.v("Receive tab E : ", " " + e);

                }
            }

        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                if (getContext() != null) {
                    params.put(API.KEY_JSON_USER_AUTHENTICATION, ((MainActivity) getContext()).authenticationKey);
                    params.put(API.KEY_JSON_USER_GROUP, ((MainActivity) getContext()).userGroup);
                    params.put(API.KEY_USER_ID, ((MainActivity) getContext()).userID);
                    params.put(API.KEY_CONTENT_TYPE, API.VALUE_CONTENT_VALUE);
                }

                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                Config.volley_time_out,
                Config.max_retries,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Add the request to the RequestQueue.
        if (queue != null)
            queue.add(stringRequest);

    }

    private void dataFromServer(JSONObject jsonObject) throws JSONException {

        JSONObject dataObj = jsonObject.getJSONObject(API.KEY_JSON_DATA);
        JSONArray receiveStatusArray;

        if (dataObj.has(API.KEY_JSON_RECEIVE_STATUS)) {

            receiveStatusArray = dataObj.getJSONArray(API.KEY_JSON_RECEIVE_STATUS);

        } else {

            return;

        }

        adapter.transferCount = 0;
        adapter.deliveryCount = 0;
        adapter.returnCount = 0;
        adapter.cancelCount = 0;
        adapter.holdCount = 0;
        adapter.fulfillmentCount = 0;
        adapter.cash = 0;

        for (int i = 0; i < receiveStatusArray.length(); i++) {

            JSONObject summeryValue = receiveStatusArray.getJSONObject(i);

            if (userType.equals(API.VALUE_USER_GROUP_MANAGER)) {

                if (summeryValue.getString(API.KEY_JSON_STATUS).equals(API.VALUE_JSON_STATUS_FULFILLMENT))
                    adapter.fulfillmentCount = summeryValue.getInt(API.KEY_JSON_TOTAL);
                else if (summeryValue.getString(API.KEY_JSON_STATUS).equals(API.VALUE_JSON_STATUS_CANCEL))
                    adapter.cancelCount = summeryValue.getInt(API.KEY_JSON_TOTAL);
                else if (summeryValue.getString(API.KEY_JSON_STATUS).equals(API.VALUE_JSON_STATUS_HOLD))
                    adapter.holdCount = summeryValue.getInt(API.KEY_JSON_TOTAL);
                else if (summeryValue.getString(API.KEY_JSON_STATUS).equals(API.VALUE_JSON_STATUS_RETURN))
                    adapter.returnCount = summeryValue.getInt(API.KEY_JSON_TOTAL);

                if (dataObj.has(API.VALUE_JSON_STATUS_CASH)) {
                    try {

                        adapter.cash = dataObj.getInt(API.VALUE_JSON_STATUS_CASH);

                    } catch (Exception e) {

                    }
                }

            } else {

                if (summeryValue.getString(API.KEY_JSON_STATUS).equals(API.VALUE_JSON_STATUS_DELIVERY))
                    adapter.deliveryCount = summeryValue.getInt(API.KEY_JSON_TOTAL);
                else if (summeryValue.getString(API.KEY_JSON_STATUS).equals(API.VALUE_JSON_STATUS_RETURN))
                    adapter.returnCount = summeryValue.getInt(API.KEY_JSON_TOTAL);
                else if (summeryValue.getString(API.KEY_JSON_STATUS).equals(API.VALUE_JSON_STATUS_TRANSFER))
                    adapter.transferCount = summeryValue.getInt(API.KEY_JSON_TOTAL);


            }

        }

    }

    public void refreshPageTitle() {

        fetchListTotal();

    }

    public void refreshTransferTitle(int transferCount) {

        adapter.transferCount = transferCount;
        adapter.notifyDataSetChanged();

    }

}
