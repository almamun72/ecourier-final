package bd.com.ecourier.ecourier.View;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bd.com.ecourier.ecourier.API;
import bd.com.ecourier.ecourier.Config;
import bd.com.ecourier.ecourier.Model.CustomerProduct;
import bd.com.ecourier.ecourier.Presenter.CustomerProductListAdapter;
import bd.com.ecourier.ecourier.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

/**
 * Created by User on 10-Dec-17.
 */

public class DistributeReturnForAgentFragment extends Fragment {

    @BindView(R.id.swipe_down_to_refresh_list)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.recyclerView_list)
    RecyclerView recyclerView;
    @BindView(R.id.fab_scan)
    FloatingActionButton QRScan;
    @BindView(R.id.recyclerView_list_notice)
    TextView noListNotice;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    private CustomerProductListAdapter customerProductListAdapter;
    private ArrayList<CustomerProduct> customerProductArrayList;
    private View rootView;
    private ArrayList<String> qrScanCodes = new ArrayList<>();
    private IntentIntegrator intentIntegrator;


    public DistributeReturnForAgentFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.recycle_view_list_layout, container, false);
        ButterKnife.bind(this, rootView);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                customerProductListAdapter.clearList();
                fetchListData();

            }
        });

        customerProductArrayList = new ArrayList<>();

        showFeed(customerProductArrayList);
        recyclerView.setVisibility(View.GONE);
        noListNotice.setVisibility(View.VISIBLE);

        if (getContext() != null)
            if (((MainActivity) getContext()).connectedTOInternet)
                fetchListData();

        intentIntegrator = IntentIntegrator.forSupportFragment(this);
        intentIntegrator.setCaptureLayout(R.layout.barcode_scanner);
        intentIntegrator.setResultDisplayDuration(0);

        return rootView;
    }

    private void fetchListData() {

        progressBar.setVisibility(View.VISIBLE);
        swipeRefreshLayout.setEnabled(false);

        RequestQueue queue = null;
        // Instantiate the RequestQueue.
        if (getContext() != null)
            queue = Volley.newRequestQueue(getContext());
        String url = API.baseUrl + API.agentDistributeReturn + API.LIMIT + API.DATA_LIMIT + API.OFFSET + customerProductArrayList.size();

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        progressBar.setVisibility(View.GONE);

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String code = jsonObject.getString(API.KEY_CODE);

                            if (code.equals(API.VALUE_CODE_SUCCESS)) {

                                customerProductListAdapter.listUpdated(dataFromServer(jsonObject));
                                recyclerView.setVisibility(View.VISIBLE);
                                noListNotice.setVisibility(View.GONE);
                                swipeRefreshLayout.setRefreshing(false);

                                JSONArray jsonArray = jsonObject.getJSONArray(API.KEY_JSON_DATA);
                                if (jsonArray.length() > 0)
                                    fetchListData();
                                else
                                    swipeRefreshLayout.setEnabled(true);

                            } else {

                                JSONObject error = jsonObject.getJSONObject(API.KEY_JSON_ERROR);

                                if (code.equals(API.VALUE_CODE_AUTHENTICATION_ERROR)) {

                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                } else if (code.equals(API.VALUE_CODE_PARAMETER_ERROR)) {
                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                swipeRefreshLayout.setRefreshing(false);
                swipeRefreshLayout.setEnabled(true);

                try {

                    if (getContext() != null) {

                        if (error.getMessage().equals("com.android.volley.TimeoutError"))
                            Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_timeout), Toast.LENGTH_SHORT).show();

                        Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_unreachable), Toast.LENGTH_SHORT).show();
                    }

                    Log.v("Volly error : ", "" + error);

                } catch (Exception e) {

                    Log.v("Distribute Return E : ", " " + e);

                }
            }

        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                if (getContext() != null) {
                    params.put(API.KEY_JSON_USER_AUTHENTICATION, ((MainActivity) getContext()).authenticationKey);
                    params.put(API.KEY_JSON_USER_GROUP, ((MainActivity) getContext()).userGroup);
                    params.put(API.KEY_USER_ID, ((MainActivity) getContext()).userID);
                    params.put(API.KEY_CONTENT_TYPE, API.VALUE_CONTENT_VALUE);
                }

                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                Config.volley_time_out,
                Config.max_retries,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Add the request to the RequestQueue.
        if (queue != null)
            queue.add(stringRequest);

    }

    private CustomerProduct[] dataFromServer(JSONObject jsonObject) throws JSONException {

        JSONArray jsonArray = jsonObject.getJSONArray(API.KEY_JSON_DATA);

        CustomerProduct[] customerProducts = new CustomerProduct[jsonArray.length()];

        for (int i = 0; i < jsonArray.length(); i++) {

            JSONObject customerProduct = jsonArray.getJSONObject(i);

            customerProducts[i] = new CustomerProduct(customerProduct.getString(API.KEY_CUSTOMER_PRODUCT_ECR_NUMBER), customerProduct.getString(API.KEY_CUSTOMER_PRODUCT_CUSTOMER_NAME), customerProduct.getString(API.KEY_CUSTOMER_PRODUCT_CUSTOMER_ADDRESS), customerProduct.getString(API.KEY_CUSTOMER_PRODUCT_PRICE), customerProduct.getString(API.KEY_CUSTOMER_PRODUCT_MERCHANT_NAME), customerProduct.getString(API.KEY_CUSTOMER_PRODUCT_CUSTOMER_PHONE), customerProduct.getString(API.KEY_CUSTOMER_PRODUCT_MERCHANT_PHONE));

        }

        return customerProducts;
    }

    private void showFeed(ArrayList<CustomerProduct> customerProductArrayList) {

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        customerProductListAdapter = new CustomerProductListAdapter(getActivity(), customerProductArrayList, Config.TAG_DISTRIBUTE_RETURN_FOR_AGENT_FRAGMENT);
        recyclerView.setAdapter(customerProductListAdapter);

    }

    @OnClick(R.id.fab_scan)
    public void onClick(View view) {

        intentIntegrator.addExtra("PROMPT_MESSAGE", "Total : " + qrScanCodes.size());
        intentIntegrator.initiateScan();

    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {

        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (scanResult != null) {

            if (resultCode == RESULT_OK) {
                // contents contains whatever the code was
                String contents = scanResult.getContents();

                // Format contains the type of code i.e. UPC, EAN, QRCode etc...
                //String format = intent.getStringExtra("SCAN_RESULT_FORMAT");

                // Handle successful scan. In this example add contents to ArrayList
                if (contents != null) {
                    for (int i = 0; i < customerProductArrayList.size(); i++) {

                        if (customerProductArrayList.get(i).getECRNumber().equals(contents)) {

                            Bundle bundle = new Bundle();
                            bundle.putString(API.KEY_JSON_CONSIGNMENTS, contents);
                            DistributeReturnDetailFragment distributeReturnDetailFragment = new DistributeReturnDetailFragment();
                            distributeReturnDetailFragment.setArguments(bundle);

                            if (getActivity() != null)
                                ((MainActivity) getActivity()).getSupportFragmentManager()
                                        .beginTransaction()
                                        .replace(R.id.content_main, distributeReturnDetailFragment)
                                        .addToBackStack(null)
                                        .commit();

                            break;

                        }

                    }

                }

            } else if (resultCode == RESULT_CANCELED) {

                qrScanCodes.clear();

            }


        } else {
            Toast toast = Toast.makeText(getContext(),
                    "No scan data received!", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

}
