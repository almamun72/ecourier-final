package bd.com.ecourier.ecourier.firebaseServices;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.RemoteMessage;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import bd.com.ecourier.ecourier.R;
import bd.com.ecourier.ecourier.View.MainActivity;

/**
 * Created by User on 27-Nov-17.
 */

public class FirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService {

    public class MyFirebaseMessagingService extends FirebaseMessagingService {

        private static final String TAG = "MyGcmListenerService";

        @Override
        public void onMessageReceived(RemoteMessage message) {

            String image = message.getNotification().getIcon();
            String title = message.getNotification().getTitle();
            String text = message.getNotification().getBody();
            String sound = message.getNotification().getSound();
            String action = message.getData().get("push_key");

            // TODO(developer): Handle FCM messages here.
            // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
            Log.d(TAG, "From: " + message.getFrom());

            int id = 0;
            Object obj = message.getData().get("id");
            if (obj != null) {
                id = Integer.valueOf(obj.toString());
            }

            this.sendNotification(new NotificationData(image, id, title, text, sound, action));
        }

        /**
         * Create and show a simple notification containing the received GCM message.
         *
         * @param notificationData GCM message received.
         */
        private void sendNotification(NotificationData notificationData) {

            Intent intent = new Intent(this, MainActivity.class);
            intent.putExtra(NotificationData.TEXT, notificationData.getAction());

            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                    PendingIntent.FLAG_ONE_SHOT);

            NotificationCompat.Builder notificationBuilder = null;
            try {

                notificationBuilder = new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(URLDecoder.decode(notificationData.getTitle(), "UTF-8"))
                        .setContentText(URLDecoder.decode(notificationData.getTextMessage(), "UTF-8"))
                        .setAutoCancel(true)
                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                        .setContentIntent(pendingIntent);

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            if (notificationBuilder != null) {
                NotificationManager notificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.notify(notificationData.getId(), notificationBuilder.build());
            } else {
                Log.d(TAG, "Não foi possível criar objeto notificationBuilder");
            }
        }
    }

    /*private static final String TAG = "Message from server";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // ...

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());

            *//*if (*//**//* Check if data needs to be processed by long running job *//**//* true) {
                // For long-running tasks (10 seconds or more) use Firebase Job Dispatcher.
                scheduleJob();
            } else {
                // Handle message within 10 seconds
                handleNow();
            }
*//*
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }*/

}
