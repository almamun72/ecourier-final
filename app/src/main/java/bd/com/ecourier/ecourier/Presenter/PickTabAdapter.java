package bd.com.ecourier.ecourier.Presenter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;

import bd.com.ecourier.ecourier.Config;
import bd.com.ecourier.ecourier.R;
import bd.com.ecourier.ecourier.View.PickAssignedAgentFragment;
import bd.com.ecourier.ecourier.View.PickAssignedFragment;
import bd.com.ecourier.ecourier.View.PickRequestedFragment;
import bd.com.ecourier.ecourier.View.PickedAgentFragment;
import bd.com.ecourier.ecourier.View.PickedFragment;

/**
 * Created by User on 10-Dec-17.
 */

public class PickTabAdapter extends android.support.v4.app.FragmentPagerAdapter {

    public String requested, assigned, picked;
    private Context context;
    private String userType;

    public PickTabAdapter(Context context, FragmentManager fm, String userType) {
        super(fm);
        this.context = context;
        this.userType = userType;

        requested = "0";
        assigned = "0";
        picked = "0";

    }

    @Override
    public Fragment getItem(int position) {

        if (userType.equals(Config.USER_TYPE_MANAGER)) {

            if (position == 0) {
                return new PickRequestedFragment();
            } else if (position == 1) {
                return new PickAssignedFragment();
            } else {
                return new PickedFragment();
            }

        } else {

            if (position == 0) {
                return new PickAssignedAgentFragment();
            } else {
                return new PickedAgentFragment();
            }

        }

    }

    @Override
    public CharSequence getPageTitle(int position) {

        if (userType.equals(Config.USER_TYPE_MANAGER)) {

            if (position == 0) {
                return context.getString(R.string.pick_request) + " (" + requested + ")";
            } else if (position == 1) {
                return context.getString(R.string.pick_assign) + " (" + assigned + ")";
            } else {
                return context.getString(R.string.picked) + " (" + picked + ")";
            }

        } else {

            if (position == 0) {
                return context.getString(R.string.pick_assign) + " (" + assigned + ")";
            } else {
                return context.getString(R.string.picked) + " (" + picked + ")";
            }

        }


    }

    @Override
    public int getCount() {
        try {

            if (userType.equals(Config.USER_TYPE_MANAGER))
                return 3;
            else
                return 2;

        } catch (Exception e) {

            Log.e("Pick Tab: ", " " + e);

        }

        return 0;

    }

}
