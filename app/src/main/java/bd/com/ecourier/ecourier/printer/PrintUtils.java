package bd.com.ecourier.ecourier.printer;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;

import com.android.print.sdk.Barcode;
import com.android.print.sdk.CanvasPrint;
import com.android.print.sdk.FontProperty;
import com.android.print.sdk.PrinterConstants.BarcodeType;
import com.android.print.sdk.PrinterConstants.Command;
import com.android.print.sdk.PrinterInstance;
import com.android.print.sdk.PrinterType;
import com.android.print.sdk.Table;

import bd.com.ecourier.ecourier.R;


public class PrintUtils {

    public static void printText(Resources resources, PrinterInstance mPrinter) {
        mPrinter.init();
        /*mPrinter.printText(resources.getString(R.string.example_text));*/
        // 换行
        // mPrinter.setPrinter(Command.PRINT_AND_NEWLINE);
        mPrinter.setPrinter(Command.PRINT_AND_WAKE_PAPER_BY_LINE, 2); // 换2行

        //切刀
        //mPrinter.cutPaper();
    }

	/*public static void printNote(Resources resources, PrinterInstance mPrinter,
                                 boolean is58mm) {
		mPrinter.init();
		StringBuffer sb = new StringBuffer();
		// mPrinter.setPrinter(BluetoothPrinter.COMM_LINE_HEIGHT, 80);

		mPrinter.setPrinter(Command.ALIGN, Command.ALIGN_CENTER);
		// 字号横向纵向扩大一倍
		mPrinter.setCharacterMultiple(1, 1);
		mPrinter.printText(resources.getString(R.string.shop_company_title)
				+ "\n");

		mPrinter.setPrinter(Command.ALIGN, Command.ALIGN_LEFT);
		// 字号使用默认
		mPrinter.setCharacterMultiple(0, 0);
		sb.append(resources.getString(R.string.shop_num) + "574001\n");
		sb.append(resources.getString(R.string.shop_receipt_num)
				+ "S00003169\n");
		sb.append(resources.getString(R.string.shop_cashier_num)
				+ "s004_s004\n");

		sb.append(resources.getString(R.string.shop_receipt_date)
				+ "2012-06-17\n");
		sb.append(resources.getString(R.string.shop_print_time)
				+ "2012-06-17 13:37:24\n");
		mPrinter.printText(sb.toString()); // 打印

		printTable1(resources, mPrinter, is58mm); // 打印表格

		sb = new StringBuffer();
		if (is58mm) {
			sb.append(resources.getString(R.string.shop_goods_number)
					+ "                6.00\n");
			sb.append(resources.getString(R.string.shop_goods_total_price)
					+ "                   35.00\n");
			sb.append(resources.getString(R.string.shop_payment)
					+ "                 100.00\n");
			sb.append(resources.getString(R.string.shop_change)
					+ "                  65.00\n");
		} else {
			sb.append(resources.getString(R.string.shop_goods_number)
					+ "                                6.00\n");
			sb.append(resources.getString(R.string.shop_goods_total_price)
					+ "                                35.00\n");
			sb.append(resources.getString(R.string.shop_payment)
					+ "                                100.00\n");
			sb.append(resources.getString(R.string.shop_change)
					+ "                                65.00\n");
		}

		sb.append(resources.getString(R.string.shop_company_name) + "\n");
		sb.append(resources.getString(R.string.shop_company_site)
				+ "www.jiangsu1510.com\n");
		sb.append(resources.getString(R.string.shop_company_address) + "\n");
		sb.append(resources.getString(R.string.shop_company_tel)
				+ "0574-88222999\n");
		sb.append(resources.getString(R.string.shop_Service_Line)
				+ "4008-567-567 \n");
		if (is58mm) {
			sb.append("==============================\n");
		} else {
			sb.append("==============================================\n");
		}
		mPrinter.printText(sb.toString());

		mPrinter.setPrinter(Command.ALIGN, Command.ALIGN_CENTER);
		mPrinter.setCharacterMultiple(0, 1);
		mPrinter.printText(resources.getString(R.string.shop_thanks) + "\n");
		mPrinter.printText(resources.getString(R.string.shop_demo) + "\n\n\n");

		mPrinter.setPrinter(Command.PRINT_AND_WAKE_PAPER_BY_LINE, 2);
	}*/

    public static void printImage(Resources resources, PrinterInstance mPrinter, boolean isStylus) {
        mPrinter.init();
        Bitmap bitmap = BitmapFactory.decodeResource(resources,
                R.drawable.e_courier_logo);
        // getCanvasImage方法获得画布上所画的图像,printImage方法打印图像.
        mPrinter.printText("Print Image:\n");
        if (isStylus) {
            //针打图形,第二个参数为0倍高倍宽， 为1只倍高
            mPrinter.printImageStylus(bitmap, 1);
        } else {
            mPrinter.printImage(bitmap);
        }
        mPrinter.setPrinter(Command.PRINT_AND_WAKE_PAPER_BY_LINE, 2); // 换2行
    }

    public static void printCustomImage(Resources resources,
                                        PrinterInstance mPrinter, boolean isStylus, boolean is58mm) {
        mPrinter.init();
        // TODO Auto-generated method stub

        CanvasPrint cp = new CanvasPrint();
        /*
         * 初始化画布，画布的宽度为变量，一般有两个选择： 1、58mm型号打印机实际可用是48mm，48*8=384px
         * 2、80mm型号打印机实际可用是72mm，72*8=576px 因为画布的高度是无限制的，但从内存分配方面考虑要小于4M比较合适，
         * 所以预置为宽度的5倍。 初始化画笔，默认属性有： 1、消除锯齿 2、设置画笔颜色为黑色
         */
        // init 方法包含cp.initCanvas(550)和cp.initPaint(), T9打印宽度为72mm,其他为47mm.
        if (isStylus) {
            cp.init(PrinterType.T5);
        } else {
            if (is58mm) {
                cp.init(PrinterType.TIII);
            } else {
                cp.init(PrinterType.T9);
            }
        }

        // 非中文使用空格分隔单词
        cp.setUseSplit(true);
        //cp.setUseSplitAndString(true, " ");
        // 阿拉伯文靠右显示
        cp.setTextAlignRight(true);
        /*
         * 插入图片函数: drawImage(float x, float y, String path)
         * 其中(x,y)是指插入图片的左上顶点坐标。
         */
        FontProperty fp = new FontProperty();
        fp.setFont(false, false, false, false, 25, null);
        // 通过初始化的字体属性设置画笔
        cp.setFontProperty(fp);
        cp.drawText("Contains Arabic language:");
        // pg.drawText("温度的影响主要表现在两个方面温度的影响主要表现在两个方面温度的影响主要表现在两个方面温度的影响主要表现在两个方面");
        fp.setFont(false, false, false, false, 30, null);
        cp.setFontProperty(fp);
        /*cp.drawText("ومن تكهناته إيمانه بإستحالة قياس السرعة اللحظية للجسيمات متناهية الصغر والتي تهتز عشوائياً في مختلف الإتجاهات بما يعرف باسم الحركة البراونية، لكن بعد قرن من الزمان، تمكن عالم يدعى مارك رايزن من تفنيد هذه المقولة عملياً بمعمل أبحاثه بجامعة تكساس وإستطاع قياس السرعة اللحظية لتلك الجسيمات، في خضم إختباراته لقانون التوزع المتساوي الذي يقرر أن طاقة حركة الجسيم تعتمد على حرارته بشكل بحت وليس على على كتلته أو حجمه، ");*/
        cp.drawImage(BitmapFactory.decodeResource(resources,
                R.drawable.e_courier_logo_bw_print));

        mPrinter.printText("Print Custom Image:\n");
        if (isStylus) {
            //针打图形,第二个参数为0倍高倍宽， 为1只倍高
            mPrinter.printImageStylus(cp.getCanvasImage(), 1);
        } else {
            mPrinter.printImage(cp.getCanvasImage());
        }

        mPrinter.setPrinter(Command.PRINT_AND_WAKE_PAPER_BY_LINE, 2);
    }

    public static void printTable(Resources resources,
                                  PrinterInstance mPrinter, boolean is58mm, String merchantName, String merchantPhone, String customerName, String Address) {
        mPrinter.init();
        // getTable方法:参数1,以特定符号分隔的列名; 2,列名分隔符;
        // 3,各列所占字符宽度,中文2个,英文1个. 默认字体总共不要超过48
        // 表格超出部分会另起一行打印.若想手动换行,可加\n.
        mPrinter.setCharacterMultiple(0, 0);
        String column = resources.getString(R.string.note_title);
        Table table;
        if (is58mm) {
//			table = new Table(column, ";", new int[] {12, 8, 6, 6 });
            table = new Table(column, ";", new int[]{18, 14});
        } else {
            table = new Table(column, ";", new int[]{16, 8, 8, 12});
        }

        table.setColumnAlignRight(true);
        table.addRow("Merchant: " + merchantName + "Date:--/--/--");
        table.addRow("Mobile: " + merchantPhone);
        table.addRow(";" + "");
        table.addRow("Customer Name: " + customerName);
        table.addRow("Address: " + Address);

		/*
        table.addRow("3," + resources.getString(R.string.frog)
				+ ";1.00;68.00;68.00");
		table.addRow("4," + resources.getString(R.string.cucumber)
				+ ";1.00;4.00;4.00");
		table.addRow("5," + resources.getString(R.string.peanuts)
				+ "; 1.00;5.00;5.00");
		table.addRow("6," + resources.getString(R.string.rice)
				+ ";1.00;2.00;2.00");*/
        mPrinter.printTable(table);

        mPrinter.setPrinter(Command.PRINT_AND_WAKE_PAPER_BY_LINE, 2);
    }

    public static void printTable1(Resources resources,
                                   PrinterInstance mPrinter, boolean is58mm) {
        mPrinter.init();
        String column = resources.getString(R.string.note_title);
        Table table;
        if (is58mm) {
            table = new Table(column, ";", new int[]{13, 8, 5, 6});
        } else {
            table = new Table(column, ";", new int[]{18, 10, 10, 12});
        }
        /*table.addRow("" + resources.getString(R.string.bags) + ";10.00;1;10.00");
        table.addRow("" + resources.getString(R.string.hook) + ";5.00;2;10.00");
		table.addRow("" + resources.getString(R.string.umbrella)
				+ ";5.00;3;15.00");*/
        mPrinter.printTable(table);
    }

    public static void printBarCode(PrinterInstance mPrinter, String content) {
        mPrinter.init();
        mPrinter.setCharacterMultiple(0, 0);

        Barcode barcode;

        // "QRCODE"
        mPrinter.printText("QRCODE\n");
        barcode = new Barcode(BarcodeType.QRCODE, 2, 3, 3, content);
        mPrinter.printBarCode(barcode);

        mPrinter.setPrinter(Command.PRINT_AND_WAKE_PAPER_BY_LINE, 1);
    }

    public static Bitmap convertToBlackWhite(Bitmap bmp) {
        int width = bmp.getWidth(); // 获取位图的宽
        int height = bmp.getHeight(); // 获取位图的高
        int[] pixels = new int[width * height]; // 通过位图的大小创建像素点数组

        bmp.getPixels(pixels, 0, width, 0, 0, width, height);
        int alpha = 0xFF << 24;
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                int grey = pixels[width * i + j];

                int red = ((grey & 0x00FF0000) >> 16);
                int green = ((grey & 0x0000FF00) >> 8);
                int blue = (grey & 0x000000FF);

                grey = (int) (red * 0.3 + green * 0.59 + blue * 0.11);
                grey = alpha | (grey << 16) | (grey << 8) | grey;
                pixels[width * i + j] = grey;
            }
        }
        Bitmap newBmp = Bitmap.createBitmap(width, height, Config.RGB_565);

        newBmp.setPixels(pixels, 0, width, 0, 0, width, height);

        Bitmap resizeBmp = ThumbnailUtils.extractThumbnail(newBmp, 380, 460);
        return resizeBmp;
    }
}
