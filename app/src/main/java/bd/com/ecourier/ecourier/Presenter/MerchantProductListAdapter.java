package bd.com.ecourier.ecourier.Presenter;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;

import bd.com.ecourier.ecourier.Config;
import bd.com.ecourier.ecourier.Model.MerchantProduct;
import bd.com.ecourier.ecourier.R;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by User on 11-Dec-17.
 */

public class MerchantProductListAdapter extends RecyclerView.Adapter<MerchantProductListAdapter.MerchantListAdapterHolder> {

    private LayoutInflater inflater;
    private Activity activity;
    private ArrayList<MerchantProduct> merchantProductArrayList;
    private String fragmentTag, userType;
    private int position;
    private String userID;

    public MerchantProductListAdapter(Activity activity, ArrayList<MerchantProduct> merchantProductArrayList, String fragmentTag) {

        inflater = LayoutInflater.from(activity);
        this.activity = activity;
        this.merchantProductArrayList = merchantProductArrayList;
        this.fragmentTag = fragmentTag;
        userType = activity
                .getSharedPreferences(Config.SP_ECourier_APP, MODE_PRIVATE)
                .getString(Config.SP_USER_TYPE, "");

    }

    public MerchantProductListAdapter(Activity activity, ArrayList<MerchantProduct> merchantProductArrayList, String fragmentTag, String userID) {

        inflater = LayoutInflater.from(activity);
        this.activity = activity;
        this.merchantProductArrayList = merchantProductArrayList;
        this.fragmentTag = fragmentTag;
        userType = activity
                .getSharedPreferences(Config.SP_ECourier_APP, MODE_PRIVATE)
                .getString(Config.SP_USER_TYPE, "");
        this.userID = userID;

    }

    @Override
    public MerchantListAdapterHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MerchantListAdapterHolder(inflater.inflate(R.layout.pick_merchant_info_item, parent, false));
    }

    @Override
    public void onBindViewHolder(MerchantListAdapterHolder holder, int position) {

        MerchantProduct merchantProduct = merchantProductArrayList.get(position);

        holder.id = merchantProduct.getId();

        holder.name.setText(merchantProduct.getName());
        holder.name.setTextColor(merchantProduct.getTextColor());

        holder.address.setText(merchantProduct.getAddress());
        holder.address.setTextColor(merchantProduct.getTextColor());

        holder.parcelAmount.setText(String.format("%s%s", holder.noOfParcelAnnotation, merchantProduct.getNoOfParcel()));
        holder.parcelAmount.setTextColor(merchantProduct.getTextColor());

        holder.phoneNumber = merchantProduct.getPhoneNumber();

        /*holder.productChecked.setChecked(merchantProduct.isChecked());*/

        if (merchantProduct.isChecked()) {
            holder.productChecked.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.ic_check_box_black_24dp));
        } else {
            holder.productChecked.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.ic_check_box_outline_blank_black_24dp));
        }

        holder.merchantView.setBackgroundColor(merchantProduct.getColor());
        holder.address.setMaxLines(merchantProduct.getMaxLine());

        if (fragmentTag.equals(Config.TAG_PICKED_ASSIGNED_MERCHANT_LIST_FRAGMENT) || fragmentTag.equals(Config.TAG_PICKED_MERCHANT_LIST_FRAGMENT) || fragmentTag.equals(Config.TAG_PICKED_ASSIGNED_AGENT_FRAGMENT) || fragmentTag.equals(Config.TAG_PICKED_AGENT_FRAGMENT))
            holder.productChecked.setVisibility(View.GONE);

        if (fragmentTag.equals(Config.TAG_PICKED_ASSIGNED_MERCHANT_LIST_FRAGMENT) && userType.equals(Config.USER_TYPE_MANAGER)) {
            holder.cancelAgent.setVisibility(View.VISIBLE);
            holder.pickParcel.setVisibility(View.GONE);
            holder.confirmParcel.setVisibility(View.GONE);

        } else if (fragmentTag.equals(Config.TAG_PICKED_ASSIGNED_AGENT_FRAGMENT) && userType.equals(Config.USER_TYPE_AGENT)) {

            holder.cancelAgent.setVisibility(View.GONE);
            holder.pickParcel.setVisibility(View.VISIBLE);
            holder.confirmParcel.setVisibility(View.GONE);


        } else if (fragmentTag.equals(Config.TAG_PICKED_MERCHANT_LIST_FRAGMENT) && userType.equals(Config.USER_TYPE_MANAGER)) {

            holder.cancelAgent.setVisibility(View.GONE);
            holder.pickParcel.setVisibility(View.GONE);
            holder.confirmParcel.setVisibility(View.VISIBLE);

        } else {
            holder.cancelAgent.setVisibility(View.GONE);
            holder.pickParcel.setVisibility(View.GONE);
            holder.confirmParcel.setVisibility(View.GONE);
            holder.merchantOptions.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return merchantProductArrayList.size();
    }

    public void listUpdated(MerchantProduct[] merchantProducts) {


        if (merchantProducts.length > 0) {
            Collections.addAll(merchantProductArrayList, merchantProducts);
            notifyDataSetChanged();
        }

    }

    public void clearList() {

        merchantProductArrayList.clear();
        notifyDataSetChanged();

    }

    public int getItemPosition() {

        return position;

    }


    class MerchantListAdapterHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.merchant_name)
        TextView name;
        @BindView(R.id.merchant_address)
        TextView address;
        @BindView(R.id.merchant_no_of_parcel)
        TextView parcelAmount;
        @BindView(R.id.merchant_phone_number)
        ImageView callBtn;
        @BindView(R.id.merchant_select_checkbox)
        ImageView productChecked;
        @BindView(R.id.merchant_info_body)
        LinearLayout merchantInfoBody;
        @BindString(R.string.address)
        String addressAnnotation = "Address: ";
        @BindString(R.string.no_of_parcel)
        String noOfParcelAnnotation = "No of parcelAmount: ";
        @BindView(R.id.merchant_options)
        RelativeLayout merchantOptions;
        @BindView(R.id.cancel_agent)
        ImageView cancelAgent;
        @BindView(R.id.pick_parcel)
        ImageView pickParcel;
        @BindView(R.id.confirm_parcel)
        ImageView confirmParcel;
        @BindView(R.id.merchant_view)
        LinearLayout merchantView;

        private String phoneNumber, id;

        MerchantListAdapterHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick({R.id.merchant_info_body, R.id.cancel_agent, R.id.pick_parcel, R.id.confirm_parcel, R.id.location_icon, R.id.merchant_phone_number, R.id.merchant_select_checkbox})
        public void onClick(View view) {

            switch (view.getId()) {

                case R.id.merchant_info_body: {

                    if (address.getMaxLines() == 3)
                        address.setMaxLines(5);
                    else
                        address.setMaxLines(3);

                    break;
                }

                case R.id.location_icon: {

                    //Uri gmmIntentUri = Uri.parse("geo:0,0?q=1600 Amphitheatre Parkway, Mountain+View, California");
                    Uri gmmIntentUri = Uri.parse("geo:0,0?q=" + Uri.encode(address.getText().toString()));
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    activity.startActivity(mapIntent);

                    break;
                }

                case R.id.merchant_phone_number: {

                    int REQUEST_PHONE_CALL = 1;

                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + phoneNumber));

                    if (ContextCompat.checkSelfPermission(activity,
                            Manifest.permission.CALL_PHONE)
                            != PackageManager.PERMISSION_GRANTED) {

                        // Permission is not granted
                        // Should we show an explanation?
                        if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                                Manifest.permission.CALL_PHONE)) {

                            // Show an explanation to the user *asynchronously* -- don't block
                            // this thread waiting for the user's response! After the user
                            // sees the explanation, try again to request the permission.

                            ActivityCompat.requestPermissions(activity,
                                    new String[]{Manifest.permission.CALL_PHONE},
                                    REQUEST_PHONE_CALL);

                        } else {

                            // No explanation needed; request the permission
                            ActivityCompat.requestPermissions(activity,
                                    new String[]{Manifest.permission.CALL_PHONE},
                                    REQUEST_PHONE_CALL);

                            // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                            // app-defined int constant. The callback method gets the
                            // result of the request.
                        }
                    } else {
                        // Permission has already been granted
                        activity.startActivity(callIntent);


                    }

                    break;
                }

            }

        }


    }

}
