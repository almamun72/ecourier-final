package bd.com.ecourier.ecourier.Model;

import java.util.ArrayList;

/**
 * Created by User on 26-Dec-17.
 */

public class ParcelDeliveredPartialDelivered {

    private String status, sms_code, collected_amount, items_delivered, comments;
    private ArrayList<String> consignment_no;

    public ParcelDeliveredPartialDelivered(ArrayList<String> consignment_no, String status, String sms_code, String collected_amount, String items_delivered, String comments) {

        this.consignment_no = consignment_no;
        this.status = status;
        this.collected_amount = collected_amount;
        this.sms_code = sms_code;
        this.items_delivered = items_delivered;
        this.comments = comments;

    }

    public ArrayList<String> getConsignment_no() {
        return consignment_no;
    }

    public void setConsignment_no(ArrayList<String> consignment_no) {
        this.consignment_no = consignment_no;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSms_code() {
        return sms_code;
    }

    public void setSms_code(String sms_code) {
        this.sms_code = sms_code;
    }

    public String getCollected_amount() {
        return collected_amount;
    }

    public void setCollected_amount(String collected_amount) {
        this.collected_amount = collected_amount;
    }

    public String getItems_delivered() {
        return items_delivered;
    }

    public void setItems_delivered(String items_delivered) {
        this.items_delivered = items_delivered;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }
}
