package bd.com.ecourier.ecourier.Model;

import java.util.ArrayList;

/**
 * Created by User on 25-Dec-17.
 */

public class AgentParcelUpdate {

    private ArrayList<String> consignment_no;
    private String status;

    public AgentParcelUpdate(ArrayList<String> consignment_no, String status) {

        this.consignment_no = consignment_no;
        this.status = status;

    }

    public ArrayList<String> getConsignment_no() {
        return consignment_no;
    }

    public void setConsignment_no(ArrayList<String> consignment_no) {
        this.consignment_no = consignment_no;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
