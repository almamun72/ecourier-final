package bd.com.ecourier.ecourier.Model;

/**
 * Created by User on 02-Jan-18.
 */

public class DeliveredItem {

    private String ecrNumber, productPrice, totalCollected, customerName;

    public DeliveredItem(String ecrNumber, String productPrice, String totalCollected, String customerName) {

        this.ecrNumber = ecrNumber;
        this.productPrice = productPrice;
        this.totalCollected = totalCollected;
        this.customerName = customerName;

    }

    public String getEcrNumber() {
        return ecrNumber;
    }

    public void setEcrNumber(String ecrNumber) {
        this.ecrNumber = ecrNumber;
    }

    public String getProductProce() {
        return productPrice;
    }

    public void setProductProce(String productProce) {
        this.productPrice = productProce;
    }

    public String getTotalCollected() {
        return totalCollected;
    }

    public void setTotalCollected(String totalCollected) {
        this.totalCollected = totalCollected;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }
}
