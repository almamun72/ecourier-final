package bd.com.ecourier.ecourier.View;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bd.com.ecourier.ecourier.API;
import bd.com.ecourier.ecourier.Config;
import bd.com.ecourier.ecourier.Model.AssignAgentPickup;
import bd.com.ecourier.ecourier.Model.MerchantProduct;
import bd.com.ecourier.ecourier.Presenter.MerchantProductListAdapter;
import bd.com.ecourier.ecourier.R;
import bd.com.ecourier.ecourier.clickListener.ClickListener;
import bd.com.ecourier.ecourier.clickListener.RecyclerTouchListener;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by User on 11-Dec-17.
 */

public class PickAssignedMerchantListForAgentFragment extends Fragment {

    @BindView(R.id.swipe_down_to_refresh_list)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.recyclerView_list)
    RecyclerView recyclerView;
    @BindView(R.id.fab_scan)
    FloatingActionButton QRScan;
    @BindView(R.id.recyclerView_list_notice)
    TextView noListNotice;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    private MerchantProductListAdapter merchantProductListAdapter;
    private ArrayList<MerchantProduct> merchantProductArrayList;
    private View rootView;
    private ArrayList<String> qrScanCodes = new ArrayList<>();
    private String agentName;
    private String id;

    public PickAssignedMerchantListForAgentFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {

            id = getArguments().getString(API.KEY_JSON_USER_ID_AGENT);
            agentName = getArguments().getString(API.KEY_AGENT_DETAIL_AGENT_NAME);

        }

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.recycle_view_list_layout, container, false);
        ButterKnife.bind(this, rootView);

        QRScan.setVisibility(View.GONE);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                fetchListData();

            }
        });


        merchantProductArrayList = new ArrayList<>();
        displayAgentDetail();

        showFeed(merchantProductArrayList);
        recyclerView.setVisibility(View.GONE);
        noListNotice.setVisibility(View.VISIBLE);

        if (getContext() != null)
            if (((MainActivity) getContext()).connectedTOInternet)
                fetchListData();

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, final int position) {

                ImageView cancelAgentItem = view.findViewById(R.id.cancel_agent);

                cancelAgentItem.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {

                        if (getActivity() != null) {

                            final Activity activity = getActivity();

                            AlertDialog.Builder builder1 = new AlertDialog.Builder(activity);
                            builder1.setMessage(R.string.cancel_agent_alert);
                            builder1.setCancelable(false);

                            builder1.setPositiveButton(
                                    activity.getResources().getString(R.string.yes),
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {

                                        }
                                    }).setNegativeButton(activity.getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // User cancelled the dialog
                                    dialog.cancel();
                                }
                            });


                            final AlertDialog alert11 = builder1.create();
                            alert11.setOnShowListener(new DialogInterface.OnShowListener() {
                                @Override
                                public void onShow(DialogInterface dialogInterface) {

                                    alert11.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.RED);
                                    alert11.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(activity.getResources().getColor(R.color.colorPrimary));

                                    alert11.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {

                                            Gson gson = new Gson();

                                            ArrayList<String> temp = new ArrayList<>();
                                            temp.add(merchantProductArrayList.get(position).getId());

                                            AssignAgentPickup assignAgentPickup = new AssignAgentPickup(temp, API.STATUS_MANAGER_CANCEL_AGENT_PICK, id, "0");

                                            managerCancelAgentORConfirmPick(gson.toJson(assignAgentPickup), activity);
                                            alert11.dismiss();
                                        }
                                    });

                                }
                            });
                            alert11.show();

                        }

                    }
                });

            }

            @Override
            public void onLongClick(View view, int position) {


            }
        }));

        return rootView;
    }

    private void displayAgentDetail() {

        if (getActivity() != null) {

            try {

                String data = getResources().getString(R.string.pick_assign) + " " + agentName + " " + "(" + merchantProductArrayList.size() + ")";

                noListNotice.setText(data);

            } catch (Exception e) {

                Log.e("Error: ", " " + e);

            }

        }

    }

    private void fetchListData() {

        progressBar.setVisibility(View.VISIBLE);

        RequestQueue queue = null;
        // Instantiate the RequestQueue.
        if (getContext() != null)
            queue = Volley.newRequestQueue(getContext());
        String url = API.baseUrl + API.assignManagerParcelDetail + id;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        progressBar.setVisibility(View.GONE);

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String code = jsonObject.getString(API.KEY_CODE);

                            if (code.equals(API.VALUE_CODE_SUCCESS)) {

                                recyclerView.setVisibility(View.GONE);
                                merchantProductListAdapter.clearList();
                                merchantProductListAdapter.listUpdated(dataFromServer(jsonObject));
                                recyclerView.setVisibility(View.VISIBLE);
                                swipeRefreshLayout.setRefreshing(false);
                                displayAgentDetail();

                            } else {

                                JSONObject error = jsonObject.getJSONObject(API.KEY_JSON_ERROR);

                                if (code.equals(API.VALUE_CODE_AUTHENTICATION_ERROR)) {

                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                } else if (code.equals(API.VALUE_CODE_PARAMETER_ERROR)) {
                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                swipeRefreshLayout.setRefreshing(false);

                try {

                    if (getContext() != null) {

                        if (error.getMessage().equals("com.android.volley.TimeoutError"))
                            Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_timeout), Toast.LENGTH_SHORT).show();

                        Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_unreachable), Toast.LENGTH_SHORT).show();
                    }

                    Log.v("Volly error : ", "" + error);

                } catch (Exception e) {

                    Log.v("Pick Assign E : ", " " + e);

                }
            }

        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                if (getContext() != null) {

                    params.put(API.KEY_JSON_USER_AUTHENTICATION, ((MainActivity) getContext()).authenticationKey);
                    params.put(API.KEY_JSON_USER_GROUP, ((MainActivity) getContext()).userGroup);
                    params.put(API.KEY_USER_ID, ((MainActivity) getContext()).userID);
                    params.put(API.KEY_CONTENT_TYPE, API.VALUE_CONTENT_VALUE_JSON);

                }

                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                Config.volley_time_out,
                Config.max_retries,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Add the request to the RequestQueue.
        if (queue != null)
            queue.add(stringRequest);

    }

    private MerchantProduct[] dataFromServer(JSONObject jsonObject) throws JSONException {

        JSONArray jsonArray = jsonObject.getJSONArray(API.KEY_JSON_DATA);

        displayAgentDetail();

        MerchantProduct[] merchantProducts = new MerchantProduct[jsonArray.length()];

        for (int i = 0; i < jsonArray.length(); i++) {

            JSONObject merchantProduct = jsonArray.getJSONObject(i);

            merchantProducts[i] = new MerchantProduct(merchantProduct.getString(API.KEY_MERCHANT_PRODUCT_PICKUP_ID), merchantProduct.getString(API.KEY_MERCHANT_PRODUCT_MERCHANT_NAME), merchantProduct.getString(API.KEY_MERCHANT_PRODUCT_MERCHANT_ADDRESS), merchantProduct.getString(API.KEY_MERCHANT_PRODUCT_NO_OF_PARCEL), merchantProduct.getString(API.KEY_MERCHANT_PRODUCT_MERCHANT_PHONE));

        }

        return merchantProducts;
    }

    private void showFeed(ArrayList<MerchantProduct> merchantProductArrayList) {

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        merchantProductListAdapter = new MerchantProductListAdapter(getActivity(), merchantProductArrayList, Config.TAG_PICKED_ASSIGNED_MERCHANT_LIST_FRAGMENT, id);
        recyclerView.setAdapter(merchantProductListAdapter);

    }

    private void managerCancelAgentORConfirmPick(final String data, final Activity activity) {

        final ProgressDialog dialog = ProgressDialog.show(getActivity(), "",
                getActivity().getResources().getString(R.string.loading), true);

        dialog.show();

        RequestQueue queue = null;
        // Instantiate the RequestQueue.
        if (activity != null)
            queue = Volley.newRequestQueue(activity);
        String url = API.baseUrl + API.managerPickUpUpdate;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        dialog.cancel();

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String code = jsonObject.getString(API.KEY_CODE);

                            if (code.equals(API.VALUE_CODE_SUCCESS)) {

                                JSONObject data = jsonObject.getJSONObject(API.KEY_JSON_DATA);

                                JSONArray updated = data.getJSONArray(API.KEY_JSON_UPDATED);
                                JSONArray failed = data.getJSONArray(API.KEY_JSON_FAILED);

                                if (updated.length() > 0) {
                                    Toast.makeText(activity, "SUCCESS", Toast.LENGTH_SHORT).show();
                                    fetchListData();


                                } else if (failed.length() > 0) {
                                    Toast.makeText(activity, "FAILED", Toast.LENGTH_SHORT).show();

                                }

                            } else {

                                JSONObject error = jsonObject.getJSONObject(API.KEY_JSON_ERROR);

                                if (code.equals(API.VALUE_CODE_AUTHENTICATION_ERROR)) {

                                    Toast.makeText(activity, "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                } else if (code.equals(API.VALUE_CODE_PARAMETER_ERROR)) {
                                    Toast.makeText(activity, "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                dialog.cancel();

                try {

                    if (getContext() != null) {

                        if (error.getMessage().equals("com.android.volley.TimeoutError"))
                            Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_timeout), Toast.LENGTH_SHORT).show();

                        Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_unreachable), Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    Log.v("Pick Error : ", "" + e);

                }

                Log.v("Volly error : ", "" + error);
            }

        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                if (activity != null) {

                    params.put(API.KEY_JSON_USER_AUTHENTICATION, ((MainActivity) activity).authenticationKey);
                    params.put(API.KEY_JSON_USER_GROUP, ((MainActivity) activity).userGroup);
                    params.put(API.KEY_USER_ID, ((MainActivity) activity).userID);
                    params.put(API.KEY_CONTENT_TYPE, API.VALUE_CONTENT_VALUE_JSON);

                }

                return params;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                String your_string_json = data; // put your json
                return your_string_json.getBytes();
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                Config.volley_time_out,
                Config.max_retries,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Add the request to the RequestQueue.
        if (queue != null)
            queue.add(stringRequest);

    }

}