package bd.com.ecourier.ecourier.Presenter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;

import bd.com.ecourier.ecourier.Model.DeliveredItem;
import bd.com.ecourier.ecourier.R;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by User on 11-Dec-17.
 */

public class DeliveredItemAdapter extends RecyclerView.Adapter<DeliveredItemAdapter.DeliveredItemAdapterHolder> {

    private LayoutInflater inflater;
    private Activity activity;
    private ArrayList<DeliveredItem> deliveredItemArrayList;
    private String fragmentTag;

    public DeliveredItemAdapter(Activity activity, ArrayList<DeliveredItem> deliveredItemArrayList, String fragmentTag) {

        inflater = LayoutInflater.from(activity);
        this.activity = activity;
        this.deliveredItemArrayList = deliveredItemArrayList;
        this.fragmentTag = fragmentTag;

    }

    @Override
    public DeliveredItemAdapterHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new DeliveredItemAdapterHolder(inflater.inflate(R.layout.delivered_item, parent, false));
    }

    @Override
    public void onBindViewHolder(DeliveredItemAdapterHolder holder, int position) {

        DeliveredItem deliveredItem = deliveredItemArrayList.get(position);

        holder.customerName.setText(deliveredItem.getCustomerName());

        holder.collectedPrice.setText(String.format("%s%s%s",
                activity.getResources().getString(R.string.collected_amount),
                deliveredItem.getTotalCollected(),
                activity.getResources().getString(R.string.taka)));

        holder.productPrice.setText(String.format("%s%s%s",
                activity.getResources().getString(R.string.product_price),
                deliveredItem.getProductProce(),
                activity.getResources().getString(R.string.taka)));

        holder.productEcrNumber.setText(deliveredItem.getEcrNumber());
        holder.itemNumber.setText(String.valueOf(position + 1));

    }

    @Override
    public int getItemCount() {
        return deliveredItemArrayList.size();
    }

    public void listUpdated(DeliveredItem[] deliveredItems) {

        deliveredItemArrayList.clear();
        Collections.addAll(deliveredItemArrayList, deliveredItems);
        notifyDataSetChanged();

    }

    class DeliveredItemAdapterHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.customer_name)
        TextView customerName;
        @BindView(R.id.collected_price)
        TextView collectedPrice;
        @BindView(R.id.product_price)
        TextView productPrice;
        @BindView(R.id.product_ecr_number)
        TextView productEcrNumber;
        @BindView(R.id.item_number)
        TextView itemNumber;

        DeliveredItemAdapterHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }


    }
}
