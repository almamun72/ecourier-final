package bd.com.ecourier.ecourier.Model;

import java.util.ArrayList;

/**
 * Created by User on 11-Dec-17.
 */

public class Agent {

    private String name, id, phoneNumber, productCount, totalProductPrice, totalCollectedPrice;
    private ArrayList<String> consignments = new ArrayList<>();

    public Agent(String name, String id, String phoneNumber, String productCount, ArrayList<String> consignments) {

        this.name = name;
        this.id = id;
        this.phoneNumber = phoneNumber;
        this.productCount = productCount;
        this.consignments = consignments;

    }

    public Agent(String name, String id, String phoneNumber, String productCount, ArrayList<String> consignments, String totalProductPrice, String totalCollectedPrice) {

        this.name = name;
        this.id = id;
        this.phoneNumber = phoneNumber;
        this.productCount = productCount;
        this.consignments = consignments;
        this.totalCollectedPrice = totalCollectedPrice;
        this.totalProductPrice = totalProductPrice;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getProductCount() {
        return productCount;
    }

    public void setProductCount(String productCount) {
        this.productCount = productCount;
    }

    public ArrayList<String> getConsignments() {
        return consignments;
    }

    public void setConsignments(ArrayList<String> consignments) {
        this.consignments = consignments;
    }

    public String getTotalProductPrice() {
        return totalProductPrice;
    }

    public void setTotalProductPrice(String totalProductPrice) {
        this.totalProductPrice = totalProductPrice;
    }

    public String getTotalCollectedPrice() {
        return totalCollectedPrice;
    }

    public void setTotalCollectedPrice(String totalCollectedPrice) {
        this.totalCollectedPrice = totalCollectedPrice;
    }
}
