package bd.com.ecourier.ecourier.Model;

import android.graphics.Color;

/**
 * Created by User on 17-Dec-17.
 */

public class CustomerProduct {

    private String ECRNumber, customerName, address, price, merchantName, customerPhoneNumber, merchantPhoneNumber, lastState;
    private boolean isChecked;
    private int maxLine, color, min = 3, max = 8, colorUnselected = Color.WHITE, colorSelected, textColor, colorWhite = Color.WHITE, colorBlack = Color.BLACK;

    public CustomerProduct(String ECRNumber, String customerName, String address, String price, String merchantName, String customerPhoneNumber, String merchantPhoneNumber) {

        this.ECRNumber = ECRNumber;
        this.customerName = customerName;
        this.address = address;
        this.price = price;
        this.merchantName = merchantName;
        this.customerPhoneNumber = customerPhoneNumber;
        this.merchantPhoneNumber = merchantPhoneNumber;
        isChecked = false;
        maxLine = min;
        color = colorUnselected;
        textColor = colorBlack;
        colorSelected = Color.parseColor("#25b472");

    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerAddress() {
        return address;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getECRNumber() {
        return ECRNumber;
    }

    public void setECRNumber(String ECRNumber) {
        this.ECRNumber = ECRNumber;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getCustomerPhoneNumber() {
        return customerPhoneNumber;
    }

    public void setCustomerPhoneNumber(String customerPhoneNumber) {
        this.customerPhoneNumber = customerPhoneNumber;
    }

    public String getMerchantPhoneNumber() {
        return merchantPhoneNumber;
    }

    public void setMerchantPhoneNumber(String merchantPhoneNumber) {
        this.merchantPhoneNumber = merchantPhoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
        if (checked) {

            color = colorSelected;
            textColor = colorWhite;

        } else {

            color = colorUnselected;
            textColor = colorBlack;
        }
    }

    public int getMaxLine() {
        return maxLine;
    }

    public void setMaxLine(int maxLine) {
        this.maxLine = maxLine;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public int getTextColor() {
        return textColor;
    }

    public void setTextColor(int textColor) {
        this.textColor = textColor;
    }

    public String getLastState() {
        return lastState;
    }

    public void setLastState(String lastState) {
        this.lastState = lastState;
    }
}
