package bd.com.ecourier.ecourier.NetworkChangeReceiver;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import bd.com.ecourier.ecourier.View.MainActivity;

/**
 * Created by User on 28-Nov-17.
 */

public class NetworkChangeReceiver extends BroadcastReceiver {
    @SuppressLint("UnsafeProtectedBroadcastReceiver")
    @Override
    public void onReceive(Context context, Intent intent) {

        try {

            boolean isVisible = ECourierApplication.isActivityVisible();// Check if
            // activity
            // is
            // visible
            // or not
            /*Log.i("Activity is Visible ", "Is activity visible : " + isVisible);*/

            // If it is visible then trigger the task else do nothing
            if (isVisible) {
                ConnectivityManager connectivityManager = (ConnectivityManager) context
                        .getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = null;
                if (connectivityManager != null) {
                    networkInfo = connectivityManager
                            .getActiveNetworkInfo();
                }

                // Check internet connection and accrding to state change the
                // text of activity by calling method
                if (networkInfo != null && networkInfo.isConnectedOrConnecting()) {
                    ((MainActivity) context).networkStateChange(true);
                } else {
                    ((MainActivity) context).networkStateChange(false);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        /*ConnectivityManager cm =
                (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = null;
        if (cm != null) {
            activeNetwork = cm.getActiveNetworkInfo();
        }
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        if (ECourierApplication.isActivityVisible())
                new MainActivity().networkStateChange(isConnected);*/

        /*int status = NetworkUtil.getConnectivityStatusString(context);
        Log.e("network reciever", "network reciever");
        if (!"android.net.conn.CONNECTIVITY_CHANGE".equals(intent.getAction())) {
            if(status == NetworkUtil.NETWORK_STATUS_NOT_CONNECTED){
                Toast.makeText(context, "Not Connected", Toast.LENGTH_SHORT).show();
            }else{

                if(status == NetworkUtil.NETWORK_STAUS_WIFI){
                    Toast.makeText(context, "Connected WIFI", Toast.LENGTH_SHORT).show();

                } else if (status == NetworkUtil.NETWORK_STATUS_MOBILE){
                    Toast.makeText(context, "Connected DATA", Toast.LENGTH_SHORT).show();

                }

            }

        }*/
    }
}
