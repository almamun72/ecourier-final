package bd.com.ecourier.ecourier.View;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bd.com.ecourier.ecourier.API;
import bd.com.ecourier.ecourier.Config;
import bd.com.ecourier.ecourier.Model.AssignAgentPickup;
import bd.com.ecourier.ecourier.Model.MerchantProduct;
import bd.com.ecourier.ecourier.Presenter.MerchantProductListAdapter;
import bd.com.ecourier.ecourier.R;
import bd.com.ecourier.ecourier.clickListener.ClickListener;
import bd.com.ecourier.ecourier.clickListener.RecyclerTouchListener;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by User on 11-Dec-17.
 */

public class PickedMerchantListForAgentFragment extends Fragment {

    @BindView(R.id.swipe_down_to_refresh_list)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.recyclerView_list)
    RecyclerView recyclerView;
    @BindView(R.id.fab_scan)
    FloatingActionButton QRScan;
    @BindView(R.id.recyclerView_list_notice)
    TextView noListNotice;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    private MerchantProductListAdapter merchantProductListAdapter;
    private ArrayList<MerchantProduct> merchantProductArrayList;
    private ArrayList<String> consignments;
    private int size = 0;
    private View rootView;
    private String agentName;
    private String id;
    private Gson gson = new Gson();

    public PickedMerchantListForAgentFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {

            id = getArguments().getString(API.KEY_JSON_USER_ID_AGENT);
            agentName = getArguments().getString(API.KEY_AGENT_DETAIL_AGENT_NAME);

        }

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.recycle_view_list_layout, container, false);
        ButterKnife.bind(this, rootView);

        merchantProductArrayList = new ArrayList<>();

        QRScan.setVisibility(View.GONE);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                fetchListData();

            }
        });

        displayAgentDetail();

        showFeed(merchantProductArrayList);
        recyclerView.setVisibility(View.GONE);
        noListNotice.setVisibility(View.VISIBLE);

        if (getContext() != null)
            if (((MainActivity) getContext()).connectedTOInternet)
                fetchListData();

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {

                ImageView pickParcel = view.findViewById(R.id.confirm_parcel);
                final MerchantProduct merchantProduct = merchantProductArrayList.get(position);

                pickParcel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (getContext() != null) {

                            if (merchantProduct.getConsignments().size() > 0) {

                                Bundle bundle = new Bundle();
                                bundle.putStringArrayList(API.KEY_JSON_CONSIGNMENTS, merchantProduct.getConsignments());
                                bundle.putString(API.KEY_MERCHANT_PRODUCT_PICKUP_ID, merchantProduct.getId());
                                bundle.putString(API.KEY_CUSTOMER_PRODUCT_MERCHANT_NAME, merchantProduct.getName());
                                bundle.putString(API.KEY_CUSTOMER_PRODUCT_COLLECTED_AMOUNT, merchantProduct.getNoOfParcel());
                                bundle.putString(API.KEY_JSON_USER_ID_AGENT, id);
                                AutomatedPickFragment automatedPickFragment = new AutomatedPickFragment();
                                automatedPickFragment.setArguments(bundle);

                                ((MainActivity) getContext()).getSupportFragmentManager()
                                        .beginTransaction()
                                        .replace(R.id.content_main, automatedPickFragment)
                                        .addToBackStack(null)
                                        .commit();


                            } else {

                                AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                                builder1.setCancelable(false);

                                View alertView = inflater.inflate(R.layout.agent_pick_parcel_confirm_dialog, null);
                                builder1.setView(alertView);

                                final TextView merchantName = alertView.findViewById(R.id.alert_merchant_name);
                                TextView requestedAmount = alertView.findViewById(R.id.alert_merchant_requested_amount);
                                final EditText amountPicked = alertView.findViewById(R.id.alert_parcel_amount_picked);
                                final EditText otp = alertView.findViewById(R.id.alert_parcel_otp_picked);

                                otp.setVisibility(View.GONE);

                                merchantName.setText(merchantProduct.getName());
                                requestedAmount.setText(String.format("%s%s%s", getContext().getResources().getString(R.string.pick_request), " ", merchantProduct.getNoOfParcel()));

                                builder1.setPositiveButton(
                                        getContext().getResources().getString(R.string.confirm_parcel),
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {

                                            }
                                        }).setNegativeButton(getContext().getResources().getString(R.string.back), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        // User cancelled the dialog
                                        dialog.cancel();
                                    }
                                });

                                final AlertDialog alert11 = builder1.create();
                                alert11.setOnShowListener(new DialogInterface.OnShowListener() {
                                    @Override
                                    public void onShow(DialogInterface arg0) {
                                        alert11.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.RED);
                                        alert11.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getContext().getResources().getColor(R.color.colorPrimary));

                                        alert11.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {

                                                if (!amountPicked.getText().toString().equals("")) {

                                                    ArrayList<String> temp = new ArrayList<>();
                                                    temp.add(String.valueOf(merchantProduct.getId()));

                                                    AssignAgentPickup assignAgentPickup = new AssignAgentPickup(temp, API.STATUS_MANAGER_CONFIRM_AGENT_PICK, id, amountPicked.getText().toString());

                                                    updateStatus(gson.toJson(assignAgentPickup));

                                                    alert11.cancel();
                                                } else {
                                                    Toast.makeText(getActivity(), "Please enter picked amount", Toast.LENGTH_SHORT).show();
                                                }

                                            }
                                        });

                                    }
                                });

                                alert11.show();

                            }


                        }

                    }
                });


            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        return rootView;
    }

    private void displayAgentDetail() {

        if (getActivity() != null) {

            try {

                String data = getResources().getString(R.string.pick_parcel) + " " + agentName + " " + "(" + size + ")";
                noListNotice.setText(data);

            } catch (Exception e) {

                Log.e("Error: ", " " + e);

            }

        }

    }

    private void fetchListData() {

        progressBar.setVisibility(View.VISIBLE);

        RequestQueue queue = null;
        // Instantiate the RequestQueue.
        if (getContext() != null)
            queue = Volley.newRequestQueue(getContext());
        String url = API.baseUrl + API.pickManagerParcelDetail + id;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        progressBar.setVisibility(View.GONE);

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String code = jsonObject.getString(API.KEY_CODE);

                            if (code.equals(API.VALUE_CODE_SUCCESS)) {

                                merchantProductListAdapter.clearList();
                                merchantProductListAdapter.listUpdated(dataFromServer(jsonObject));
                                recyclerView.setVisibility(View.VISIBLE);
                                swipeRefreshLayout.setRefreshing(false);
                                displayAgentDetail();

                            } else {

                                JSONObject error = jsonObject.getJSONObject(API.KEY_JSON_ERROR);

                                if (code.equals(API.VALUE_CODE_AUTHENTICATION_ERROR)) {

                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                } else if (code.equals(API.VALUE_CODE_PARAMETER_ERROR)) {
                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                swipeRefreshLayout.setRefreshing(false);

                try {

                    if (getContext() != null) {

                        if (error.getMessage().equals("com.android.volley.TimeoutError"))
                            Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_timeout), Toast.LENGTH_SHORT).show();

                        Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_unreachable), Toast.LENGTH_SHORT).show();
                    }

                    Log.v("Volly error : ", "" + error);

                } catch (Exception e) {

                    Log.v("Picked agent E : ", " " + e);

                }
            }

        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                if (getContext() != null) {

                    params.put(API.KEY_JSON_USER_AUTHENTICATION, ((MainActivity) getContext()).authenticationKey);
                    params.put(API.KEY_JSON_USER_GROUP, ((MainActivity) getContext()).userGroup);
                    params.put(API.KEY_USER_ID, ((MainActivity) getContext()).userID);
                    params.put(API.KEY_CONTENT_TYPE, API.VALUE_CONTENT_VALUE_JSON);

                }

                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                Config.volley_time_out,
                Config.max_retries,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Add the request to the RequestQueue.
        if (queue != null)
            queue.add(stringRequest);

    }

    private MerchantProduct[] dataFromServer(JSONObject jsonObject) throws JSONException {

        JSONArray jsonArray = jsonObject.getJSONArray(API.KEY_JSON_DATA);

        size = jsonArray.length();

        MerchantProduct[] merchantProducts = new MerchantProduct[jsonArray.length()];

        for (int i = 0; i < jsonArray.length(); i++) {

            JSONObject merchantProduct = jsonArray.getJSONObject(i);
            consignments = new ArrayList<>();

            merchantProducts[i] = new MerchantProduct(merchantProduct.getString(API.KEY_MERCHANT_PRODUCT_PICKUP_ID), merchantProduct.getString(API.KEY_MERCHANT_PRODUCT_MERCHANT_NAME), merchantProduct.getString(API.KEY_MERCHANT_PRODUCT_MERCHANT_ADDRESS), merchantProduct.getString(API.KEY_MERCHANT_PRODUCT_NO_OF_PARCEL), merchantProduct.getString(API.KEY_MERCHANT_PRODUCT_MERCHANT_PHONE));

            if (merchantProduct.has(API.KEY_JSON_CONSIGNMENTS)) {

                JSONArray consignmentDataArray = merchantProduct.getJSONArray(API.KEY_JSON_CONSIGNMENTS);

                for (int j = 0; j < consignmentDataArray.length(); j++) {

                    consignments.add(consignmentDataArray.getString(j));

                }

                merchantProducts[i].setConsignments(consignments);
            }

        }

        return merchantProducts;
    }

    private void showFeed(ArrayList<MerchantProduct> merchantProductArrayList) {

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        merchantProductListAdapter = new MerchantProductListAdapter(getActivity(), merchantProductArrayList, Config.TAG_PICKED_MERCHANT_LIST_FRAGMENT, id);
        recyclerView.setAdapter(merchantProductListAdapter);

    }

    private void resendOTP(final MerchantProduct merchantProduct) {

        RequestQueue queue = null;
        // Instantiate the RequestQueue.
        if (getContext() != null)
            queue = Volley.newRequestQueue(getContext());
        String url = API.baseUrl + API.resendOTP;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String code = jsonObject.getString(API.KEY_CODE);

                            if (code.equals(API.VALUE_CODE_SUCCESS)) {

                                JSONObject data = jsonObject.getJSONObject(API.KEY_JSON_DATA);

                                Toast.makeText(getContext(), " " + data.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_SHORT).show();

                            } else {

                                JSONObject error = jsonObject.getJSONObject(API.KEY_JSON_ERROR);

                                if (code.equals(API.VALUE_CODE_AUTHENTICATION_ERROR)) {

                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                } else if (code.equals(API.VALUE_CODE_PARAMETER_ERROR)) {
                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                try {

                    if (getContext() != null) {

                        if (error.getMessage().equals("com.android.volley.TimeoutError"))
                            Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_timeout), Toast.LENGTH_SHORT).show();

                        Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_unreachable), Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    Log.v("pick mer Error : ", "" + e);

                }

                Log.v("Volly error : ", "" + error);
            }

        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                if (getContext() != null) {

                    params.put(API.KEY_JSON_USER_AUTHENTICATION, ((MainActivity) getContext()).authenticationKey);
                    params.put(API.KEY_JSON_USER_GROUP, ((MainActivity) getContext()).userGroup);
                    params.put(API.KEY_USER_ID, ((MainActivity) getContext()).userID);
                    params.put(API.KEY_CONTENT_TYPE, API.VALUE_CONTENT_VALUE);

                }

                return params;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put(API.KEY_OTP_TYPE, API.VALUE_OTP_TYPE_PICK);
                params.put(API.KEY_ORDER_ID, merchantProduct.getId());
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                Config.volley_time_out,
                Config.max_retries,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Add the request to the RequestQueue.
        if (queue != null)
            queue.add(stringRequest);

    }

    private void updateStatus(final String data) {

        final ProgressDialog dialog = ProgressDialog.show(getActivity(), "",
                getActivity().getResources().getString(R.string.loading), true);

        dialog.show();

        RequestQueue queue = null;
        // Instantiate the RequestQueue.
        if (getContext() != null)
            queue = Volley.newRequestQueue(getContext());
        String url = API.baseUrl + API.managerPickUpUpdate;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        swipeRefreshLayout.setRefreshing(false);
                        dialog.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String code = jsonObject.getString(API.KEY_CODE);

                            if (code.equals(API.VALUE_CODE_SUCCESS)) {

                                JSONObject data = jsonObject.getJSONObject(API.KEY_JSON_DATA);

                                String updated = data.getString(API.KEY_JSON_UPDATED);

                                if (updated.length() > 0) {
                                    Toast.makeText(getContext(), "SUCCESS ", Toast.LENGTH_SHORT).show();
                                    fetchListData();

                                }

                            } else {

                                JSONObject error = jsonObject.getJSONObject(API.KEY_JSON_ERROR);

                                if (code.equals(API.VALUE_CODE_AUTHENTICATION_ERROR)) {

                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                } else if (code.equals(API.VALUE_CODE_PARAMETER_ERROR)) {
                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                swipeRefreshLayout.setRefreshing(false);
                dialog.dismiss();
                try {

                    if (getContext() != null) {

                        if (error.getMessage().equals("com.android.volley.TimeoutError"))
                            Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_timeout), Toast.LENGTH_SHORT).show();

                        Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_unreachable), Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    Log.v("pick mer Error : ", "" + e);

                }

                Log.v("Volly error : ", "" + error);
            }

        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                if (getContext() != null) {

                    params.put(API.KEY_JSON_USER_AUTHENTICATION, ((MainActivity) getContext()).authenticationKey);
                    params.put(API.KEY_JSON_USER_GROUP, ((MainActivity) getContext()).userGroup);
                    params.put(API.KEY_USER_ID, ((MainActivity) getContext()).userID);
                    params.put(API.KEY_CONTENT_TYPE, API.VALUE_CONTENT_VALUE_JSON);

                }

                return params;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                String your_string_json = data; // put your json
                return your_string_json.getBytes();
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                Config.volley_time_out,
                Config.max_retries,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Add the request to the RequestQueue.
        if (queue != null)
            queue.add(stringRequest);

    }

}
