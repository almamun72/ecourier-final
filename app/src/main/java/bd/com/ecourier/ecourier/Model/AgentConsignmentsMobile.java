package bd.com.ecourier.ecourier.Model;

import java.util.ArrayList;

/**
 * Created by User on 25-Dec-17.
 */

public class AgentConsignmentsMobile {

    private ArrayList<String> mobile;

    public AgentConsignmentsMobile(ArrayList<String> mobile) {

        this.mobile = mobile;

    }

    public ArrayList<String> getMobile() {
        return mobile;
    }

    public void setMobile(ArrayList<String> mobile) {
        this.mobile = mobile;
    }
}
