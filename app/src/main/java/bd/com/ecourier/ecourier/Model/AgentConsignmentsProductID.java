package bd.com.ecourier.ecourier.Model;

import java.util.ArrayList;

/**
 * Created by User on 25-Dec-17.
 */

public class AgentConsignmentsProductID {

    private ArrayList<String> product_id;

    public AgentConsignmentsProductID(ArrayList<String> product_id) {

        this.product_id = product_id;

    }

    public ArrayList<String> getProduct_id() {
        return product_id;
    }

    public void setProduct_id(ArrayList<String> product_id) {
        this.product_id = product_id;
    }
}
