package bd.com.ecourier.ecourier.View;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bd.com.ecourier.ecourier.API;
import bd.com.ecourier.ecourier.Config;
import bd.com.ecourier.ecourier.Model.AssignAgentPickup;
import bd.com.ecourier.ecourier.Model.MerchantProduct;
import bd.com.ecourier.ecourier.Presenter.MerchantProductListAdapter;
import bd.com.ecourier.ecourier.R;
import bd.com.ecourier.ecourier.clickListener.ClickListener;
import bd.com.ecourier.ecourier.clickListener.RecyclerTouchListener;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.graphics.Paint.ANTI_ALIAS_FLAG;

/**
 * Created by User on 10-Dec-17.
 */

public class PickRequestedFragment extends Fragment {

    @BindView(R.id.swipe_down_to_refresh_list)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.recyclerView_list)
    RecyclerView recyclerView;
    @BindView(R.id.fab_scan)
    FloatingActionButton QRScan;
    @BindView(R.id.recyclerView_list_notice)
    TextView noListNotice;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    private MerchantProduct merchantProduct;
    private MerchantProductListAdapter merchantProductListAdapter;
    private ArrayList<MerchantProduct> merchantProductArrayList = new ArrayList<>();
    private ArrayList<MerchantProduct> merchantProductArrayListForServer = new ArrayList<>();
    private ArrayList<String> merchantProductsIDListForServer = new ArrayList<>();
    private View rootView;
    private ArrayList<String> qrScanCodes = new ArrayList<>();
    private Gson gson = new Gson();
    private String json;

    public PickRequestedFragment() {
    }

    public static Bitmap textAsBitmap(String text, float textSize, int textColor) {
        Paint paint = new Paint(ANTI_ALIAS_FLAG);
        paint.setTextSize(textSize);
        paint.setColor(textColor);
        paint.setTextAlign(Paint.Align.LEFT);
        float baseline = -paint.ascent(); // ascent() is negative
        int width = (int) (paint.measureText(text) + 0.0f); // round
        int height = (int) (baseline + paint.descent() + 0.0f);
        Bitmap image = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(image);
        canvas.drawText(text, 0, baseline, paint);
        return image;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.recycle_view_list_layout, container, false);
        ButterKnife.bind(this, rootView);

        QRScan.hide();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                refreshList();

            }
        });

        showFeed(merchantProductArrayList);
        recyclerView.setVisibility(View.GONE);
        noListNotice.setVisibility(View.VISIBLE);

        /*if (getContext() != null)
            if (((MainActivity) getContext()).connectedTOInternet)
                fetchListData();*/

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, final int position) {

                merchantProduct = merchantProductArrayList.get(position);
                ImageView checkBox = view.findViewById(R.id.merchant_select_checkbox);

                checkBox.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {

                        selectMerchantItem();
                        merchantProductListAdapter.notifyItemChanged(position);

                    }
                });

            }

            @Override
            public void onLongClick(View view, int position) {

                /*merchantProduct = merchantProductArrayList.get(position);

                selectMerchantItem();
                merchantProductListAdapter.notifyItemChanged(position);*/

            }
        }));

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (((LinearLayoutManager) recyclerView.getLayoutManager()).findLastCompletelyVisibleItemPosition() > (merchantProductArrayList.size() - API.FETCH_DATA_AT_SCROLL) && newState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE) {
                    fetchListData();

                }

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });

        return rootView;
    }

    private void refreshList() {

        merchantProductListAdapter.clearList();
        fetchListData();
        merchantProductArrayListForServer.clear();
        merchantProductsIDListForServer.clear();
        QRScan.hide();

        if ((getContext() != null))
            ((MainActivity) getContext()).refreshPickTabTitle();
    }

    private void selectMerchantItem() {

        merchantProduct.setChecked(!merchantProduct.isChecked());
        if (merchantProduct.isChecked())
            merchantProductArrayListForServer.add(merchantProduct);
        else
            merchantProductArrayListForServer.remove(merchantProduct);

        if (merchantProductArrayListForServer.size() == 0) {
            QRScan.hide();

        } else {

            QRScan.show();
            QRScan.setImageBitmap(textAsBitmap("" + merchantProductArrayListForServer.size(), 40, Color.WHITE));

        }


    }

    private void fetchListData() {

        progressBar.setVisibility(View.VISIBLE);
        swipeRefreshLayout.setEnabled(false);

        RequestQueue queue = null;
        // Instantiate the RequestQueue.
        if (getContext() != null)
            queue = Volley.newRequestQueue(getContext());
        String url = API.baseUrl + API.managerPickRequest + API.LIMIT + API.DATA_LIMIT + API.OFFSET + merchantProductArrayList.size();

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        progressBar.setVisibility(View.GONE);

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String code = jsonObject.getString(API.KEY_CODE);

                            if (code.equals(API.VALUE_CODE_SUCCESS)) {

                                merchantProductListAdapter.listUpdated(dataFromServer(jsonObject));
                                recyclerView.setVisibility(View.VISIBLE);
                                noListNotice.setVisibility(View.GONE);
                                swipeRefreshLayout.setRefreshing(false);

                                JSONArray jsonArray = jsonObject.getJSONArray(API.KEY_JSON_DATA);
                                if (jsonArray.length() > 0)
                                    fetchListData();
                                else
                                    swipeRefreshLayout.setEnabled(true);

                            } else {

                                JSONObject error = jsonObject.getJSONObject(API.KEY_JSON_ERROR);

                                if (code.equals(API.VALUE_CODE_AUTHENTICATION_ERROR)) {

                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                } else if (code.equals(API.VALUE_CODE_PARAMETER_ERROR)) {
                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                swipeRefreshLayout.setRefreshing(false);
                swipeRefreshLayout.setEnabled(true);

                try {

                    if (getContext() != null) {

                        if (error.getMessage().equals("com.android.volley.TimeoutError"))
                            Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_timeout), Toast.LENGTH_SHORT).show();

                        Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_unreachable), Toast.LENGTH_SHORT).show();
                    }

                    Log.v("Volly error : ", "" + error);

                } catch (Exception e) {

                    Log.v("Receive Ful E : ", " " + e);

                }
            }

        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                if (getContext() != null) {
                    params.put(API.KEY_JSON_USER_AUTHENTICATION, ((MainActivity) getContext()).authenticationKey);
                    params.put(API.KEY_JSON_USER_GROUP, ((MainActivity) getContext()).userGroup);
                    params.put(API.KEY_USER_ID, ((MainActivity) getContext()).userID);
                    params.put(API.KEY_CONTENT_TYPE, API.VALUE_CONTENT_VALUE);
                }


                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                Config.volley_time_out,
                Config.max_retries,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Add the request to the RequestQueue.
        if (queue != null)
            queue.add(stringRequest);

    }

    private MerchantProduct[] dataFromServer(JSONObject jsonObject) throws JSONException {

        JSONArray jsonArray = jsonObject.getJSONArray(API.KEY_JSON_DATA);

        MerchantProduct[] merchantProducts = new MerchantProduct[jsonArray.length()];

        for (int i = 0; i < jsonArray.length(); i++) {

            JSONObject merchantProduct = jsonArray.getJSONObject(i);

            merchantProducts[i] = new MerchantProduct(merchantProduct.getString(API.KEY_MERCHANT_PRODUCT_PICKUP_ID), merchantProduct.getString(API.KEY_MERCHANT_PRODUCT_MERCHANT_NAME), merchantProduct.getString(API.KEY_MERCHANT_PRODUCT_MERCHANT_ADDRESS), merchantProduct.getString(API.KEY_MERCHANT_PRODUCT_NO_OF_PARCEL), merchantProduct.getString(API.KEY_MERCHANT_PRODUCT_MERCHANT_PHONE));

        }

        return merchantProducts;
    }

    private void showFeed(ArrayList<MerchantProduct> merchantProducts) {

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        merchantProductListAdapter = new MerchantProductListAdapter(getActivity(), merchantProducts, Config.TAG_PICKED_REQUESTED_FRAGMENT);
        recyclerView.setAdapter(merchantProductListAdapter);

    }

    @OnClick({R.id.fab_scan})
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.fab_scan: {

                for (int i = 0; i < merchantProductArrayListForServer.size(); i++) {

                    merchantProductsIDListForServer.add(merchantProductArrayListForServer.get(i).getId());

                }

                if (getContext() != null) {

                    AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                    builder1.setCancelable(false);

                    LayoutInflater inflater = this.getLayoutInflater();
                    View alertView = inflater.inflate(R.layout.select_spinner, null);
                    builder1.setView(alertView);

                    final Spinner agentSpinner = alertView.findViewById(R.id.list_spinner);
                    ArrayAdapter<String> agentListAdapter;
                    agentListAdapter = new ArrayAdapter<String>(getContext(), R.layout.spinner_item, R.id.spinner_item_text, ((MainActivity) getContext()).agentNameList);
                    agentSpinner.setAdapter(agentListAdapter);

                    TextInputLayout cmt = alertView.findViewById(R.id.id_edit_text_input_layout_comment);
                    cmt.setVisibility(View.GONE);

                    builder1.setPositiveButton(
                            getContext().getResources().getString(R.string.confirm_parcel),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            }).setNegativeButton(getContext().getResources().getString(R.string.back), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User cancelled the dialog
                            merchantProductsIDListForServer.clear();
                            dialog.cancel();
                        }
                    });

                    final AlertDialog alert11 = builder1.create();
                    alert11.setOnShowListener(new DialogInterface.OnShowListener() {
                        @Override
                        public void onShow(DialogInterface arg0) {
                            alert11.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.RED);
                            alert11.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getContext().getResources().getColor(R.color.colorPrimary));

                            alert11.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    if (agentSpinner.getSelectedItemPosition() != 0) {

                                        AssignAgentPickup assignAgentPickup = new AssignAgentPickup(merchantProductsIDListForServer, API.STATUS_ASSIGN_AGENT, ((MainActivity) getContext()).agentIDList.get(agentSpinner.getSelectedItemPosition()), "0");

                                        updateStatus(gson.toJson(assignAgentPickup));
                                        alert11.dismiss();

                                    } else {
                                        Toast.makeText(getContext(), "Please Select an agent", Toast.LENGTH_SHORT).show();
                                    }


                                }
                            });

                        }
                    });

                    alert11.show();

                }

                break;
            }

        }

    }

    private void updateStatus(final String data) {

        final ProgressDialog dialog = ProgressDialog.show(getActivity(), "",
                getActivity().getResources().getString(R.string.loading), true);

        dialog.show();

        RequestQueue queue = null;
        // Instantiate the RequestQueue.
        if (getContext() != null)
            queue = Volley.newRequestQueue(getContext());
        String url = API.baseUrl + API.managerPickUpUpdate;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        swipeRefreshLayout.setRefreshing(false);
                        dialog.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String code = jsonObject.getString(API.KEY_CODE);


                            if (code.equals(API.VALUE_CODE_SUCCESS)) {

                                JSONObject data = jsonObject.getJSONObject(API.KEY_JSON_DATA);

                                JSONArray updated = data.getJSONArray(API.KEY_JSON_UPDATED);
                                JSONArray failed = data.getJSONArray(API.KEY_JSON_FAILED);

                                if (updated.length() > 0) {
                                    Toast.makeText(getContext(), "SUCCESS", Toast.LENGTH_SHORT).show();
                                    refreshList();
                                } else if (failed.length() > 0) {
                                    Toast.makeText(getContext(), "FAILED", Toast.LENGTH_SHORT).show();

                                }

                            } else {

                                JSONObject error = jsonObject.getJSONObject(API.KEY_JSON_ERROR);

                                if (code.equals(API.VALUE_CODE_AUTHENTICATION_ERROR)) {

                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                } else if (code.equals(API.VALUE_CODE_PARAMETER_ERROR)) {
                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                swipeRefreshLayout.setRefreshing(false);
                dialog.dismiss();
                try {

                    if (getContext() != null) {

                        if (error.getMessage().equals("com.android.volley.TimeoutError"))
                            Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_timeout), Toast.LENGTH_SHORT).show();

                        Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_unreachable), Toast.LENGTH_SHORT).show();
                    }

                    Log.v("Volly error : ", "" + error);

                } catch (Exception e) {

                    Log.v("Receive Ful E : ", " " + e);

                }
            }

        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                if (getContext() != null) {

                    params.put(API.KEY_JSON_USER_AUTHENTICATION, ((MainActivity) getContext()).authenticationKey);
                    params.put(API.KEY_JSON_USER_GROUP, ((MainActivity) getContext()).userGroup);
                    params.put(API.KEY_USER_ID, ((MainActivity) getContext()).userID);
                    params.put(API.KEY_CONTENT_TYPE, API.VALUE_CONTENT_VALUE_JSON);

                }

                return params;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                String your_string_json = data; // put your json
                return your_string_json.getBytes();
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                Config.volley_time_out,
                Config.max_retries,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Add the request to the RequestQueue.
        if (queue != null)
            queue.add(stringRequest);

    }

    @Override
    public void onPause() {
        super.onPause();

        merchantProductListAdapter.clearList();
        merchantProductArrayListForServer.clear();
        merchantProductsIDListForServer.clear();
        QRScan.hide();

    }

    @Override
    public void onResume() {
        super.onResume();

        if (getContext() != null)
            if (((MainActivity) getContext()).connectedTOInternet)
                fetchListData();

    }
}
