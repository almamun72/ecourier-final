package bd.com.ecourier.ecourier.Model;

import java.util.ArrayList;

/**
 * Created by User on 25-Dec-17.
 */

public class ManagerAssignParcel {

    private ArrayList<String> consignment_no;
    private String status;
    private String agent_id;

    public ManagerAssignParcel(ArrayList<String> consignment_no, String status, String agent_id) {

        this.consignment_no = consignment_no;
        this.status = status;
        this.agent_id = agent_id;

    }

    public ArrayList<String> getConsignment_no() {
        return consignment_no;
    }

    public void setConsignment_no(ArrayList<String> consignment_no) {
        this.consignment_no = consignment_no;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAgent_id() {
        return agent_id;
    }

    public void setAgent_id(String agent_id) {
        this.agent_id = agent_id;
    }
}
