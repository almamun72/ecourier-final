package bd.com.ecourier.ecourier.View;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;

import bd.com.ecourier.ecourier.Config;
import bd.com.ecourier.ecourier.R;
import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by User on 10-Dec-17.
 */

public class SettingsFragment extends Fragment {

    @BindView(R.id.bangla_lang_switch)
    Switch banglaLangSwitch;

    private View rootView;
    private SharedPreferences sp;
    private Intent mainActivityIntent;


    public SettingsFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.settings_layout, container, false);
        ButterKnife.bind(this, rootView);

        String language = "en";

        if (getActivity() != null) {

            mainActivityIntent = new Intent(getActivity(), MainActivity.class);
            getActivity().setTitle(getResources().getString(R.string.action_settings));
            sp = getActivity().getSharedPreferences(Config.SP_ECourier_APP, MODE_PRIVATE);
            language = getActivity().getSharedPreferences(Config.SP_ECourier_APP, MODE_PRIVATE).getString(Config.SP_LANGUAGE, "en");

        }

        final SharedPreferences.Editor editor = sp.edit();

        if (language.equals("bn"))
            banglaLangSwitch.setChecked(true);

        banglaLangSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (b) {
                    editor.putString(Config.SP_LANGUAGE, "bn");
                    if (editor.commit()) {
                        startActivity(mainActivityIntent);
                        getActivity().finish();
                    }
                } else {
                    editor.putString(Config.SP_LANGUAGE, "en");
                    if (editor.commit()) {
                        startActivity(mainActivityIntent);
                        getActivity().finish();
                    }
                }

            }
        });


        return rootView;
    }

}
