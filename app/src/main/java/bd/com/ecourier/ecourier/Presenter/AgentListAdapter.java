package bd.com.ecourier.ecourier.Presenter;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;

import bd.com.ecourier.ecourier.API;
import bd.com.ecourier.ecourier.Config;
import bd.com.ecourier.ecourier.Model.Agent;
import bd.com.ecourier.ecourier.R;
import bd.com.ecourier.ecourier.View.MainActivity;
import bd.com.ecourier.ecourier.View.PendingDeliveryForAgentFragment;
import bd.com.ecourier.ecourier.View.PendingReturnForAgentFragment;
import bd.com.ecourier.ecourier.View.PickAssignedMerchantListForAgentFragment;
import bd.com.ecourier.ecourier.View.PickedMerchantListForAgentFragment;
import bd.com.ecourier.ecourier.View.ReceiveCancelCustomerProductFromAgentFragment;
import bd.com.ecourier.ecourier.View.ReceiveCashFromAgentFragment;
import bd.com.ecourier.ecourier.View.ReceiveHoldCustomerProductFromAgentFragment;
import bd.com.ecourier.ecourier.View.ReceiveReturnCustomerProductFromAgentFragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by User on 11-Dec-17.
 */

public class AgentListAdapter extends RecyclerView.Adapter<AgentListAdapter.AgentListAdapterHolder> {

    private LayoutInflater inflater;
    private Activity activity;
    private ArrayList<Agent> agentArrayList;
    private String fragmentTag;

    public AgentListAdapter(Activity activity, ArrayList<Agent> agentArrayList, String fragmentTag) {

        inflater = LayoutInflater.from(activity);
        this.activity = activity;
        this.agentArrayList = agentArrayList;
        this.fragmentTag = fragmentTag;

    }

    @Override
    public AgentListAdapterHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new AgentListAdapter.AgentListAdapterHolder(inflater.inflate(R.layout.agent_info_item, parent, false));

    }

    @Override
    public void onBindViewHolder(AgentListAdapterHolder holder, int position) {

        Agent agent = agentArrayList.get(position);

        holder.name.setText(String.format("%s%s", activity.getResources().getString(R.string.agent_name), agent.getName()));
        holder.agentName = agent.getName();
        holder.id.setText(String.format("%s%s", activity.getResources().getString(R.string.agent_id), agent.getId()));
        holder.agentId = agent.getId();
        holder.productCount.setText(String.format("%s%s", activity.getResources().getString(R.string.total_product), String.valueOf(agent.getProductCount())));
        holder.phoneNumber = agent.getPhoneNumber();
        holder.consignments = agent.getConsignments();

        if (fragmentTag.equals(Config.TAG_RECEIVE_CASH_FRAGMENT)) {
            holder.collectedPrice.setVisibility(View.VISIBLE);
            holder.totalPrice.setVisibility(View.VISIBLE);
            holder.collectedPrice.setText(String.format("%s%s", activity.getResources().getString(R.string.total_collected), agent.getTotalCollectedPrice()));
            holder.totalPrice.setText(String.format("%s%s", activity.getResources().getString(R.string.total), agent.getTotalProductPrice()));

        } else if (fragmentTag.equals(Config.TAG_AGENT_LIST_FRAGMENT)) {

            holder.productCount.setVisibility(View.GONE);
            holder.collectedPrice.setVisibility(View.GONE);
            holder.totalPrice.setVisibility(View.GONE);

        } else {

            holder.collectedPrice.setVisibility(View.GONE);
            holder.totalPrice.setVisibility(View.GONE);

        }

    }

    @Override
    public int getItemCount() {
        return agentArrayList.size();
    }

    public void listUpdated(Agent[] agents) {

        agentArrayList.clear();
        Collections.addAll(agentArrayList, agents);
        notifyDataSetChanged();

    }

    class AgentListAdapterHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.agent_name)
        TextView name;
        @BindView(R.id.agent_id)
        TextView id;
        @BindView(R.id.agent_product_count)
        TextView productCount;
        @BindView(R.id.agent_call_btn)
        ImageView callBtn;
        @BindView(R.id.agent_list_body)
        RelativeLayout agentListBody;
        @BindView(R.id.collected_price)
        TextView collectedPrice;
        @BindView(R.id.total_price)
        TextView totalPrice;

        private Bundle bundle = new Bundle();

        private String phoneNumber, agentId, agentName;
        private ArrayList<String> consignments;

        AgentListAdapterHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick({R.id.agent_list_body, R.id.agent_call_btn})
        void onClick(View view) {

            switch (view.getId()) {

                case R.id.agent_list_body: {

                    if (fragmentTag.equals(Config.TAG_PICKED_ASSIGNED_FRAGMENT)) {

                        bundle.putString(API.KEY_JSON_USER_ID_AGENT, agentId);
                        bundle.putString(API.KEY_AGENT_DETAIL_AGENT_NAME, agentName);

                        PickAssignedMerchantListForAgentFragment pickAssignedMerchantListForAgentFragment = new PickAssignedMerchantListForAgentFragment();

                        pickAssignedMerchantListForAgentFragment.setArguments(bundle);

                        ((MainActivity) activity).getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.content_main, pickAssignedMerchantListForAgentFragment)
                                .addToBackStack(null)
                                .commit();
                    } else if (fragmentTag.equals(Config.TAG_PICKED_FRAGMENT)) {

                        bundle.putString(API.KEY_JSON_USER_ID_AGENT, agentId);
                        bundle.putString(API.KEY_AGENT_DETAIL_AGENT_NAME, agentName);

                        PickedMerchantListForAgentFragment pickedMerchantListForAgentFragment = new PickedMerchantListForAgentFragment();

                        pickedMerchantListForAgentFragment.setArguments(bundle);

                        ((MainActivity) activity).getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.content_main, pickedMerchantListForAgentFragment)
                                .addToBackStack(null)
                                .commit();
                    } else if (fragmentTag.equals(Config.TAG_RECEIVE_RETURN_FRAGMENT)) {

                        bundle.putStringArrayList(API.KEY_JSON_CONSIGNMENTS, consignments);
                        bundle.putString(API.KEY_AGENT_DETAIL_AGENT_NAME, agentName);

                        ReceiveReturnCustomerProductFromAgentFragment receiveReturnCustomerProductFromAgentFragment = new ReceiveReturnCustomerProductFromAgentFragment();

                        receiveReturnCustomerProductFromAgentFragment.setArguments(bundle);

                        ((MainActivity) activity).getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.content_main, receiveReturnCustomerProductFromAgentFragment)
                                .addToBackStack(null)
                                .commit();

                    } else if (fragmentTag.equals(Config.TAG_RECEIVE_HOLD_FRAGMENT)) {

                        bundle.putStringArrayList(API.KEY_JSON_CONSIGNMENTS, consignments);
                        bundle.putString(API.KEY_AGENT_DETAIL_AGENT_NAME, agentName);

                        ReceiveHoldCustomerProductFromAgentFragment receiveHoldCustomerProductFromAgentFragment = new ReceiveHoldCustomerProductFromAgentFragment();

                        receiveHoldCustomerProductFromAgentFragment.setArguments(bundle);

                        ((MainActivity) activity).getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.content_main, receiveHoldCustomerProductFromAgentFragment)
                                .addToBackStack(null)
                                .commit();

                    } else if (fragmentTag.equals(Config.TAG_RECEIVE_CANCEL_FRAGMENT)) {

                        bundle.putStringArrayList(API.KEY_JSON_CONSIGNMENTS, consignments);
                        bundle.putString(API.KEY_AGENT_DETAIL_AGENT_NAME, agentName);

                        ReceiveCancelCustomerProductFromAgentFragment receiveCancelCustomerProductFromAgentFragment = new ReceiveCancelCustomerProductFromAgentFragment();

                        receiveCancelCustomerProductFromAgentFragment.setArguments(bundle);

                        ((MainActivity) activity).getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.content_main, receiveCancelCustomerProductFromAgentFragment)
                                .addToBackStack(null)
                                .commit();

                    } else if (fragmentTag.equals(Config.TAG_PENDING_DELIVERY_FRAGMENT)) {

                        bundle.putStringArrayList(API.KEY_JSON_CONSIGNMENTS, consignments);
                        bundle.putString(API.KEY_AGENT_DETAIL_AGENT_NAME, agentName);

                        PendingDeliveryForAgentFragment pendingDeliveryForAgentFragment = new PendingDeliveryForAgentFragment();

                        pendingDeliveryForAgentFragment.setArguments(bundle);

                        ((MainActivity) activity).getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.content_main, pendingDeliveryForAgentFragment)
                                .addToBackStack(null)
                                .commit();
                    } else if (fragmentTag.equals(Config.TAG_PENDING_RETURN_FRAGMENT)) {

                        bundle.putStringArrayList(API.KEY_JSON_CONSIGNMENTS, consignments);
                        bundle.putString(API.KEY_AGENT_DETAIL_AGENT_NAME, agentName);

                        PendingReturnForAgentFragment pendingReturnForAgentFragment = new PendingReturnForAgentFragment();

                        pendingReturnForAgentFragment.setArguments(bundle);

                        ((MainActivity) activity).getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.content_main, pendingReturnForAgentFragment)
                                .addToBackStack(null)
                                .commit();

                    } else if (fragmentTag.equals(Config.TAG_RECEIVE_CASH_FRAGMENT)) {

                        bundle.putStringArrayList(API.KEY_JSON_CONSIGNMENTS, consignments);

                        ReceiveCashFromAgentFragment receiveCashFromAgentFragment = new ReceiveCashFromAgentFragment();

                        receiveCashFromAgentFragment.setArguments(bundle);

                        ((MainActivity) activity).getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.content_main, receiveCashFromAgentFragment)
                                .addToBackStack(null)
                                .commit();
                    }

                    break;
                }

                case R.id.agent_call_btn: {

                    int REQUEST_PHONE_CALL = 1;

                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + phoneNumber));

                    if (ContextCompat.checkSelfPermission(activity,
                            Manifest.permission.CALL_PHONE)
                            != PackageManager.PERMISSION_GRANTED) {

                        // Permission is not granted
                        // Should we show an explanation?
                        if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                                Manifest.permission.CALL_PHONE)) {

                            // Show an explanation to the user *asynchronously* -- don't block
                            // this thread waiting for the user's response! After the user
                            // sees the explanation, try again to request the permission.

                            ActivityCompat.requestPermissions(activity,
                                    new String[]{Manifest.permission.CALL_PHONE},
                                    REQUEST_PHONE_CALL);

                        } else {

                            // No explanation needed; request the permission
                            ActivityCompat.requestPermissions(activity,
                                    new String[]{Manifest.permission.CALL_PHONE},
                                    REQUEST_PHONE_CALL);

                            // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                            // app-defined int constant. The callback method gets the
                            // result of the request.
                        }
                    } else {
                        // Permission has already been granted
                        activity.startActivity(callIntent);


                    }

                    break;

                }

            }

        }

    }
}
