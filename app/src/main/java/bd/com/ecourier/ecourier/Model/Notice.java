package bd.com.ecourier.ecourier.Model;

/**
 * Created by User on 02-Jan-18.
 */

public class Notice {

    private String title, description, publishedDate, id;

    public Notice(String id, String title, String description, String publishedDate) {

        this.id = id;
        this.title = title;
        this.description = description;
        this.publishedDate = publishedDate;

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPublishedDate() {
        return publishedDate;
    }

    public void setPublishedDate(String publishedDate) {
        this.publishedDate = publishedDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
