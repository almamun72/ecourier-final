package bd.com.ecourier.ecourier.Model;

import java.util.ArrayList;

/**
 * Created by User on 28-Dec-17.
 */

public class AssignAgentPickup {

    private ArrayList<String> pick_up_ids;
    private String status;
    private String agent_id;
    private String orders;

    public AssignAgentPickup(ArrayList<String> pick_up_ids, String status, String agent_id, String orders) {

        this.pick_up_ids = pick_up_ids;
        this.status = status;
        this.agent_id = agent_id;
        this.orders = orders;

    }

    public ArrayList<String> getPick_up_ids() {
        return pick_up_ids;
    }

    public void setPick_up_ids(ArrayList<String> pick_up_ids) {
        this.pick_up_ids = pick_up_ids;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAgent_id() {
        return agent_id;
    }

    public void setAgent_id(String agent_id) {
        this.agent_id = agent_id;
    }

    public String getOrders() {
        return orders;
    }

    public void setOrders(String orders) {
        this.orders = orders;
    }
}
