package bd.com.ecourier.ecourier.View;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bd.com.ecourier.ecourier.API;
import bd.com.ecourier.ecourier.Config;
import bd.com.ecourier.ecourier.Model.Consignments;
import bd.com.ecourier.ecourier.Model.ManagerPickProductFromAgent;
import bd.com.ecourier.ecourier.Presenter.ConsignmentsListAdapter;
import bd.com.ecourier.ecourier.R;
import bd.com.ecourier.ecourier.clickListener.ClickListener;
import bd.com.ecourier.ecourier.clickListener.RecyclerTouchListener;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static android.graphics.Paint.ANTI_ALIAS_FLAG;

/**
 * Created by User on 10-Dec-17.
 */

public class AutomatedPickFragment extends Fragment {

    @BindView(R.id.swipe_down_to_refresh_list)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.recyclerView_list)
    RecyclerView recyclerView;
    @BindView(R.id.fab_scan)
    FloatingActionButton QRScan;
    @BindView(R.id.recyclerView_list_notice)
    TextView noListNotice;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    private ConsignmentsListAdapter consignmentsListAdapter;
    private Consignments consignment;
    private View rootView;
    private ArrayList<String> qrScanCodes = new ArrayList<>();

    private ArrayList<Consignments> consignmentsArrayListForServer = new ArrayList<>();
    private ArrayList<String> consignmentsECRListForServer = new ArrayList<>();
    private ArrayList<String> consignments = new ArrayList<>();
    private ArrayList<Consignments> consignmentsArrayList;

    private String accept = null, pickUpID = "", nameOfMerchant = "", noOfProduct = "", agentId = "";
    private IntentIntegrator intentIntegrator;

    public AutomatedPickFragment() {
    }

    public static Bitmap textAsBitmap(String text, float textSize, int textColor) {
        Paint paint = new Paint(ANTI_ALIAS_FLAG);
        paint.setTextSize(textSize);
        paint.setColor(textColor);
        paint.setTextAlign(Paint.Align.LEFT);
        float baseline = -paint.ascent(); // ascent() is negative
        int width = (int) (paint.measureText(text) + 0.0f); // round
        int height = (int) (baseline + paint.descent() + 0.0f);
        Bitmap image = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(image);
        canvas.drawText(text, 0, baseline, paint);
        return image;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {

            consignments = getArguments().getStringArrayList(API.KEY_JSON_CONSIGNMENTS);
            pickUpID = getArguments().getString(API.KEY_MERCHANT_PRODUCT_PICKUP_ID);
            nameOfMerchant = getArguments().getString(API.KEY_CUSTOMER_PRODUCT_MERCHANT_NAME);
            noOfProduct = getArguments().getString(API.KEY_CUSTOMER_PRODUCT_COLLECTED_AMOUNT);
            agentId = getArguments().getString(API.KEY_JSON_USER_ID_AGENT);

        }

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.recycle_view_list_layout, container, false);
        ButterKnife.bind(this, rootView);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                swipeRefreshLayout.setRefreshing(false);

            }
        });

        progressBar.setVisibility(View.GONE);
        consignmentsArrayList = new ArrayList<>();

        for (int i = 0; i < consignments.size(); i++) {

            consignmentsArrayList.add(new Consignments(consignments.get(i)));

        }

        showFeed(consignmentsArrayList);
        noListNotice.setVisibility(View.VISIBLE);
        noListNotice.setCompoundDrawablesWithIntrinsicBounds(getActivity().getResources().getDrawable(R.drawable.ic_check_box_outline_blank_black_24dp), null, null, null);
        noListNotice.setCompoundDrawablePadding(16);

        noListNotice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Boolean check;

                consignmentsArrayListForServer.clear();

                int checkCount = 0;
                for (int i = 0; i < consignmentsArrayList.size(); i++) {

                    if (consignmentsArrayList.get(i).isChecked())
                        checkCount++;

                }

                if (checkCount == consignmentsArrayList.size())
                    check = false;
                else
                    check = true;

                for (int i = 0; i < consignmentsArrayList.size(); i++) {

                    consignment = consignmentsArrayList.get(i);
                    selectMerchantItem(check);

                }

                consignmentsListAdapter.notifyDataSetChanged();

                if (consignmentsArrayList.get(0).isChecked()) {

                    noListNotice.setCompoundDrawablesWithIntrinsicBounds(getActivity().getResources().getDrawable(R.drawable.ic_check_box_white_24dp), null, null, null);

                } else {

                    noListNotice.setCompoundDrawablesWithIntrinsicBounds(getActivity().getResources().getDrawable(R.drawable.ic_check_box_outline_blank_black_24dp), null, null, null);

                }


            }
        });

        if (getContext() != null) {

            accept = getContext().getResources().getString(R.string.number_of_item);

        }

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, final int position) {

                consignment = consignmentsArrayList.get(position);

                selectMerchantItem();
                consignmentsListAdapter.notifyItemChanged(position);

            }

            @Override
            public void onLongClick(View view, int position) {

                /*consignment = consignmentsArrayList.get(position);

                selectMerchantItem();
                consignmentsListAdapter.notifyItemChanged(position);*/

            }
        }));

        intentIntegrator = IntentIntegrator.forSupportFragment(this);
        intentIntegrator.setCaptureLayout(R.layout.barcode_scanner);
        intentIntegrator.setResultDisplayDuration(0);

        displayAgentDetail();

        return rootView;
    }

    private void displayAgentDetail() {

        if (getActivity() != null) {

            try {

                String data = getResources().getString(R.string.total) + " (" + consignments.size() + ")";
                noListNotice.setText(data);

            } catch (Exception e) {

                Log.e("Error: ", " " + e);

            }

        }


    }

    private void refreshList() {

        consignmentsListAdapter.clearList();
        consignmentsArrayListForServer.clear();
        consignmentsECRListForServer.clear();
        QRScan.setImageResource(R.drawable.cycle);

    }

    private void selectMerchantItem() {

        consignment.setChecked(!consignment.isChecked());
        if (consignment.isChecked())
            consignmentsArrayListForServer.add(consignment);
        else
            consignmentsArrayListForServer.remove(consignment);

        if (consignmentsArrayListForServer.size() == 0) {

            QRScan.setImageResource(R.drawable.cycle);

        } else {

            QRScan.setImageBitmap(textAsBitmap("" + consignmentsArrayListForServer.size(), 40, Color.WHITE));

        }

        int checkCount = 0;
        for (int i = 0; i < consignmentsArrayList.size(); i++) {

            if (consignmentsArrayList.get(i).isChecked())
                checkCount++;

        }

        if (checkCount == consignmentsArrayList.size()) {

            noListNotice.setCompoundDrawablesWithIntrinsicBounds(getActivity().getResources().getDrawable(R.drawable.ic_check_box_white_24dp), null, null, null);

        } else {

            noListNotice.setCompoundDrawablesWithIntrinsicBounds(getActivity().getResources().getDrawable(R.drawable.ic_check_box_outline_blank_black_24dp), null, null, null);

        }

    }

    private void selectMerchantItem(Boolean check) {

        consignment.setChecked(check);
        if (consignment.isChecked())
            consignmentsArrayListForServer.add(consignment);
        else
            consignmentsArrayListForServer.remove(consignment);

        if (consignmentsArrayListForServer.size() == 0) {

            QRScan.setImageResource(R.drawable.cycle);

        } else {

            QRScan.setImageBitmap(textAsBitmap("" + consignmentsArrayListForServer.size(), 40, Color.WHITE));

        }

        int checkCount = 0;
        for (int i = 0; i < consignmentsArrayList.size(); i++) {

            if (consignmentsArrayList.get(i).isChecked())
                checkCount++;

        }

        if (checkCount == consignmentsArrayList.size()) {

            noListNotice.setCompoundDrawablesWithIntrinsicBounds(getActivity().getResources().getDrawable(R.drawable.ic_check_box_white_24dp), null, null, null);

        } else {

            noListNotice.setCompoundDrawablesWithIntrinsicBounds(getActivity().getResources().getDrawable(R.drawable.ic_check_box_outline_blank_black_24dp), null, null, null);

        }

    }

    private void showFeed(ArrayList<Consignments> consignmentsArrayList) {

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        consignmentsListAdapter = new ConsignmentsListAdapter(getActivity(), consignmentsArrayList, Config.TAG_PICK_AUTOMATED);
        recyclerView.setAdapter(consignmentsListAdapter);

    }

    @OnClick(R.id.fab_scan)
    public void onClick(View view) {

        if (consignmentsArrayListForServer.size() == 0) {
            intentIntegrator.addExtra("PROMPT_MESSAGE", "Total : " + qrScanCodes.size());
            intentIntegrator.initiateScan();
        } else if (consignmentsArrayListForServer.size() > 0) {

            consignmentsECRListForServer.clear();

            for (int i = 0; i < consignmentsArrayListForServer.size(); i++) {

                consignmentsECRListForServer.add(consignmentsArrayListForServer.get(i).getConsignment_no());

            }

            if (getContext() != null) {

                AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                builder1.setCancelable(false);

                LayoutInflater inflater = getActivity().getLayoutInflater();

                View alertView = inflater.inflate(R.layout.agent_pick_parcel_confirm_dialog, null);
                builder1.setView(alertView);

                final TextView merchantName = alertView.findViewById(R.id.alert_merchant_name);
                TextView requestedAmount = alertView.findViewById(R.id.alert_merchant_requested_amount);
                final EditText amountPicked = alertView.findViewById(R.id.alert_parcel_amount_picked);
                amountPicked.setVisibility(View.GONE);
                final EditText otp = alertView.findViewById(R.id.alert_parcel_otp_picked);

                /*if (((MainActivity) getActivity()).userGroup.equals(Config.USER_TYPE_MANAGER))*/
                otp.setVisibility(View.GONE);

                merchantName.setText(nameOfMerchant);
                requestedAmount.setText(String.format("%s %s", getContext().getResources().getString(R.string.pick_request), noOfProduct));

                /*builder1.setNeutralButton(
                        getContext().getResources().getString(R.string.resend_sms),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                            }
                        }).setNegativeButton(getContext().getResources().getString(R.string.back), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog

                    }
                });*/

                if (((MainActivity) getActivity()).userGroup.equals(Config.USER_TYPE_MANAGER)) {

                    builder1.setPositiveButton(
                            getContext().getResources().getString(R.string.confirm_parcel),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            }).setNegativeButton(getContext().getResources().getString(R.string.back), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User cancelled the dialog
                            dialog.cancel();
                        }
                    });

                } else {

                    builder1.setPositiveButton(
                            getContext().getResources().getString(R.string.get_sms),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            }).setNegativeButton(getContext().getResources().getString(R.string.back), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User cancelled the dialog
                            dialog.cancel();
                        }
                    });

                }

                final AlertDialog alert11 = builder1.create();
                alert11.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface arg0) {
                        alert11.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.RED);
                        alert11.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getContext().getResources().getColor(R.color.colorPrimary));
                        alert11.getButton(AlertDialog.BUTTON_NEUTRAL).setTextColor(getContext().getResources().getColor(R.color.colorBlack));

                        alert11.getButton(AlertDialog.BUTTON_NEUTRAL).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //resendOTP(pickUpID);
                            }
                        });

                        /*if (((MainActivity) getActivity()).userGroup.equals(Config.USER_TYPE_MANAGER))
                            alert11.getButton(AlertDialog.BUTTON_NEUTRAL).setVisibility(View.GONE);*/

                        alert11.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                Gson gson = new Gson();

                                /*if (*//*!otp.getText().toString().equals("") || *//*((MainActivity) getActivity()).userGroup.equals(Config.USER_TYPE_MANAGER)) {
                                 */
                                if (((MainActivity) getContext()).userGroup.equals(Config.USER_TYPE_MANAGER)) {

                                    ArrayList<String> temp = new ArrayList<>();
                                    temp.add(pickUpID);

                                    updateStatusManager(gson.toJson(new ManagerPickProductFromAgent(temp, API.STATUS_MANAGER_CONFIRM_AGENT_PICK, agentId, consignmentsECRListForServer)));

                                } else {

                                    resendOTP(pickUpID, String.valueOf(consignmentsArrayListForServer.size()));

                                    /*updateStatus(pickUpID, API.STATUS_AGENT_CONFIRM_PICK, otp.getText().toString(), String.valueOf(consignmentsECRListForServer.size()), gson.toJson(consignmentsECRListForServer));*/

                                }

                                alert11.cancel();

                                /*} else {
                                    Toast.makeText(getActivity(), "Enter Code", Toast.LENGTH_SHORT).show();
                                }*/

                            }
                        });

                    }
                });

                alert11.show();

            }

        }

    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {

        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (scanResult != null) {

            if (resultCode == RESULT_OK) {
                // contents contains whatever the code was
                String contents = scanResult.getContents();

                // Format contains the type of code i.e. UPC, EAN, QRCode etc...
                //String format = intent.getStringExtra("SCAN_RESULT_FORMAT");

                // Handle successful scan. In this example add contents to ArrayList
                if (contents != null) {
                    for (int i = 0; i < consignmentsArrayList.size(); i++) {

                        if (consignmentsArrayList.get(i).getConsignment_no().equals(contents)) {

                            if (consignmentsArrayListForServer.size() > 0) {

                                Boolean found = false;

                                for (int j = 0; j < consignmentsArrayListForServer.size(); j++) {

                                    if (consignmentsArrayListForServer.get(j).getConsignment_no().equals(contents)) {

                                        found = true;
                                        break;

                                    }

                                }

                                if (!found) {

                                    consignment = consignmentsArrayList.get(i);
                                    selectMerchantItem();
                                    consignmentsListAdapter.notifyItemChanged(i);
                                    qrScanCodes.add(contents);

                                }

                            } else {

                                consignment = consignmentsArrayList.get(i);
                                selectMerchantItem();
                                consignmentsListAdapter.notifyItemChanged(i);
                                qrScanCodes.add(contents);

                            }

                            break;

                        }

                    }

                }

                intentIntegrator.addExtra("PROMPT_MESSAGE", "Total : " + qrScanCodes.size());
                intentIntegrator.initiateScan();

            } else if (resultCode == RESULT_CANCELED) {

                qrScanCodes.clear();

            }


        } else {
            try {
                Toast toast = Toast.makeText(getContext(),
                        "No scan data received!", Toast.LENGTH_SHORT);
                toast.show();
            } catch (Exception e) {
                Log.e("Error: ", " " + e);
            }

        }

    }

    private void resendOTP(final String merchantProductID, final String orders) {

        final ProgressDialog dialog = ProgressDialog.show(getActivity(), "",
                getActivity().getResources().getString(R.string.loading), true);

        dialog.show();

        RequestQueue queue = null;
        // Instantiate the RequestQueue.
        if (getContext() != null)
            queue = Volley.newRequestQueue(getContext());
        String url = API.baseUrl + API.resendOTP;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog.cancel();
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String code = jsonObject.getString(API.KEY_CODE);

                            if (code.equals(API.VALUE_CODE_SUCCESS)) {

                                Toast.makeText(getContext(), "Verification code send.", Toast.LENGTH_SHORT).show();

                                if (getContext() != null) {

                                    AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                                    builder1.setCancelable(false);

                                    LayoutInflater inflater = getActivity().getLayoutInflater();

                                    View alertView = inflater.inflate(R.layout.agent_pick_parcel_confirm_dialog, null);
                                    builder1.setView(alertView);

                                    final TextView merchantName = alertView.findViewById(R.id.alert_merchant_name);
                                    TextView requestedAmount = alertView.findViewById(R.id.alert_merchant_requested_amount);
                                    final EditText amountPicked = alertView.findViewById(R.id.alert_parcel_amount_picked);
                                    amountPicked.setVisibility(View.GONE);
                                    final EditText otp = alertView.findViewById(R.id.alert_parcel_otp_picked);

                                    merchantName.setText(nameOfMerchant);
                                    requestedAmount.setText(String.format("%s %s", getContext().getResources().getString(R.string.pick_request), noOfProduct));

                                    builder1.setNeutralButton(
                                            getContext().getResources().getString(R.string.resend_sms),
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {

                                                }
                                            }).setNegativeButton(getContext().getResources().getString(R.string.back), new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            // User cancelled the dialog

                                        }
                                    });

                                    builder1.setPositiveButton(
                                            getContext().getResources().getString(R.string.confirm_parcel),
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {

                                                }
                                            }).setNegativeButton(getContext().getResources().getString(R.string.back), new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            // User cancelled the dialog
                                            dialog.cancel();
                                        }
                                    });

                                    final AlertDialog alert11 = builder1.create();
                                    alert11.setOnShowListener(new DialogInterface.OnShowListener() {
                                        @Override
                                        public void onShow(DialogInterface arg0) {
                                            alert11.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.RED);
                                            alert11.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getContext().getResources().getColor(R.color.colorPrimary));
                                            alert11.getButton(AlertDialog.BUTTON_NEUTRAL).setTextColor(getContext().getResources().getColor(R.color.colorBlack));

                                            alert11.getButton(AlertDialog.BUTTON_NEUTRAL).setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    alert11.cancel();
                                                    resendOTP(pickUpID, orders);
                                                }
                                            });

                                            alert11.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {

                                                    Gson gson = new Gson();

                                                    if (!otp.getText().toString().equals("")) {

                                                        updateStatus(pickUpID, API.STATUS_AGENT_CONFIRM_PICK, otp.getText().toString(), String.valueOf(consignmentsECRListForServer.size()), gson.toJson(consignmentsECRListForServer), alert11);

                                                    } else {
                                                        Toast.makeText(getActivity(), "Enter Code", Toast.LENGTH_SHORT).show();
                                                    }

                                                }
                                            });

                                        }
                                    });

                                    alert11.show();

                                }

                                /*JSONObject data = jsonObject.getJSONObject(API.KEY_JSON_DATA);

                                Toast.makeText(getContext(), " " + data.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_SHORT).show();*/

                            } else {

                                JSONObject error = jsonObject.getJSONObject(API.KEY_JSON_ERROR);

                                if (code.equals(API.VALUE_CODE_AUTHENTICATION_ERROR)) {

                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                } else if (code.equals(API.VALUE_CODE_PARAMETER_ERROR)) {
                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.cancel();
                try {

                    if (getContext() != null) {

                        if (error.getMessage().equals("com.android.volley.TimeoutError"))
                            Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_timeout), Toast.LENGTH_SHORT).show();

                        Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_unreachable), Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    Log.v("Automatted Error : ", "" + e);

                }

                Log.v("Volly error : ", "" + error);
            }

        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                if (getContext() != null) {

                    params.put(API.KEY_JSON_USER_AUTHENTICATION, ((MainActivity) getContext()).authenticationKey);
                    params.put(API.KEY_JSON_USER_GROUP, ((MainActivity) getContext()).userGroup);
                    params.put(API.KEY_USER_ID, ((MainActivity) getContext()).userID);
                    params.put(API.KEY_CONTENT_TYPE, API.VALUE_CONTENT_VALUE);

                }

                return params;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put(API.KEY_OTP_TYPE, API.VALUE_OTP_TYPE_PICK);
                params.put(API.KEY_ORDER_ID, merchantProductID);
                params.put(API.KEY_ORDER, orders);
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                Config.volley_time_out,
                Config.max_retries,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Add the request to the RequestQueue.
        if (queue != null)
            queue.add(stringRequest);

    }

    private void updateStatus(final String id, final String status, final String smsCode, final String orders, final String consignment, final AlertDialog alertDialog) {

        final ProgressDialog dialog = ProgressDialog.show(getActivity(), "",
                getActivity().getResources().getString(R.string.loading), true);

        dialog.show();

        RequestQueue queue = null;
        String url = null;
        // Instantiate the RequestQueue.
        if (getContext() != null) {

            queue = Volley.newRequestQueue(getContext());
            url = API.baseUrl + API.agentPickUpUpdate;

        }

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        swipeRefreshLayout.setRefreshing(false);
                        dialog.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String code = jsonObject.getString(API.KEY_CODE);

                            if (code.equals(API.VALUE_CODE_SUCCESS)) {
                                alertDialog.cancel();
                                if (getActivity() != null)
                                    ((MainActivity) getActivity()).getSupportFragmentManager().popBackStack();


                                /*JSONObject data = jsonObject.getJSONObject(API.KEY_JSON_DATA);

                                String updated = data.getString(API.KEY_JSON_MESSAGE);

                                if (updated.length() > 0) {
                                    Toast.makeText(getContext(), " " + updated, Toast.LENGTH_SHORT).show();

                                    ((MainActivity) getContext()).refreshPickTabTitle();

                                }*/

                            } else {

                                JSONObject error = jsonObject.getJSONObject(API.KEY_JSON_ERROR);

                                if (code.equals(API.VALUE_CODE_AUTHENTICATION_ERROR)) {

                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();

                                    Log.v("Error: ", " " + error.getString(API.KEY_JSON_MESSAGE));

                                } else if (code.equals(API.VALUE_CODE_PARAMETER_ERROR)) {
                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();

                                    Log.v("Error: ", " " + error.getString(API.KEY_JSON_MESSAGE));


                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                alertDialog.cancel();
                try {

                    swipeRefreshLayout.setRefreshing(false);
                    dialog.dismiss();

                    if (getContext() != null) {

                        if (error.getMessage().equals("com.android.volley.TimeoutError"))
                            Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_timeout), Toast.LENGTH_SHORT).show();

                        Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_unreachable), Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    Log.v("Automatted Error : ", "" + e);

                }

                Log.v("Volly error : ", "" + error);
            }

        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                if (getContext() != null) {

                    params.put(API.KEY_JSON_USER_AUTHENTICATION, ((MainActivity) getContext()).authenticationKey);
                    params.put(API.KEY_JSON_USER_GROUP, ((MainActivity) getContext()).userGroup);
                    params.put(API.KEY_USER_ID, ((MainActivity) getContext()).userID);
                    params.put(API.KEY_CONTENT_TYPE, API.VALUE_CONTENT_VALUE);

                }

                return params;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put(API.KEY_MERCHANT_PRODUCT_PICKUP_ID, id);
                params.put(API.KEY_JSON_STATUS, status);
                params.put(API.KEY_JSON_SMS_CODE, smsCode);
                params.put(API.KEY_JSON_ORDER, orders);
                params.put(API.KEY_JSON_CONSIGNMENTS, consignment);
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                Config.volley_time_out,
                Config.max_retries,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Add the request to the RequestQueue.
        if (queue != null)
            queue.add(stringRequest);

    }

    private void updateStatusManager(final String data) {

        final ProgressDialog dialog = ProgressDialog.show(getActivity(), "",
                getActivity().getResources().getString(R.string.loading), true);

        dialog.show();

        RequestQueue queue = null;
        String url = null;
        // Instantiate the RequestQueue.
        if (getContext() != null) {

            queue = Volley.newRequestQueue(getContext());
            url = API.baseUrl + API.managerPickUpUpdate;

        }

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        swipeRefreshLayout.setRefreshing(false);
                        dialog.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String code = jsonObject.getString(API.KEY_CODE);

                            if (code.equals(API.VALUE_CODE_SUCCESS)) {

                                if (getActivity() != null)
                                    ((MainActivity) getActivity()).getSupportFragmentManager().popBackStack();


                                /*JSONObject data = jsonObject.getJSONObject(API.KEY_JSON_DATA);

                                String updated = data.getString(API.KEY_JSON_MESSAGE);

                                if (updated.length() > 0) {
                                    Toast.makeText(getContext(), " " + updated, Toast.LENGTH_SHORT).show();

                                    ((MainActivity) getContext()).refreshPickTabTitle();

                                }*/

                            } else {

                                JSONObject error = jsonObject.getJSONObject(API.KEY_JSON_ERROR);

                                if (code.equals(API.VALUE_CODE_AUTHENTICATION_ERROR)) {

                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();

                                    Log.v("Error: ", " " + error.getString(API.KEY_JSON_MESSAGE));

                                } else if (code.equals(API.VALUE_CODE_PARAMETER_ERROR)) {
                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();

                                    Log.v("Error: ", " " + error.getString(API.KEY_JSON_MESSAGE));


                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                swipeRefreshLayout.setRefreshing(false);
                dialog.dismiss();

                try {

                    if (getContext() != null) {

                        if (error.getMessage().equals("com.android.volley.TimeoutError"))
                            Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_timeout), Toast.LENGTH_SHORT).show();

                        Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_unreachable), Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    Log.v("Automatted Error : ", "" + e);

                }

                Log.v("Volly error : ", "" + error);
            }

        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                if (getContext() != null) {

                    params.put(API.KEY_JSON_USER_AUTHENTICATION, ((MainActivity) getContext()).authenticationKey);
                    params.put(API.KEY_JSON_USER_GROUP, ((MainActivity) getContext()).userGroup);
                    params.put(API.KEY_USER_ID, ((MainActivity) getContext()).userID);
                    params.put(API.KEY_CONTENT_TYPE, API.VALUE_CONTENT_VALUE_JSON);

                }

                return params;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                String your_string_json = data; // put your json
                return your_string_json.getBytes();
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                Config.volley_time_out,
                Config.max_retries,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Add the request to the RequestQueue.
        if (queue != null)
            queue.add(stringRequest);

    }

}
