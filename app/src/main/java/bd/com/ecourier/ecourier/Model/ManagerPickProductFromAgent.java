package bd.com.ecourier.ecourier.Model;

import java.util.ArrayList;

/**
 * Created by User on 28-Dec-17.
 */

public class ManagerPickProductFromAgent {

    private ArrayList<String> pick_up_ids;
    private String status;
    private String agent_id;
    private ArrayList<String> consignments;

    public ManagerPickProductFromAgent(ArrayList<String> pick_up_ids, String status, String agent_id, ArrayList<String> consignments) {

        this.pick_up_ids = pick_up_ids;
        this.status = status;
        this.agent_id = agent_id;
        this.consignments = consignments;

    }

    public ArrayList<String> getPick_up_ids() {
        return pick_up_ids;
    }

    public void setPick_up_ids(ArrayList<String> pick_up_ids) {
        this.pick_up_ids = pick_up_ids;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAgent_id() {
        return agent_id;
    }

    public void setAgent_id(String agent_id) {
        this.agent_id = agent_id;
    }

    public ArrayList<String> getOrders() {
        return consignments;
    }

    public void setOrders(ArrayList<String> consignments) {
        this.consignments = consignments;
    }
}
