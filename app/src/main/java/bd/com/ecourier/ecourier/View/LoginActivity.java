package bd.com.ecourier.ecourier.View;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import bd.com.ecourier.ecourier.API;
import bd.com.ecourier.ecourier.Config;
import bd.com.ecourier.ecourier.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by User on 10-Dec-17.
 */

public class LoginActivity extends AppCompatActivity {

    /*
     * Bind values of layout IDs that are associated with activity_login.
     * using butterKnife.
     * */
    @BindView(R.id.id_login_EditText)
    EditText loginID;
    @BindView(R.id.password_login_EditText)
    EditText loginPassword;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.login_btn)
    Button loginBtn;

    private Intent mainActivityIntent;
    private SharedPreferences sp;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        progressBar.setVisibility(View.GONE);
        mainActivityIntent = new Intent(this, MainActivity.class);
        sp = getSharedPreferences(Config.SP_ECourier_APP, MODE_PRIVATE);

    }

    @OnClick({R.id.login_btn})
    public void setViewOnClickEvent(View view) {

        switch (view.getId()) {

            case R.id.login_btn: {

                progressBar.setVisibility(View.VISIBLE);
                loginBtn.setVisibility(View.GONE);

                if (!TextUtils.isEmpty(loginID.getText().toString()) && !TextUtils.isEmpty(loginPassword.getText().toString())) {
                    loginUser(loginID.getText().toString(), loginPassword.getText().toString());
                } else {
                    Toast.makeText(this, "ID or Password Cannot be empty", Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(View.GONE);
                    loginBtn.setVisibility(View.VISIBLE);

                }

                /*loginUser("BADDA101", "123456");*/

                break;

            }

            /*case R.id.login_btn_manager: {

                loginUser("Badda", "123456");

                break;
            }*/

        }

    }

    private void loginUser(final String id, final String password) {

        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = API.baseUrl + API.login;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                    @Override
                    public void onResponse(String response) {

                        //Log.v("Login response", " " + response);

                        try {

                            JSONObject jsonObject = new JSONObject(response);
                            String code = jsonObject.getString(API.KEY_CODE);

                            if (code.equals(API.VALUE_CODE_SUCCESS)) {

                                userInfo(jsonObject);

                            } else {

                                Toast.makeText(getBaseContext(), "Invalid Email or password", Toast.LENGTH_LONG).show();
                                progressBar.setVisibility(View.GONE);
                                loginBtn.setVisibility(View.VISIBLE);

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressBar.setVisibility(View.GONE);
                            loginBtn.setVisibility(View.VISIBLE);
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                loginBtn.setVisibility(View.VISIBLE);

                try {

                    if (error.getMessage().equals("com.android.volley.TimeoutError"))
                        Toast.makeText(LoginActivity.this, getResources().getString(R.string.server_timeout), Toast.LENGTH_SHORT).show();

                    else
                        Toast.makeText(LoginActivity.this, getResources().getString(R.string.server_unreachable), Toast.LENGTH_SHORT).show();

                    Log.v("Volly error : ", "" + error);

                } catch (Exception e) {

                    Log.v("Login E : ", " " + e);

                }


            }

        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(API.KEY_CONTENT_TYPE, API.VALUE_CONTENT_VALUE);

                return params;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put(API.KEY_USER_ID, id);
                params.put(API.KEY_USER_PASSWORD, password);
                return params;
            }
        };


        // Add the request to the RequestQueue.
        queue.add(stringRequest);

    }

    private void userInfo(JSONObject jsonObject) throws JSONException {

        /*Toast toast = Toast.makeText(LoginActivity.this, "Login Successful, Saving Data", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.show();*/

        /*Toast.makeText(LoginActivity.this, "Login Successful, Saving Data", Toast.LENGTH_LONG).setGravity(Gravity.CENTER_VERTICAL, 0, 0).show();*/

        Gson gson = new Gson();
        String userName, userID, authenticationKey, userGroup, userPhone, defaultComments, parcelStatus, pickupStatus;

        JSONObject data = jsonObject.getJSONObject(API.KEY_JSON_DATA);
        JSONObject user = data.getJSONObject(API.KEY_JSON_USER);

        JSONObject setting = data.getJSONObject(API.KEY_JSON_USER_SETTINGS);
        JSONArray defaultCommentsArray = setting.getJSONArray(API.KEY_JSON_USER_DEFAULT_COMMENTS);
        JSONArray parcelStatusArray = setting.getJSONArray(API.KEY_JSON_USER_PARCEL_STATUS);
        JSONArray pickupStatusArray = setting.getJSONArray(API.KEY_JSON_USER_PICKUP_STATUS);

        defaultComments = gson.toJson(defaultCommentsArray);
        parcelStatus = gson.toJson(parcelStatusArray);
        pickupStatus = gson.toJson(pickupStatusArray);

        authenticationKey = data.getString(API.KEY_JSON_USER_AUTHENTICATION);
        userGroup = user.getString(API.KEY_JSON_USER_GROUP);
        userPhone = user.getString(API.KEY_JSON_USER_PHONE);

        if (userGroup.equals(API.VALUE_USER_GROUP_MANAGER)) {

            userID = user.getString(API.KEY_JSON_USER_ID_MANAGER);
            userName = user.getString(API.KEY_JSON_USER_NAME_MANAGER);
            saveUserInfo(userID, userName, userGroup, userPhone, authenticationKey, defaultComments, parcelStatus, pickupStatus);
        } else if (userGroup.equals(API.VALUE_USER_GROUP_AGENT)) {

            userID = user.getString(API.KEY_JSON_USER_ID_AGENT);
            userName = user.getString(API.KEY_JSON_USER_NAME_AGENT);
            saveUserInfo(userID, userName, userGroup, userPhone, authenticationKey, defaultComments, parcelStatus, pickupStatus);
        }

    }

    private void saveUserInfo(String userID, String name, String userGroup, String userPhone, String authenticationKey, String defaultComments, String parcelStatus, String pickupStatus) {

        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean(Config.SP_LOGGED_IN, true);
        editor.putString(API.KEY_USER_ID, userID);
        editor.putString(API.KEY_USER_NAME, name);
        editor.putString(API.KEY_JSON_USER_GROUP, userGroup);
        editor.putString(API.KEY_JSON_USER_PHONE, userPhone);
        editor.putString(API.KEY_JSON_USER_AUTHENTICATION, authenticationKey);
        editor.putString(API.KEY_JSON_USER_DEFAULT_COMMENTS, defaultComments);
        editor.putString(API.KEY_JSON_USER_PARCEL_STATUS, parcelStatus);
        editor.putString(API.KEY_JSON_USER_PICKUP_STATUS, pickupStatus);

        if (editor.commit()) {
            Toast.makeText(LoginActivity.this, "Login Successful, Registering user", Toast.LENGTH_SHORT).show();
            devicePushRegistration(userID, userGroup, authenticationKey);
        }

    }

    private void devicePushRegistration(final String id, final String userGroup, final String authValue) {

        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = API.baseUrl + API.devicePushRegistration;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                    @Override
                    public void onResponse(String response) {

                        try {

                            JSONObject jsonObject = new JSONObject(response);
                            String code = jsonObject.getString(API.KEY_CODE);

                            if (code.equals(API.VALUE_CODE_SUCCESS)) {

                                startActivity(mainActivityIntent);
                                finish();
                                /*loginBtn.setVisibility(View.VISIBLE);
                                progressBar.setVisibility(View.GONE);*/

                            } else {

                                /*loginProgressBar.setVisibility(View.GONE);
                                loginBtn.setVisibility(View.VISIBLE);*//**//**//**//* */
                                Toast.makeText(LoginActivity.this, "Device push registration failed", Toast.LENGTH_SHORT).show();
                                progressBar.setVisibility(View.GONE);
                                loginBtn.setVisibility(View.VISIBLE);

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressBar.setVisibility(View.GONE);
                            loginBtn.setVisibility(View.VISIBLE);
                        }

                        /* */

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                progressBar.setVisibility(View.GONE);
                loginBtn.setVisibility(View.VISIBLE);

                try {


                    if (error.getMessage().equals("com.android.volley.TimeoutError"))
                        Toast.makeText(LoginActivity.this, getResources().getString(R.string.server_timeout), Toast.LENGTH_SHORT).show();

                    Toast.makeText(LoginActivity.this, getResources().getString(R.string.server_unreachable), Toast.LENGTH_SHORT).show();


                    Log.v("Volly error : ", "" + error);
                } catch (Exception e) {

                    Log.v("Distribute Cancel E : ", " " + e);

                }


            }

        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(API.KEY_USER_ID, id);
                params.put(API.KEY_JSON_USER_GROUP, userGroup);
                params.put(API.KEY_JSON_USER_AUTHENTICATION, authValue);
                params.put(API.KEY_CONTENT_TYPE, API.VALUE_CONTENT_VALUE);
                return params;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put(API.KEY_JSON_PUSH_TOKEN, FirebaseInstanceId.getInstance().getToken());
                return params;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);

    }

}
