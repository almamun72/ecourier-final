package bd.com.ecourier.ecourier.View;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bd.com.ecourier.ecourier.API;
import bd.com.ecourier.ecourier.Config;
import bd.com.ecourier.ecourier.Model.CustomerProduct;
import bd.com.ecourier.ecourier.Model.ManagerAssignParcel;
import bd.com.ecourier.ecourier.Model.ManagerParcelUpdate;
import bd.com.ecourier.ecourier.Presenter.CustomerProductListAdapter;
import bd.com.ecourier.ecourier.R;
import bd.com.ecourier.ecourier.clickListener.ClickListener;
import bd.com.ecourier.ecourier.clickListener.RecyclerTouchListener;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static android.graphics.Paint.ANTI_ALIAS_FLAG;

/**
 * Created by User on 10-Dec-17.
 */

public class DistributeFulfillmentFragment extends Fragment {

    @BindView(R.id.swipe_down_to_refresh_list)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.recyclerView_list)
    RecyclerView recyclerView;
    @BindView(R.id.fab_scan)
    FloatingActionButton QRScan;
    @BindView(R.id.recyclerView_list_notice)
    TextView noListNotice;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    private CustomerProductListAdapter customerProductListAdapter;
    private View rootView;
    private ArrayList<String> qrScanCodes = new ArrayList<>();

    private CustomerProduct customerProduct;
    private ArrayList<CustomerProduct> customerProductArrayListForServer = new ArrayList<>();
    private ArrayList<String> customerProductECRListForServer = new ArrayList<>();
    private ArrayList<CustomerProduct> customerProductArrayList;

    private Gson gson = new Gson();
    private IntentIntegrator intentIntegrator;

    public DistributeFulfillmentFragment() {
    }

    public static Bitmap textAsBitmap(String text, float textSize, int textColor) {
        Paint paint = new Paint(ANTI_ALIAS_FLAG);
        paint.setTextSize(textSize);
        paint.setColor(textColor);
        paint.setTextAlign(Paint.Align.LEFT);
        float baseline = -paint.ascent(); // ascent() is negative
        int width = (int) (paint.measureText(text) + 0.0f); // round
        int height = (int) (baseline + paint.descent() + 0.0f);
        Bitmap image = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(image);
        canvas.drawText(text, 0, baseline, paint);
        return image;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.recycle_view_list_layout, container, false);
        ButterKnife.bind(this, rootView);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                refreshList();

            }
        });

        customerProductArrayList = new ArrayList<>();

        showFeed(customerProductArrayList);
        recyclerView.setVisibility(View.GONE);
        noListNotice.setVisibility(View.VISIBLE);

        if (getContext() != null)
            if (((MainActivity) getContext()).connectedTOInternet)
                fetchListData();

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, final int position) {

                customerProduct = customerProductArrayList.get(position);
                ImageView checkBox = view.findViewById(R.id.product_select_checkbox);

                checkBox.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {

                        selectMerchantItem();
                        customerProductListAdapter.notifyItemChanged(position);

                    }
                });

            }

            @Override
            public void onLongClick(View view, int position) {

                /*customerProduct = customerProductArrayList.get(position);

                selectMerchantItem();
                customerProductListAdapter.notifyItemChanged(position);*/

            }
        }));

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (((LinearLayoutManager) recyclerView.getLayoutManager()).findLastCompletelyVisibleItemPosition() > (customerProductArrayList.size() - API.FETCH_DATA_AT_SCROLL) && newState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE) {
                    fetchListData();

                }

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });

        intentIntegrator = IntentIntegrator.forSupportFragment(this);
        intentIntegrator.setCaptureLayout(R.layout.barcode_scanner);
        intentIntegrator.setResultDisplayDuration(0);

        return rootView;
    }

    private void refreshList() {

        customerProductListAdapter.clearList();
        fetchListData();
        customerProductArrayListForServer.clear();
        customerProductECRListForServer.clear();
        QRScan.setImageResource(R.drawable.cycle);

        if ((getContext() != null))
            ((MainActivity) getContext()).refreshDistributeTabTitle();

    }

    private void selectMerchantItem() {

        customerProduct.setChecked(!customerProduct.isChecked());
        if (customerProduct.isChecked())
            customerProductArrayListForServer.add(customerProduct);
        else
            customerProductArrayListForServer.remove(customerProduct);

        if (customerProductArrayListForServer.size() == 0) {

            QRScan.setImageResource(R.drawable.cycle);

        } else {

            QRScan.setImageBitmap(textAsBitmap("" + customerProductArrayListForServer.size(), 40, Color.WHITE));

        }
    }

    @OnClick(R.id.fab_scan)
    public void onClick(View view) {

        if (customerProductArrayListForServer.size() == 0) {
            intentIntegrator.addExtra("PROMPT_MESSAGE", "Total : " + qrScanCodes.size());
            intentIntegrator.initiateScan();
        } else if (customerProductArrayListForServer.size() > 0) {

            customerProductECRListForServer.clear();

            for (int i = 0; i < customerProductArrayListForServer.size(); i++) {

                customerProductECRListForServer.add(customerProductArrayListForServer.get(i).getECRNumber());

            }

            if (getContext() != null) {

                final AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                builder1.setCancelable(false);

                LayoutInflater inflater = this.getLayoutInflater();
                View alertView = inflater.inflate(R.layout.select_spinner_distribute_manager_delivery, null);
                builder1.setView(alertView);

                final EditText comment = alertView.findViewById(R.id.comment);
                comment.setVisibility(View.GONE);

                final TextView title = (TextView) alertView.findViewById(R.id.alert_title);
                title.setText(getResources().getText(R.string.select_agent));

                final LinearLayout linearLayout = (LinearLayout) alertView.findViewById(R.id.delivery_extra_option);
                linearLayout.setVisibility(View.GONE);

                Button hold = alertView.findViewById(R.id.hold);
                hold.setVisibility(View.GONE);
                Button cancel = alertView.findViewById(R.id.cancel);
                final Button wrongRouting = alertView.findViewById(R.id.wrong_routing);

                wrongRouting.setVisibility(View.GONE);

                final Spinner agentSpinner = alertView.findViewById(R.id.list_spinner);
                final ArrayAdapter<String> agentListAdapter, commentListAdapter;
                agentListAdapter = new ArrayAdapter<String>(getContext(), R.layout.spinner_item, R.id.spinner_item_text, ((MainActivity) getContext()).agentNameList);
                commentListAdapter = new ArrayAdapter<String>(getContext(), R.layout.spinner_item, R.id.spinner_item_text, ((MainActivity) getContext()).defaultComments);

                agentSpinner.setAdapter(agentListAdapter);

                agentSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        if (position == ((MainActivity) getContext()).defaultComments.size() - 1) {

                            if (linearLayout.getVisibility() == View.VISIBLE)
                                comment.setVisibility(View.VISIBLE);
                            else
                                comment.setVisibility(View.GONE);

                        } else {
                            comment.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                builder1.setPositiveButton(
                        getContext().getResources().getString(R.string.confirm_parcel),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                            }
                        }).setNegativeButton(getContext().getResources().getString(R.string.back), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                        customerProductECRListForServer.clear();
                        dialog.cancel();
                    }
                }).setNeutralButton(getContext().getResources().getString(R.string.more), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });

                final AlertDialog alert11 = builder1.create();

                hold.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (agentSpinner.getSelectedItemPosition() != 0 && agentSpinner.getSelectedItemPosition() < ((MainActivity) getContext()).defaultComments.size() - 1) {

                            /*ManagerParcelUpdate managerParcelUpdate = new ManagerParcelUpdate(customerProductECRListForServer, API.STATUS_MANAGER_DISTRIBUTE_HOLD_ITEM_FULFILLMENT, ((MainActivity) getContext()).defaultComments.get(agentSpinner.getSelectedItemPosition()));
                            Gson gson = new Gson();
                            updateStatus(gson.toJson(managerParcelUpdate));*/
                            alert11.dismiss();

                        } else if (agentSpinner.getSelectedItemPosition() == ((MainActivity) getContext()).defaultComments.size() - 1) {

                            /*ManagerParcelUpdate managerParcelUpdate = new ManagerParcelUpdate(customerProductECRListForServer, API.STATUS_MANAGER_DISTRIBUTE_HOLD_ITEM_FULFILLMENT, comment.getText().toString());
                            Gson gson = new Gson();
                            updateStatus(gson.toJson(managerParcelUpdate));*/
                            alert11.dismiss();

                        } else {

                            Toast.makeText(getContext(), "Select a reason", Toast.LENGTH_SHORT).show();

                        }


                    }
                });

                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (agentSpinner.getSelectedItemPosition() != 0 && agentSpinner.getSelectedItemPosition() < ((MainActivity) getContext()).defaultComments.size() - 1) {

                            ManagerParcelUpdate managerParcelUpdate = new ManagerParcelUpdate(customerProductECRListForServer, API.STATUS_MANAGER_DISTRIBUTE_CANCEL_ITEM_FULFILLMENT, ((MainActivity) getContext()).defaultComments.get(agentSpinner.getSelectedItemPosition()));
                            Gson gson = new Gson();
                            updateStatus(gson.toJson(managerParcelUpdate));
                            alert11.dismiss();
                        } else if (agentSpinner.getSelectedItemPosition() == ((MainActivity) getContext()).defaultComments.size() - 1 && !comment.getText().toString().equals("")) {

                            ManagerParcelUpdate managerParcelUpdate = new ManagerParcelUpdate(customerProductECRListForServer, API.STATUS_MANAGER_DISTRIBUTE_CANCEL_ITEM_FULFILLMENT, comment.getText().toString());
                            Gson gson = new Gson();
                            updateStatus(gson.toJson(managerParcelUpdate));
                            alert11.dismiss();
                        } else {

                            Toast.makeText(getContext(), "Select a reason", Toast.LENGTH_SHORT).show();

                        }

                    }
                });

                wrongRouting.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (agentSpinner.getSelectedItemPosition() != 0 && agentSpinner.getSelectedItemPosition() < ((MainActivity) getContext()).defaultComments.size() - 1) {

                            ManagerParcelUpdate managerParcelUpdate = new ManagerParcelUpdate(customerProductECRListForServer, API.STATUS_MANAGER_DISTRIBUTE_WRONG_ROUTE_ITEM_FULFILLMENT, ((MainActivity) getContext()).defaultComments.get(agentSpinner.getSelectedItemPosition()));
                            Gson gson = new Gson();
                            updateStatus(gson.toJson(managerParcelUpdate));
                            alert11.dismiss();

                        } else if (agentSpinner.getSelectedItemPosition() == ((MainActivity) getContext()).defaultComments.size() - 1 && !comment.getText().toString().equals("")) {

                            ManagerParcelUpdate managerParcelUpdate = new ManagerParcelUpdate(customerProductECRListForServer, API.STATUS_MANAGER_DISTRIBUTE_WRONG_ROUTE_ITEM_FULFILLMENT, comment.getText().toString());
                            Gson gson = new Gson();
                            updateStatus(gson.toJson(managerParcelUpdate));
                            alert11.dismiss();
                        } else {

                            Toast.makeText(getContext(), "Select a reason", Toast.LENGTH_SHORT).show();

                        }

                    }
                });

                alert11.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface arg0) {
                        alert11.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.RED);
                        alert11.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getContext().getResources().getColor(R.color.colorPrimary));

                        alert11.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                if (agentSpinner.getSelectedItemPosition() != 0) {

                                    ManagerAssignParcel managerAssignParcel = new ManagerAssignParcel(customerProductECRListForServer, API.STATUS_MANAGER_DISTRIBUTE_DELIVERY, ((MainActivity) getContext()).agentIDList.get(agentSpinner.getSelectedItemPosition()));

                                    managerAssign(gson.toJson(managerAssignParcel));

                                    alert11.dismiss();

                                } else {
                                    Toast.makeText(getContext(), "Please Select an agent", Toast.LENGTH_SHORT).show();
                                }


                            }
                        });
                        alert11.getButton(AlertDialog.BUTTON_NEUTRAL).setTextColor(Color.BLACK);


                        alert11.getButton(DialogInterface.BUTTON_NEUTRAL).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                if (linearLayout.getVisibility() == View.GONE) {

                                    linearLayout.setVisibility(View.VISIBLE);
                                    wrongRouting.setVisibility(View.VISIBLE);
                                    agentSpinner.setAdapter(commentListAdapter);
                                    title.setText(R.string.select_comment);
                                    alert11.getButton(DialogInterface.BUTTON_NEUTRAL).setText("LESS");
                                    alert11.getButton(AlertDialog.BUTTON_POSITIVE).setVisibility(View.GONE);
                                } else {

                                    linearLayout.setVisibility(View.GONE);
                                    wrongRouting.setVisibility(View.GONE);
                                    title.setText(R.string.select_agent);
                                    agentSpinner.setAdapter(agentListAdapter);
                                    alert11.getButton(DialogInterface.BUTTON_NEUTRAL).setText("MORE");
                                    alert11.getButton(AlertDialog.BUTTON_POSITIVE).setVisibility(View.VISIBLE);

                                }

                            }
                        });

                    }
                });

                alert11.show();

            }

        }

    }

    private void managerAssign(final String data) {

        final ProgressDialog dialog = ProgressDialog.show(getActivity(), "",
                getActivity().getResources().getString(R.string.loading), true);

        dialog.show();

        RequestQueue queue = null;
        // Instantiate the RequestQueue.
        if (getContext() != null)
            queue = Volley.newRequestQueue(getContext());
        String url = API.baseUrl + API.managerParcelUpdate;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String code = jsonObject.getString(API.KEY_CODE);

                            if (code.equals(API.VALUE_CODE_SUCCESS)) {

                                JSONObject data = jsonObject.getJSONObject(API.KEY_JSON_DATA);

                                JSONArray updated = data.getJSONArray(API.KEY_JSON_UPDATED);
                                JSONArray failed = data.getJSONArray(API.KEY_JSON_FAILED);

                                if (updated.length() > 0) {
                                    Toast.makeText(getContext(), "SUCCESS", Toast.LENGTH_SHORT).show();
                                    refreshList();

                                    if (getContext() != null)
                                        ((MainActivity) getContext()).refreshDistributeTabTitle();

                                } else if (failed.length() > 0) {
                                    Toast.makeText(getContext(), "FAILED", Toast.LENGTH_SHORT).show();
                                    swipeRefreshLayout.setRefreshing(false);

                                }

                            } else {
                                swipeRefreshLayout.setRefreshing(false);

                                JSONObject error = jsonObject.getJSONObject(API.KEY_JSON_ERROR);

                                if (code.equals(API.VALUE_CODE_AUTHENTICATION_ERROR)) {

                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                } else if (code.equals(API.VALUE_CODE_PARAMETER_ERROR)) {
                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                swipeRefreshLayout.setRefreshing(false);
                dialog.dismiss();
                try {

                    if (getContext() != null) {

                        if (error.getMessage().equals("com.android.volley.TimeoutError"))
                            Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_timeout), Toast.LENGTH_SHORT).show();

                        Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_unreachable), Toast.LENGTH_SHORT).show();
                    }

                    Log.v("Volly error : ", "" + error);

                } catch (Exception e) {

                    Log.v("Distribute fulfil E : ", " " + e);

                }
            }

        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                if (getContext() != null) {

                    params.put(API.KEY_JSON_USER_AUTHENTICATION, ((MainActivity) getContext()).authenticationKey);
                    params.put(API.KEY_JSON_USER_GROUP, ((MainActivity) getContext()).userGroup);
                    params.put(API.KEY_USER_ID, ((MainActivity) getContext()).userID);
                    params.put(API.KEY_CONTENT_TYPE, API.VALUE_CONTENT_VALUE_JSON);

                }

                return params;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                String your_string_json = data; // put your json
                return your_string_json.getBytes();
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                Config.volley_time_out,
                Config.max_retries,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Add the request to the RequestQueue.
        if (queue != null)
            queue.add(stringRequest);

    }

    private void fetchListData() {

        progressBar.setVisibility(View.VISIBLE);
        swipeRefreshLayout.setEnabled(false);

        RequestQueue queue = null;
        // Instantiate the RequestQueue.
        if (getContext() != null)
            queue = Volley.newRequestQueue(getContext());
        String url = API.baseUrl + API.managerDistributeFulfillmentList + API.LIMIT + API.DATA_LIMIT + API.OFFSET + customerProductArrayList.size();

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        progressBar.setVisibility(View.GONE);

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String code = jsonObject.getString(API.KEY_CODE);

                            if (code.equals(API.VALUE_CODE_SUCCESS)) {

                                customerProductListAdapter.listUpdated(dataFromServer(jsonObject));
                                recyclerView.setVisibility(View.VISIBLE);
                                noListNotice.setVisibility(View.GONE);
                                swipeRefreshLayout.setRefreshing(false);

                                JSONArray jsonArray = jsonObject.getJSONArray(API.KEY_JSON_DATA);
                                if (jsonArray.length() > 0)
                                    fetchListData();
                                else
                                    swipeRefreshLayout.setEnabled(true);

                            } else {

                                JSONObject error = jsonObject.getJSONObject(API.KEY_JSON_ERROR);

                                if (code.equals(API.VALUE_CODE_AUTHENTICATION_ERROR)) {

                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                } else if (code.equals(API.VALUE_CODE_PARAMETER_ERROR)) {
                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                swipeRefreshLayout.setRefreshing(false);
                swipeRefreshLayout.setEnabled(true);

                try {

                    if (getContext() != null) {

                        if (error.getMessage().equals("com.android.volley.TimeoutError"))
                            Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_timeout), Toast.LENGTH_SHORT).show();

                        Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_unreachable), Toast.LENGTH_SHORT).show();
                    }

                    Log.v("Volly error : ", "" + error);

                } catch (Exception e) {

                    Log.v("Distribute ful E : ", " " + e);

                }
            }

        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                if (getContext() != null) {
                    params.put(API.KEY_JSON_USER_AUTHENTICATION, ((MainActivity) getContext()).authenticationKey);
                    params.put(API.KEY_JSON_USER_GROUP, ((MainActivity) getContext()).userGroup);
                    params.put(API.KEY_USER_ID, ((MainActivity) getContext()).userID);
                    params.put(API.KEY_CONTENT_TYPE, API.VALUE_CONTENT_VALUE);
                }

                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                Config.volley_time_out,
                Config.max_retries,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Add the request to the RequestQueue.
        if (queue != null)
            queue.add(stringRequest);

    }

    private CustomerProduct[] dataFromServer(JSONObject jsonObject) throws JSONException {

        JSONArray jsonArray = jsonObject.getJSONArray(API.KEY_JSON_DATA);

        CustomerProduct[] customerProducts = new CustomerProduct[jsonArray.length()];

        for (int i = 0; i < jsonArray.length(); i++) {

            JSONObject customerProduct = jsonArray.getJSONObject(i);

            customerProducts[i] = new CustomerProduct(customerProduct.getString(API.KEY_CUSTOMER_PRODUCT_ECR_NUMBER), customerProduct.getString(API.KEY_CUSTOMER_PRODUCT_CUSTOMER_NAME), customerProduct.getString(API.KEY_CUSTOMER_PRODUCT_CUSTOMER_ADDRESS), customerProduct.getString(API.KEY_CUSTOMER_PRODUCT_PRICE), customerProduct.getString(API.KEY_CUSTOMER_PRODUCT_MERCHANT_NAME), customerProduct.getString(API.KEY_CUSTOMER_PRODUCT_CUSTOMER_PHONE), customerProduct.getString(API.KEY_CUSTOMER_PRODUCT_MERCHANT_PHONE));

        }

        return customerProducts;
    }

    private void showFeed(ArrayList<CustomerProduct> customerProductArrayList) {

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        customerProductListAdapter = new CustomerProductListAdapter(getActivity(), customerProductArrayList, Config.TAG_DISTRIBUTE_DELIVERY_FRAGMENT);
        recyclerView.setAdapter(customerProductListAdapter);

    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {

        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (scanResult != null) {

            if (resultCode == RESULT_OK) {
                // contents contains whatever the code was
                String contents = scanResult.getContents();

                // Format contains the type of code i.e. UPC, EAN, QRCode etc...
                //String format = intent.getStringExtra("SCAN_RESULT_FORMAT");

                // Handle successful scan. In this example add contents to ArrayList
                if (contents != null) {
                    for (int i = 0; i < customerProductArrayList.size(); i++) {

                        if (customerProductArrayList.get(i).getECRNumber().equals(contents)) {

                            if (customerProductArrayListForServer.size() > 0) {

                                Boolean found = false;

                                for (int j = 0; j < customerProductArrayListForServer.size(); j++) {

                                    if (customerProductArrayListForServer.get(j).getECRNumber().equals(contents)) {

                                        found = true;
                                        break;

                                    }

                                }

                                if (!found) {

                                    customerProduct = customerProductArrayList.get(i);
                                    selectMerchantItem();
                                    customerProductListAdapter.notifyItemChanged(i);
                                    qrScanCodes.add(contents);

                                }

                            } else {

                                customerProduct = customerProductArrayList.get(i);
                                selectMerchantItem();
                                customerProductListAdapter.notifyItemChanged(i);
                                qrScanCodes.add(contents);

                            }

                            break;

                        }

                    }

                }

                intentIntegrator.addExtra("PROMPT_MESSAGE", "Total : " + qrScanCodes.size());
                intentIntegrator.initiateScan();

            } else if (resultCode == RESULT_CANCELED) {

                qrScanCodes.clear();

            }


        } else {
            Toast toast = Toast.makeText(getContext(),
                    "No scan data received!", Toast.LENGTH_SHORT);
            toast.show();
        }

    }

    private void updateStatus(final String data) {
        final ProgressDialog dialog = ProgressDialog.show(getActivity(), "",
                getActivity().getResources().getString(R.string.loading), true);

        dialog.show();
        RequestQueue queue = null;
        // Instantiate the RequestQueue.
        if (getContext() != null)
            queue = Volley.newRequestQueue(getContext());
        String url = API.baseUrl + API.managerParcelUpdate;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        swipeRefreshLayout.setRefreshing(false);
                        dialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String code = jsonObject.getString(API.KEY_CODE);

                            if (code.equals(API.VALUE_CODE_SUCCESS)) {

                                JSONObject data = jsonObject.getJSONObject(API.KEY_JSON_DATA);

                                JSONArray updated = data.getJSONArray(API.KEY_JSON_UPDATED);
                                JSONArray rejected = data.getJSONArray(API.KEY_JSON_REJECTED);
                                JSONArray failed = data.getJSONArray(API.KEY_JSON_FAILED);

                                if (updated.length() > 0) {
                                    Toast.makeText(getContext(), "SUCCESS", Toast.LENGTH_SHORT).show();
                                    refreshList();

                                    if ((getContext() != null))
                                        ((MainActivity) getContext()).refreshDistributeTabTitle();

                                } else if (rejected.length() > 0) {
                                    Toast.makeText(getContext(), " " + rejected.getJSONObject(0).getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_SHORT).show();
                                } else if (failed.length() > 0) {
                                    Toast.makeText(getContext(), " " + failed.getJSONObject(0).getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_SHORT).show();

                                }

                            } else {

                                JSONObject error = jsonObject.getJSONObject(API.KEY_JSON_ERROR);

                                if (code.equals(API.VALUE_CODE_AUTHENTICATION_ERROR)) {

                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                } else if (code.equals(API.VALUE_CODE_PARAMETER_ERROR)) {
                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                swipeRefreshLayout.setRefreshing(false);
                dialog.dismiss();
                try {

                    if (getContext() != null) {

                        if (error.getMessage().equals("com.android.volley.TimeoutError"))
                            Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_timeout), Toast.LENGTH_SHORT).show();

                        Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_unreachable), Toast.LENGTH_SHORT).show();
                    }

                    Log.v("Volly error : ", "" + error);

                } catch (Exception e) {

                    Log.v("Distribute ful E : ", " " + e);

                }
            }

        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                if (getContext() != null) {

                    params.put(API.KEY_JSON_USER_AUTHENTICATION, ((MainActivity) getContext()).authenticationKey);
                    params.put(API.KEY_JSON_USER_GROUP, ((MainActivity) getContext()).userGroup);
                    params.put(API.KEY_USER_ID, ((MainActivity) getContext()).userID);
                    params.put(API.KEY_CONTENT_TYPE, API.VALUE_CONTENT_VALUE_JSON);

                }

                return params;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                String your_string_json = data; // put your json
                return your_string_json.getBytes();
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                Config.volley_time_out,
                Config.max_retries,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Add the request to the RequestQueue.
        if (queue != null)
            queue.add(stringRequest);

    }

}
