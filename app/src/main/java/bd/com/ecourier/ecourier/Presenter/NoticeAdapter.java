package bd.com.ecourier.ecourier.Presenter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;

import bd.com.ecourier.ecourier.Model.Notice;
import bd.com.ecourier.ecourier.R;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by User on 11-Dec-17.
 */

public class NoticeAdapter extends RecyclerView.Adapter<NoticeAdapter.NoticeAdapterHolder> {

    private LayoutInflater inflater;
    private Activity activity;
    private ArrayList<Notice> noticeArrayList;
    private String fragmentTag;

    public NoticeAdapter(Activity activity, ArrayList<Notice> noticeArrayList, String fragmentTag) {

        inflater = LayoutInflater.from(activity);
        this.activity = activity;
        this.noticeArrayList = noticeArrayList;
        this.fragmentTag = fragmentTag;

    }

    @Override
    public NoticeAdapterHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new NoticeAdapterHolder(inflater.inflate(R.layout.notice, parent, false));
    }

    @Override
    public void onBindViewHolder(NoticeAdapterHolder holder, int position) {

        Notice notice = noticeArrayList.get(position);

        holder.id = notice.getId();
        holder.title.setText(notice.getTitle());
        holder.description.setText(notice.getDescription());
        holder.publishedDate.setText(notice.getPublishedDate());

    }

    @Override
    public int getItemCount() {
        return noticeArrayList.size();
    }

    public void listUpdated(Notice[] notices) {

        noticeArrayList.clear();
        Collections.addAll(noticeArrayList, notices);
        notifyDataSetChanged();

    }

    class NoticeAdapterHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.description)
        TextView description;
        @BindView(R.id.published_date)
        TextView publishedDate;

        private String id;

        NoticeAdapterHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }


    }
}
