package bd.com.ecourier.ecourier.Presenter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import bd.com.ecourier.ecourier.R;
import bd.com.ecourier.ecourier.View.PendingDeliveryFragment;
import bd.com.ecourier.ecourier.View.PendingReturnFragment;

/**
 * Created by User on 17-Dec-17.
 */

public class PendingTabAdapter extends android.support.v4.app.FragmentStatePagerAdapter {

    public int returnPendingCount, deliveryPendingCount;
    private Context context;

    public PendingTabAdapter(Context context, FragmentManager fm) {
        super(fm);
        this.context = context;

        deliveryPendingCount = 0;
        returnPendingCount = 0;

    }

    @Override
    public Fragment getItem(int position) {

        if (position == 0) {
            return new PendingDeliveryFragment();
        } else {
            return new PendingReturnFragment();
        }

    }

    @Override
    public CharSequence getPageTitle(int position) {

        if (position == 0) {
            return context.getString(R.string.distribute_delivery) + " (" + deliveryPendingCount + ")";
        } else {
            return context.getString(R.string.distribute_return) + " (" + returnPendingCount + ")";
        }

    }

    @Override
    public int getCount() {

        return 2;

    }

}
