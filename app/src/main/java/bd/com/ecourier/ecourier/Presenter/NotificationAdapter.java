package bd.com.ecourier.ecourier.Presenter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;

import bd.com.ecourier.ecourier.Model.Notification;
import bd.com.ecourier.ecourier.R;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by User on 11-Dec-17.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.NotificationAdapterHolder> {

    private LayoutInflater inflater;
    private Activity activity;
    private ArrayList<Notification> notificationArrayList;
    private String fragmentTag;

    public NotificationAdapter(Activity activity, ArrayList<Notification> notificationArrayList, String fragmentTag) {

        inflater = LayoutInflater.from(activity);
        this.activity = activity;
        this.notificationArrayList = notificationArrayList;
        this.fragmentTag = fragmentTag;

    }

    @Override
    public NotificationAdapterHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new NotificationAdapterHolder(inflater.inflate(R.layout.notice, parent, false));
    }

    @Override
    public void onBindViewHolder(NotificationAdapterHolder holder, int position) {

        Notification notification = notificationArrayList.get(position);

        holder.title.setText(notification.getTitle());
        holder.description.setText(notification.getDescription());

    }

    @Override
    public int getItemCount() {
        return notificationArrayList.size();
    }

    public void listUpdated(Notification[] notifications) {

        notificationArrayList.clear();
        Collections.addAll(notificationArrayList, notifications);
        notifyDataSetChanged();

    }

    class NotificationAdapterHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.description)
        TextView description;
        @BindView(R.id.published_date)
        TextView publishedDate;

        private String id;

        NotificationAdapterHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }


    }
}
