package bd.com.ecourier.ecourier.View;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bd.com.ecourier.ecourier.API;
import bd.com.ecourier.ecourier.Config;
import bd.com.ecourier.ecourier.Model.DeliveredItem;
import bd.com.ecourier.ecourier.Presenter.DeliveredItemAdapter;
import bd.com.ecourier.ecourier.R;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by User on 10-Dec-17.
 */

public class DistributeDeliveredFragment extends Fragment {

    @BindView(R.id.recyclerView_list)
    RecyclerView recyclerView;
    @BindView(R.id.total_parcel)
    TextView totalParcel;
    @BindView(R.id.total_collected)
    TextView totalCollected;
    @BindView(R.id.total_price)
    TextView totalPrice;

    private DeliveredItemAdapter deliveredItemAdapter;
    private View rootView;

    private ArrayList<DeliveredItem> deliveredItemArrayList;

    public DistributeDeliveredFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.delivery_delivered_layout, container, false);
        ButterKnife.bind(this, rootView);

        deliveredItemArrayList = new ArrayList<>();

        showFeed(deliveredItemArrayList);

        if (getContext() != null)
            if (((MainActivity) getContext()).connectedTOInternet)
                fetchData();

        return rootView;
    }

    private void showFeed(ArrayList<DeliveredItem> deliveredItemArrayList) {

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        deliveredItemAdapter = new DeliveredItemAdapter(getActivity(), deliveredItemArrayList, Config.TAG_DISTRIBUTE_DELIVERED_FRAGMENT);
        recyclerView.setAdapter(deliveredItemAdapter);

    }

    private void fetchData() {

        RequestQueue queue = null;
        // Instantiate the RequestQueue.
        if (getContext() != null)
            queue = Volley.newRequestQueue(getContext());
        String url = API.baseUrl + API.agentDeliveredReport;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String code = jsonObject.getString(API.KEY_CODE);

                            if (code.equals(API.VALUE_CODE_SUCCESS)) {

                                deliveredItemAdapter.listUpdated(dataFromServer(jsonObject));

                            } else {

                                JSONObject error = jsonObject.getJSONObject(API.KEY_JSON_ERROR);

                                if (code.equals(API.VALUE_CODE_AUTHENTICATION_ERROR)) {

                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                } else if (code.equals(API.VALUE_CODE_PARAMETER_ERROR)) {
                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                try {

                    if (getContext() != null) {

                        if (error.getMessage().equals("com.android.volley.TimeoutError"))
                            Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_timeout), Toast.LENGTH_SHORT).show();

                        Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_unreachable), Toast.LENGTH_SHORT).show();
                    }

                    Log.v("Volly error : ", "" + error);

                } catch (Exception e) {

                    Log.v("Distribute ful E : ", " " + e);

                }
            }

        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                if (getContext() != null) {
                    params.put(API.KEY_JSON_USER_AUTHENTICATION, ((MainActivity) getContext()).authenticationKey);
                    params.put(API.KEY_JSON_USER_GROUP, ((MainActivity) getContext()).userGroup);
                    params.put(API.KEY_USER_ID, ((MainActivity) getContext()).userID);
                    params.put(API.KEY_CONTENT_TYPE, API.VALUE_CONTENT_VALUE);
                }

                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                Config.volley_time_out,
                Config.max_retries,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Add the request to the RequestQueue.
        if (queue != null)
            queue.add(stringRequest);

    }

    private DeliveredItem[] dataFromServer(JSONObject jsonObject) throws JSONException {

        JSONObject deliveryDetail = jsonObject.getJSONObject(API.KEY_JSON_DATA);

        totalCollected.setText(String.format(" %s ", deliveryDetail.getString(API.KEY_MANAGER_RECEIVE_CASH_TOTAL_COLLECTED_AMOUNT)));
        totalPrice.setText(String.format(" %s ", deliveryDetail.getString(API.KEY_MANAGER_RECEIVE_CASH_TOTAL_PRODUCT_PRICE)));
        totalParcel.setText(String.format(" %s", deliveryDetail.getString(API.KEY_TOTAL_PARCEL)));

        if (getContext() != null)
            ((MainActivity) getContext()).refreshDistributeDeliveredTitle(deliveryDetail.getString(API.KEY_MANAGER_RECEIVE_CASH_TOTAL_COLLECTED_AMOUNT) + " | " + deliveryDetail.getString(API.KEY_TOTAL_PARCEL));

        JSONArray deliveredItem = deliveryDetail.getJSONArray(API.KEY_DELIVERED_PARCEL);

        DeliveredItem[] deliveredItems = new DeliveredItem[deliveredItem.length()];

        for (int i = 0; i < deliveredItem.length(); i++) {

            JSONObject item = deliveredItem.getJSONObject(i);

            deliveredItems[i] = new DeliveredItem(item.getString(API.KEY_CUSTOMER_PRODUCT_ECR_NUMBER_LOWER_CASE), item.getString(API.KEY_CUSTOMER_PRODUCT_PRICE), item.getString(API.KEY_CUSTOMER_PRODUCT_COLLECTED_AMOUNT), item.getString(API.KEY_DELIVERED_SENDER));

        }

        return deliveredItems;
    }

}
