package bd.com.ecourier.ecourier.View;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import bd.com.ecourier.ecourier.Presenter.PendingTabAdapter;
import bd.com.ecourier.ecourier.R;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by User on 10-Dec-17.
 */

public class PendingTabFragment extends Fragment {

    // Find the view pager that will allow the user to swipe between fragments
    @BindView(R.id.tab_view_pager)
    ViewPager viewPager;
    // Find the tab layout that shows the tabs
    @BindView(R.id.tab_bar)
    TabLayout tabLayout;

    private PendingTabAdapter adapter;
    private int tabPosition = 0;

    public PendingTabFragment() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tab_layout, container, false);
        ButterKnife.bind(this, rootView);

        if (getActivity() != null) {

            //getResources().getString(R.string.pick) use this
            getActivity().setTitle(getActivity().getResources().getString(R.string.pending));

        }

        // Create an adapter that knows which fragment should be shown on each page
        adapter = new PendingTabAdapter(getContext(), getChildFragmentManager());

        // Set the adapter onto the view pager
        viewPager.setAdapter(adapter);

        // Connect the tab layout with the view pager. This will
        //   1. Update the tab layout when the view pager is swiped
        //   2. Update the view pager when a tab is selected
        //   3. Set the tab layout's tab names with the view pager's adapter's titles
        //      by calling onPageTitle()
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                tabPosition = tab.getPosition();

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        return rootView;
    }

    @Override
    public void onResume() {

        if (getArguments() != null) {

            if (getArguments().getBoolean("onResume", true)) {

                PendingTabAdapter adapter = new PendingTabAdapter(getContext(), getChildFragmentManager());
                viewPager.setAdapter(adapter);
                tabLayout.getTabAt(getArguments().getInt("tabPosition", tabPosition)).select();
                Toast.makeText(getContext(), "position : " + tabPosition, Toast.LENGTH_SHORT).show();

            }

        }


        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();

        if (getArguments() != null) {

            getArguments().putInt("tabPosition", tabPosition);
            getArguments().putBoolean("onResume", true);

        }

    }

    public void refreshDeliveredTitle(int amount) {

        adapter.deliveryPendingCount = amount;
        adapter.notifyDataSetChanged();

    }

    public void refreshReturnTitle(int amount) {

        adapter.returnPendingCount = amount;
        adapter.notifyDataSetChanged();

    }

}
