package bd.com.ecourier.ecourier.Presenter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;

import bd.com.ecourier.ecourier.Model.FAQ;
import bd.com.ecourier.ecourier.R;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by User on 11-Dec-17.
 */

public class FAQAdapter extends RecyclerView.Adapter<FAQAdapter.FAQAdapterHolder> {

    private LayoutInflater inflater;
    private Activity activity;
    private ArrayList<FAQ> faqArrayList;
    private String fragmentTag;

    public FAQAdapter(Activity activity, ArrayList<FAQ> faqArrayList, String fragmentTag) {

        inflater = LayoutInflater.from(activity);
        this.activity = activity;
        this.faqArrayList = faqArrayList;
        this.fragmentTag = fragmentTag;

    }

    @Override
    public FAQAdapterHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new FAQAdapterHolder(inflater.inflate(R.layout.faq, parent, false));
    }

    @Override
    public void onBindViewHolder(FAQAdapterHolder holder, int position) {

        FAQ faq = faqArrayList.get(position);

        holder.id = faq.getId();
        holder.question.setText(faq.getQuestion());
        holder.answer.setText(faq.getAnswer());

    }

    @Override
    public int getItemCount() {
        return faqArrayList.size();
    }

    public void listUpdated(FAQ[] faqs) {

        faqArrayList.clear();
        Collections.addAll(faqArrayList, faqs);
        notifyDataSetChanged();

    }

    class FAQAdapterHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.question)
        TextView question;
        @BindView(R.id.answer)
        TextView answer;

        private String id;

        FAQAdapterHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }


    }
}
