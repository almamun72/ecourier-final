package bd.com.ecourier.ecourier.Model;

import java.util.ArrayList;

/**
 * Created by User on 25-Dec-17.
 */

public class AgentConsignments {

    private ArrayList<String> consignment_no;

    public AgentConsignments(ArrayList<String> consignment_no) {

        this.consignment_no = consignment_no;

    }

    public ArrayList<String> getConsignment_no() {
        return consignment_no;
    }

    public void setConsignment_no(ArrayList<String> consignment_no) {
        this.consignment_no = consignment_no;
    }
}
