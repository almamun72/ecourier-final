package bd.com.ecourier.ecourier.Model;

import android.graphics.Color;

/**
 * Created by User on 17-Dec-17.
 */

public class CustomerProductCashInfo {

    private String ECRNumber, customerName, productPrice, collectedPrice, customerPhoneNumber, paymentType;
    private boolean isChecked;
    private int maxLine, color, min = 3, max = 8, colorUnselected = Color.WHITE, colorSelected, textColor, colorWhite = Color.WHITE, colorBlack = Color.BLACK;

    public CustomerProductCashInfo(String ECRNumber, String customerName, String productPrice, String collectedPrice, String customerPhoneNumber, String paymentType) {

        this.ECRNumber = ECRNumber;
        this.customerName = customerName;
        this.customerPhoneNumber = customerPhoneNumber;
        this.productPrice = productPrice;
        this.collectedPrice = collectedPrice;
        this.paymentType = paymentType;
        isChecked = false;
        maxLine = min;
        color = colorUnselected;
        textColor = colorBlack;
        colorSelected = Color.parseColor("#25b472");

    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getECRNumber() {
        return ECRNumber;
    }

    public void setECRNumber(String ECRNumber) {
        this.ECRNumber = ECRNumber;
    }

    public String getCustomerPhoneNumber() {
        return customerPhoneNumber;
    }

    public void setCustomerPhoneNumber(String customerPhoneNumber) {
        this.customerPhoneNumber = customerPhoneNumber;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
        if (checked) {

            color = colorSelected;
            textColor = colorWhite;

        } else {

            color = colorUnselected;
            textColor = colorBlack;
        }
    }

    public int getMaxLine() {
        return maxLine;
    }

    public void setMaxLine(int maxLine) {
        this.maxLine = maxLine;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public int getTextColor() {
        return textColor;
    }

    public void setTextColor(int textColor) {
        this.textColor = textColor;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public String getCollectedPrice() {
        return collectedPrice;
    }

    public void setCollectedPrice(String collectedPrice) {
        this.collectedPrice = collectedPrice;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }
}
