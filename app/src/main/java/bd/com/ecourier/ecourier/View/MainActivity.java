package bd.com.ecourier.ecourier.View;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import bd.com.ecourier.ecourier.API;
import bd.com.ecourier.ecourier.Config;
import bd.com.ecourier.ecourier.NetworkChangeReceiver.ECourierApplication;
import bd.com.ecourier.ecourier.NetworkChangeReceiver.NetworkChangeReceiver;
import bd.com.ecourier.ecourier.R;
import bd.com.ecourier.ecourier.firebaseServices.NotificationData;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public String userID, userName, userGroup, authenticationKey;
    public ArrayList<String> agentIDList, agentNameList, defaultComments;
    public HashMap<String, String> parcelStatusMap;
    public boolean connectedTOInternet;
    private DashboardFragment dashboardFragment = new DashboardFragment();
    private BroadcastReceiver mNetworkReceiver;
    private TextView internetStatus;

    private PickTabFragment pickTabFragment;
    private DistributeTabFragment distributeTabFragment;
    private PendingTabFragment pendingTabFragment;
    private ReceiveTabFragment receiveTabFragment;
    private String pushNotificationFragment = null;
    private Bundle bundle;

    @Override
    protected void onStart() {
        super.onStart();

        if (!(getSharedPreferences(Config.SP_ECourier_APP, MODE_PRIVATE).getBoolean(Config.SP_LOGGED_IN, false))) {
            logOut();
        } else {

            String commentsJson, parcelStatus, pickupStatus;
            parcelStatusMap = new HashMap<>();
            defaultComments = new ArrayList<>();

            userID = getSharedPreferences(Config.SP_ECourier_APP, MODE_PRIVATE)
                    .getString(API.KEY_USER_ID, "");

            userName = getSharedPreferences(Config.SP_ECourier_APP, MODE_PRIVATE)
                    .getString(API.KEY_USER_NAME, "");

            userGroup = getSharedPreferences(Config.SP_ECourier_APP, MODE_PRIVATE)
                    .getString(Config.SP_USER_TYPE, "");

            authenticationKey = getSharedPreferences(Config.SP_ECourier_APP, MODE_PRIVATE)
                    .getString(API.KEY_JSON_USER_AUTHENTICATION, "");

            commentsJson = getSharedPreferences(Config.SP_ECourier_APP, MODE_PRIVATE)
                    .getString(API.KEY_JSON_USER_DEFAULT_COMMENTS, "");

            parcelStatus = getSharedPreferences(Config.SP_ECourier_APP, MODE_PRIVATE)
                    .getString(API.KEY_JSON_USER_PARCEL_STATUS, "");

            pickupStatus = getSharedPreferences(Config.SP_ECourier_APP, MODE_PRIVATE)
                    .getString(API.KEY_JSON_USER_PICKUP_STATUS, "");

            try {

                JSONObject jsonObjectDefaultComment = new JSONObject(commentsJson);
                JSONObject jsonObjectParcelStatus = new JSONObject(parcelStatus);
                JSONObject jsonObjectPickupStatus = new JSONObject(pickupStatus);

                JSONArray jsonArray = jsonObjectDefaultComment
                        .getJSONArray(API.KEY_JSON_USER_DEFAULT_COMMENTS_VALUES);

                JSONArray jsonArrayParcelStatus = jsonObjectParcelStatus
                        .getJSONArray(API.KEY_JSON_USER_DEFAULT_COMMENTS_VALUES);

                JSONArray jsonArrayPickupStatus = jsonObjectPickupStatus
                        .getJSONArray(API.KEY_JSON_USER_DEFAULT_COMMENTS_VALUES);

                defaultComments.add(getResources().getString(R.string.select_comment));

                for (int i = 0; i < jsonArray.length(); i++) {

                    defaultComments.add(jsonArray.getString(i));

                }

                for (int i = 0; i < jsonArrayParcelStatus.length(); i++) {

                    JSONObject jsonObjectParcel = jsonArrayParcelStatus.getJSONObject(i);
                    JSONObject value = jsonObjectParcel.getJSONObject(API.KEY_JSON_USER_DEFAULT_NAME_VALUE_PAIRS);

                    parcelStatusMap.put(value.getString(API.KEY_JSON_USER_STATUS_CODE), value.getString(API.KEY_JSON_USER_STATUS));

                }

                for (int i = 0; i < jsonArrayPickupStatus.length(); i++) {

                    JSONObject jsonObjectPick = jsonArrayPickupStatus.getJSONObject(i);
                    JSONObject value = jsonObjectPick.getJSONObject(API.KEY_JSON_USER_DEFAULT_NAME_VALUE_PAIRS);

                    parcelStatusMap.put(value.getString(API.KEY_JSON_USER_STATUS_CODE), value.getString(API.KEY_JSON_USER_STATUS));

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            fetchManagerAgentList();

            checkValidation();

        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Locale locale = new Locale(getSharedPreferences(Config.SP_ECourier_APP, MODE_PRIVATE).getString(Config.SP_LANGUAGE, "en"));
        Locale.setDefault(locale);
        Configuration conf = getBaseContext().getResources().getConfiguration();
        conf.locale = locale;
        getBaseContext().getResources().updateConfiguration(conf, getBaseContext().getResources().getDisplayMetrics());

        setContentView(R.layout.activity_main);
        //ButterKnife.bind(this);

        mNetworkReceiver = new NetworkChangeReceiver();
        registerNetworkBroadcastForNougat();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        ((TextView) navigationView.getHeaderView(0).findViewById(R.id.textView)).setText(String.format("%s%s", getResources().getString(R.string.user_name), getSharedPreferences(Config.SP_ECourier_APP, MODE_PRIVATE).getString(API.KEY_USER_NAME, "")));

        ((TextView) navigationView.getHeaderView(0).findViewById(R.id.user_id)).setText(String.format("%s%s", getResources().getString(R.string.user_id), getSharedPreferences(Config.SP_ECourier_APP, MODE_PRIVATE).getString(API.KEY_USER_ID, "")));

        internetStatus = findViewById(R.id.network_status);

        navigationView.setNavigationItemSelectedListener(this);

        if (savedInstanceState == null) {

            bundle = new Bundle();

            if (getIntent().getExtras() != null) {

                if (!Objects.equals(getIntent().getExtras().get(Config.notification_push_key), null)) {

//                    Log.v("Key: ", " Value: " + getIntent().getExtras().get(Config.notification_push_key));
                    pushNotificationFragment = getIntent().getExtras().get(Config.notification_push_key).toString();
                } else if (!Objects.equals(getIntent().getExtras().getString(NotificationData.TEXT), null)) {

//                    Log.v("Data: ", " Value: " + getIntent().getExtras().getString(NotificationData.TEXT));
                    pushNotificationFragment = getIntent().getExtras().getString(NotificationData.TEXT);


                }

            } else {

//                Log.v("DATA: ", " NO ");
                pushNotificationFragment = "";

            }

        }

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_main, dashboardFragment, Config.TAG_DASHBOARD_FRAGMENT)
                .commit();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {

            if (getSupportFragmentManager().getBackStackEntryCount() == 0)
                finish();

            else if (getSupportFragmentManager().findFragmentByTag(Config.TAG_DASHBOARD_FRAGMENT).isVisible()) {

                finish();

            } else if (getSupportFragmentManager().findFragmentByTag(Config.TAG_PICK_TAB_FRAGMENT) != null) {

                if (getSupportFragmentManager().findFragmentByTag(Config.TAG_PICK_TAB_FRAGMENT).isVisible()) {

                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.content_main, dashboardFragment, Config.TAG_DASHBOARD_FRAGMENT)
                            .commit();

                } else {
                    getSupportFragmentManager().popBackStack();
                }


            } else if (getSupportFragmentManager().findFragmentByTag(Config.TAG_DISTRIBUTE_TAB_FRAGMENT) != null) {

                if (getSupportFragmentManager().findFragmentByTag(Config.TAG_DISTRIBUTE_TAB_FRAGMENT).isVisible()) {

                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.content_main, dashboardFragment, Config.TAG_DASHBOARD_FRAGMENT)
                            .commit();

                } else {
                    getSupportFragmentManager().popBackStack();
                }

            } else if (getSupportFragmentManager().findFragmentByTag(Config.TAG_RECEIVE_TAB_FRAGMENT) != null) {

                if (getSupportFragmentManager().findFragmentByTag(Config.TAG_RECEIVE_TAB_FRAGMENT).isVisible()) {

                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.content_main, dashboardFragment, Config.TAG_DASHBOARD_FRAGMENT)
                            .commit();

                } else {
                    getSupportFragmentManager().popBackStack();
                }

            } else if (getSupportFragmentManager().findFragmentByTag(Config.TAG_PENDING_TAB_FRAGMENT) != null) {

                if (getSupportFragmentManager().findFragmentByTag(Config.TAG_PENDING_TAB_FRAGMENT).isVisible()) {

                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.content_main, dashboardFragment, Config.TAG_DASHBOARD_FRAGMENT)
                            .commit();

                } else {
                    getSupportFragmentManager().popBackStack();
                }

            } else {

                if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                    getSupportFragmentManager().popBackStack();

                }
            }

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {

            this.getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.content_main, new SearchFragment(), Config.TAG_SEARCH_TAB_FRAGMENT)
                    .addToBackStack(null)
                    .commit();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_dashboard) {

            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.content_main, dashboardFragment, Config.TAG_DASHBOARD_FRAGMENT)
                    .commit();

        } else if (id == R.id.nav_pick) {

            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.content_main, instanceOfPickTab(), Config.TAG_PICK_TAB_FRAGMENT)
                    .commit();

        } else if (id == R.id.nav_receive) {

            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.content_main, instanceOfReceiveTab(), Config.TAG_RECEIVE_TAB_FRAGMENT)
                    .commit();

        } else if (id == R.id.nav_distribute) {

            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.content_main, instanceOfDistributeTab(), Config.TAG_DISTRIBUTE_TAB_FRAGMENT)
                    .commit();

        } else if (id == R.id.nav_agent_lsit) {

            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.content_main, new AgentListFragment(), Config.TAG_AGENT_LIST_FRAGMENT)
                    .addToBackStack(null)
                    .commit();

        } else if (id == R.id.nav_settings) {

            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.content_main, new SettingsFragment(), Config.TAG_AGENT_LIST_FRAGMENT)
                    .addToBackStack(null)
                    .commit();

        } else if (id == R.id.nav_logout) {

            logOut();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void logOut() {

        SharedPreferences sp = getSharedPreferences(Config.SP_ECourier_APP, MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean(Config.SP_LOGGED_IN, false);
        editor.clear();

        if (editor.commit()) {
            Intent loginIntent = new Intent(this, LoginActivity.class);
            startActivity(loginIntent);
            finish();
        }

    }

    private void registerNetworkBroadcastForNougat() {

        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                registerReceiver(mNetworkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                registerReceiver(mNetworkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                registerReceiver(mNetworkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
            }
        } catch (Exception e) {
            Log.v("Internet Reg : ", " " + e);
        }

    }

    protected void unregisterNetworkChanges() {
        try {
            unregisterReceiver(mNetworkReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterNetworkChanges();
    }

    @Override
    protected void onPause() {

        super.onPause();
        ECourierApplication.activityPaused();// On Pause notify the Application
    }

    @Override
    protected void onResume() {

        super.onResume();
        ECourierApplication.activityResumed();// On Resume notify the Application
    }

    public void networkStateChange(Boolean isConnected) {

        connectedTOInternet = isConnected;

        if (isConnected) {

            internetStatus.setText(R.string.connected);
            internetStatus.setBackgroundColor(Color.GREEN);

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {

                @Override
                public void run() {
                    // Hide your View after 1 seconds
                    internetStatus.setVisibility(View.GONE);

                }
            }, 1000);

            if (pushNotificationFragment.equals(Config.pushOpenFragmentKeyPickAssigned)) {

                pushNotificationFragment = "";

                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.content_main, instanceOfPickTab(), Config.TAG_PICK_TAB_FRAGMENT)
                        .addToBackStack(null)
                        .commit();

            } else if (pushNotificationFragment.equals(Config.pushOpenFragmentKeyParcelAssignedDelivery)) {

                pushNotificationFragment = "";

                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.content_main, instanceOfReceiveTab(), Config.TAG_RECEIVE_TAB_FRAGMENT)
                        .addToBackStack(null)
                        .commit();
            } else if (pushNotificationFragment.equals(Config.pushOpenFragmentKeyPendingWay) || pushNotificationFragment.equals(Config.pushOpenFragmentKeyPendingHold) || pushNotificationFragment.equals(Config.pushOpenFragmentKeyPendingReturn) || pushNotificationFragment.equals(Config.pushOpenFragmentKeyPendingCancel)) {

                pushNotificationFragment = "";

                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.content_main, instanceOfDistributeTab(), Config.TAG_DISTRIBUTE_TAB_FRAGMENT)
                        .addToBackStack(null)
                        .commit();
            } else if (pushNotificationFragment.equals(Config.pushOpenFragmentKeyNotice)) {

                pushNotificationFragment = "";

                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.content_main, new NoticeFragment(), Config.TAG_NOTICE)
                        .addToBackStack(null)
                        .commit();
            }


        } else {
            internetStatus.setBackgroundColor(Color.RED);
            internetStatus.setVisibility(View.VISIBLE);
            internetStatus.setText(R.string.no_internet);
        }

    }

    public PickTabFragment instanceOfPickTab() {

        /*if (pickTabFragment == null) {

            return pickTabFragment = new PickTabFragment();

        } else {

            return pickTabFragment;
        }*/

        return pickTabFragment = new PickTabFragment();

    }

    public ReceiveTabFragment instanceOfReceiveTab() {

        /*if (receiveTabFragment == null)
            return receiveTabFragment = new ReceiveTabFragment();
        else
            return receiveTabFragment;*/

        return receiveTabFragment = new ReceiveTabFragment();


    }

    public DistributeTabFragment instanceOfDistributeTab() {

        /*if (distributeTabFragment == null)
            return distributeTabFragment = new DistributeTabFragment();
        else
            return distributeTabFragment;*/

        return distributeTabFragment = new DistributeTabFragment();


    }

    public PendingTabFragment instanceOfPendingTab() {

        /*if (distributeTabFragment == null)
            return distributeTabFragment = new DistributeTabFragment();
        else
            return distributeTabFragment;*/

        return pendingTabFragment = new PendingTabFragment();


    }

    public void refreshPickTabTitle() {

        try {

            if (connectedTOInternet)
                pickTabFragment.refreshPageTitle();

        } catch (Exception exception) {

            Log.v("(Main)pick title: ", " " + exception);

        }

    }

    public void refreshReceiveTabTitle() {

        try {

            if (connectedTOInternet)
                receiveTabFragment.refreshPageTitle();

        } catch (Exception exception) {

            Log.v("(Main)receive title: ", " " + exception);

        }

    }

    public void refreshReceiveTransferTitle(int transferCount) {

        try {

            if (connectedTOInternet)
                receiveTabFragment.refreshTransferTitle(transferCount);

        } catch (Exception exception) {

            Log.v("(Main)rec transfer: ", " " + exception);

        }

    }

    public void refreshDistributeTransferTitle(int transferCount) {

        try {

            if (connectedTOInternet)
                distributeTabFragment.refreshTransferTitle(transferCount);

        } catch (Exception exception) {

            Log.v("(Main)rec transfer: ", " " + exception);

        }

    }

    public void refreshDistributeDeliveredTitle(String collectedAmount) {

        try {

            if (connectedTOInternet)
                distributeTabFragment.refreshDeliveredTitle(collectedAmount);

        } catch (Exception exception) {

            Log.v("(Main)rec transfer: ", " " + exception);

        }

    }

    public void refreshDistributeTabTitle() {

        try {

            if (connectedTOInternet)
                distributeTabFragment.refreshPageTitle();

        } catch (Exception exception) {

            Log.v("(Main)distribute title:", " " + exception);

        }

    }

    public void changeDistributeTab(int position) {

        try {
            distributeTabFragment.resetTabToPosition(position);
        } catch (Exception e) {
            Log.v("(Main)resetTabToPos:", " " + e);

        }

    }

    public void refreshPendingDeliveredTitle(int amount) {

        try {

            if (connectedTOInternet)
                pendingTabFragment.refreshDeliveredTitle(amount);

        } catch (Exception exception) {

            Log.v("(Main)rec pending: ", " " + exception);

        }

    }

    public void refreshPendingReturnTitle(int amount) {

        try {

            if (connectedTOInternet)
                pendingTabFragment.refreshReturnTitle(amount);

        } catch (Exception exception) {

            Log.v("(Main)rec pending: ", " " + exception);

        }

    }

    private void fetchManagerAgentList() {

        RequestQueue queue = null;
        // Instantiate the RequestQueue.
        if (getApplicationContext() != null)
            queue = Volley.newRequestQueue(getApplicationContext());
        String url = API.baseUrl + API.managerAgentList;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String code = jsonObject.getString(API.KEY_CODE);

                            if (code.equals(API.VALUE_CODE_SUCCESS)) {

                                dataFromServer(jsonObject);


                            } else {

                                JSONObject error = jsonObject.getJSONObject(API.KEY_JSON_ERROR);

                                if (code.equals(API.VALUE_CODE_AUTHENTICATION_ERROR)) {

                                    Toast.makeText(getApplicationContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                } else if (code.equals(API.VALUE_CODE_PARAMETER_ERROR)) {
                                    Toast.makeText(getApplicationContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                try {

                    if (error.getMessage().equals("com.android.volley.TimeoutError"))
                        Toast.makeText(MainActivity.this, getResources().getString(R.string.server_timeout), Toast.LENGTH_SHORT).show();

                    Toast.makeText(MainActivity.this, getResources().getString(R.string.server_unreachable), Toast.LENGTH_SHORT).show();


                    Log.v("Volly error : ", "" + error);

                } catch (Exception e) {

                    Log.v("Distribute Cancel E : ", " " + e);

                }
            }

        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                if (getApplicationContext() != null) {
                    params.put(API.KEY_JSON_USER_AUTHENTICATION, authenticationKey);
                    params.put(API.KEY_JSON_USER_GROUP, userGroup);
                    params.put(API.KEY_USER_ID, userID);
                    params.put(API.KEY_CONTENT_TYPE, API.VALUE_CONTENT_VALUE);
                }


                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                Config.volley_time_out,
                Config.max_retries,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Add the request to the RequestQueue.
        if (queue != null)
            queue.add(stringRequest);

    }

    private void dataFromServer(JSONObject jsonObject) throws JSONException {

        JSONArray jsonArray = jsonObject.getJSONArray(API.KEY_JSON_DATA);

        agentIDList = new ArrayList<>();
        agentNameList = new ArrayList<>();
        agentIDList.add("select");
        agentNameList.add("Select Agent");

        for (int i = 0; i < jsonArray.length(); i++) {

            JSONObject agentList = jsonArray.getJSONObject(i);

            agentIDList.add(agentList.getString(API.KEY_AGENT_LIST_ID));
            agentNameList.add(agentList.getString(API.KEY_AGENT_LIST_NAME));

        }


    }

    private void checkValidation() {

        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = Config.VALIDATION_BASE_URL + Config.VALIDATION_PROJECT_STATUS;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {

                        try {

                            JSONObject jsonObject = new JSONObject(response);
                            String code = jsonObject.getString(API.KEY_CODE);

                            if (code.equals(API.VALUE_CODE_SUCCESS)) {

                                JSONArray jsonArray = jsonObject.getJSONArray(API.KEY_JSON_DATA);
                                JSONObject data = jsonArray.getJSONObject(0);
                                String status = data.getString(Config.VALIDATION_STAUTS);

                                if (!status.equals(Config.VALIDATION_ACTIVE)) {
                                    logOut();
                                }

                            } else {

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.v("Volly error : ", "" + error);


            }

        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(Config.VALIDATION_PROJECT_ID, Config.PROJECT_ID);

                return params;
            }
        };

        queue.add(stringRequest);

    }

}
