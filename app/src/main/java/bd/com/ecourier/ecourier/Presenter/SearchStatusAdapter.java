package bd.com.ecourier.ecourier.Presenter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;

import bd.com.ecourier.ecourier.Model.StatusHistory;
import bd.com.ecourier.ecourier.R;
import bd.com.ecourier.ecourier.View.MainActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by User on 11-Dec-17.
 */

public class SearchStatusAdapter extends RecyclerView.Adapter<SearchStatusAdapter.SearchStatusAdapterHolder> {

    private LayoutInflater inflater;
    private Activity activity;
    private ArrayList<StatusHistory> statusHistoryArrayList;
    private String fragmentTag;

    public SearchStatusAdapter(Activity activity, ArrayList<StatusHistory> statusHistoryArrayList, String fragmentTag) {

        inflater = LayoutInflater.from(activity);
        this.activity = activity;
        this.statusHistoryArrayList = statusHistoryArrayList;
        this.fragmentTag = fragmentTag;

    }

    @Override
    public SearchStatusAdapterHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SearchStatusAdapterHolder(inflater.inflate(R.layout.status_history_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(SearchStatusAdapterHolder holder, int position) {

        StatusHistory statusHistory = statusHistoryArrayList.get(position);

        holder.branchId.setText(String.format("%s%s%s",
                activity.getResources().getString(R.string.branch_id), " ", statusHistory.getBranchId()));

        /*holder.shippingAgentGroup.setText(String.format("%s%s",
                activity.getResources().getString(R.string.shipping_agent_group),
                statusHistory.getShippingAgentGroup()));*/

        holder.shippingAgentId.setText(String.format("%s%s%s", activity.getResources().getString(R.string.shipping_agent_id), " ", statusHistory.getShippingAgentId()));
        holder.status.setText(String.format("%s%s%s", activity.getResources().getString(R.string.status), " ", ((MainActivity) activity).parcelStatusMap.get(statusHistory.getStatus())));
        holder.time.setText(String.format("%s%s%s", activity.getResources().getString(R.string.time), " ", statusHistory.getTime()));
        holder.updaterGroup.setText(String.format("%s%s%s", activity.getResources().getString(R.string.updater_group), " ", statusHistory.getUpdaterGroup()));
        holder.updaterId.setText(String.format("%s%s%s", activity.getResources().getString(R.string.updater_id), " ", statusHistory.getUpdaterId()));

    }

    @Override
    public int getItemCount() {
        return statusHistoryArrayList.size();
    }

    public void listUpdated(StatusHistory[] statusHistories) {

        statusHistoryArrayList.clear();
        Collections.addAll(statusHistoryArrayList, statusHistories);
        notifyDataSetChanged();

    }

    class SearchStatusAdapterHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.product_status)
        TextView status;
        @BindView(R.id.updater_id)
        TextView updaterId;
        @BindView(R.id.updater_group)
        TextView updaterGroup;
        @BindView(R.id.shipping_agent_id)
        TextView shippingAgentId;
        /*@BindView(R.id.shipping_agent_group)
        TextView shippingAgentGroup;*/
        @BindView(R.id.branch_id)
        TextView branchId;
        @BindView(R.id.time)
        TextView time;

        SearchStatusAdapterHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }


    }
}
