package bd.com.ecourier.ecourier.Presenter;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;

import bd.com.ecourier.ecourier.Model.CustomerProductCashInfo;
import bd.com.ecourier.ecourier.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by User on 11-Dec-17.
 */

public class CustomerProductCashInfoListAdapter extends RecyclerView.Adapter<CustomerProductCashInfoListAdapter.CustomerProductCashInfoListAdapterHolder> {

    private LayoutInflater inflater;
    private Activity activity;
    private ArrayList<CustomerProductCashInfo> customerProductCashInfoArrayList;
    private String fragmentTag, userType;

    public CustomerProductCashInfoListAdapter(Activity activity, ArrayList<CustomerProductCashInfo> customerProductCashInfoArrayList, String fragmentTag) {

        inflater = LayoutInflater.from(activity);
        this.activity = activity;
        this.customerProductCashInfoArrayList = customerProductCashInfoArrayList;
        this.fragmentTag = fragmentTag;

    }

    @Override
    public CustomerProductCashInfoListAdapterHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CustomerProductCashInfoListAdapterHolder(inflater.inflate(R.layout.agent_cash_info_item, parent, false));
    }

    @Override
    public void onBindViewHolder(CustomerProductCashInfoListAdapterHolder holder, int position) {

        CustomerProductCashInfo customerProductCashInfo = customerProductCashInfoArrayList.get(position);

        holder.ecrNumber.setText(customerProductCashInfo.getECRNumber());
        holder.ecrNumber.setTextColor(customerProductCashInfo.getTextColor());

        holder.customerName.setText(customerProductCashInfo.getCustomerName());
        holder.customerName.setTextColor(customerProductCashInfo.getTextColor());

        holder.productPrice.setText(String.format("%s %s", activity.getResources().getString(R.string.product_price), customerProductCashInfo.getProductPrice()));
        holder.productPrice.setTextColor(customerProductCashInfo.getTextColor());

        holder.paymentType.setText(customerProductCashInfo.getPaymentType());
        holder.paymentType.setTextColor(customerProductCashInfo.getTextColor());

        holder.totalPrice.setText(String.format("%s %s", activity.getResources().getString(R.string.collected_amount), customerProductCashInfo.getCollectedPrice()));
        holder.totalPrice.setTextColor(customerProductCashInfo.getTextColor());

        /*holder.agentChecked.setChecked(customerProductCashInfo.isChecked());*/

        if (customerProductCashInfo.isChecked()) {
            holder.agentChecked.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.ic_check_box_black_24dp));
        } else {
            holder.agentChecked.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.ic_check_box_outline_blank_black_24dp));
        }

        holder.agentCashBody.setBackgroundColor(customerProductCashInfo.getColor());

        holder.customerPhoneNumber = customerProductCashInfo.getCustomerPhoneNumber();

    }

    @Override
    public int getItemCount() {
        return customerProductCashInfoArrayList.size();
    }

    public void listUpdated(CustomerProductCashInfo[] customerProducts) {

        customerProductCashInfoArrayList.clear();
        Collections.addAll(customerProductCashInfoArrayList, customerProducts);
        notifyDataSetChanged();

    }

    class CustomerProductCashInfoListAdapterHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.product_ecr_number)
        TextView ecrNumber;
        @BindView(R.id.customer_name)
        TextView customerName;
        @BindView(R.id.payment_type)
        TextView paymentType;
        @BindView(R.id.product_price)
        TextView productPrice;
        @BindView(R.id.checkbox)
        ImageView agentChecked;
        @BindView(R.id.total_price)
        TextView totalPrice;
        @BindView(R.id.agent_call_btn)
        ImageView callBtn;
        @BindView(R.id.agent_list_body)
        RelativeLayout agentCashBody;

        private String customerPhoneNumber;

        CustomerProductCashInfoListAdapterHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.agent_call_btn)
        void onClick(View view) {

            switch (view.getId()) {

                case R.id.agent_call_btn: {

                    int REQUEST_PHONE_CALL = 1;

                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + customerPhoneNumber));

                    if (ContextCompat.checkSelfPermission(activity,
                            Manifest.permission.CALL_PHONE)
                            != PackageManager.PERMISSION_GRANTED) {

                        // Permission is not granted
                        // Should we show an explanation?
                        if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                                Manifest.permission.CALL_PHONE)) {

                            // Show an explanation to the user *asynchronously* -- don't block
                            // this thread waiting for the user's response! After the user
                            // sees the explanation, try again to request the permission.

                            ActivityCompat.requestPermissions(activity,
                                    new String[]{Manifest.permission.CALL_PHONE},
                                    REQUEST_PHONE_CALL);

                        } else {

                            // No explanation needed; request the permission
                            ActivityCompat.requestPermissions(activity,
                                    new String[]{Manifest.permission.CALL_PHONE},
                                    REQUEST_PHONE_CALL);

                            // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                            // app-defined int constant. The callback method gets the
                            // result of the request.
                        }
                    } else {
                        // Permission has already been granted
                        activity.startActivity(callIntent);


                    }

                    break;
                }

            }

        }

    }
}
