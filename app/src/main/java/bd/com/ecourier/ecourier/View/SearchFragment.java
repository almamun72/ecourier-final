package bd.com.ecourier.ecourier.View;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bd.com.ecourier.ecourier.API;
import bd.com.ecourier.ecourier.Config;
import bd.com.ecourier.ecourier.Model.AgentConsignments;
import bd.com.ecourier.ecourier.Model.AgentConsignmentsMobile;
import bd.com.ecourier.ecourier.Model.AgentConsignmentsProductID;
import bd.com.ecourier.ecourier.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

/**
 * Created by User on 10-Dec-17.
 */

public class SearchFragment extends Fragment {

    @BindView(R.id.search_spinner)
    Spinner searchTypeSpinner;
    @BindView(R.id.search_editText)
    EditText searchNumber;
    @BindView(R.id.search_btn)
    Button searchBtn;
    @BindView(R.id.fab_scan)
    FloatingActionButton fabScan;

    ArrayList<String> searchOption;
    ArrayAdapter<String> searchOptionAdapter;
    private IntentIntegrator intentIntegrator;
    private View rootView;

    public SearchFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_search);
        item.setVisible(false);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.search_layout, container, false);
        ButterKnife.bind(this, rootView);

        if (getActivity() != null) {

            getActivity().setTitle(getResources().getString(R.string.setting_search));

        }

        searchOptionType();
        searchOptionAdapter = new ArrayAdapter<String>(getContext(), R.layout.spinner_item, R.id.spinner_item_text, searchOption);
        searchTypeSpinner.setAdapter(searchOptionAdapter);

        intentIntegrator = IntentIntegrator.forSupportFragment(this);
        intentIntegrator.setCaptureLayout(R.layout.barcode_scanner);
        intentIntegrator.setResultDisplayDuration(0);

        searchTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if (i == 0)
                    searchNumber.setHint(getActivity().getResources().getString(R.string.enter_ecr_number));
                else if (i == 1)
                    searchNumber.setHint("ENTER PHONE NUMBER");
                else if (i == 2)
                    searchNumber.setHint("ENTER PRODUCT ID");

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        return rootView;
    }

    public void searchOptionType() {

        searchOption = new ArrayList<>();
        searchOption.add(getActivity().getResources().getString(R.string.ecr_number));
        searchOption.add("Phone Number");
        searchOption.add("Product ID");

    }

    @OnClick({R.id.search_btn, R.id.fab_scan})
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.search_btn: {

                searchBtn.setVisibility(View.GONE);

                ArrayList<String> temp = new ArrayList<>();
                Gson gson = new Gson();

                if (!searchNumber.getText().toString().equals("")) {

                    if (searchTypeSpinner.getSelectedItemPosition() == 0) {

                        temp.clear();
                        temp.add(searchNumber.getText().toString());

                        AgentConsignments agentConsignments = new AgentConsignments(temp);
                        searchData(gson.toJson(agentConsignments));

                    } else if (searchTypeSpinner.getSelectedItemPosition() == 1) {

                        temp.clear();
                        temp.add(searchNumber.getText().toString());

                        AgentConsignmentsMobile agentConsignments = new AgentConsignmentsMobile(temp);
                        searchData(gson.toJson(agentConsignments));

                    } else if (searchTypeSpinner.getSelectedItemPosition() == 2) {

                        temp.clear();
                        temp.add(searchNumber.getText().toString());

                        AgentConsignmentsProductID agentConsignments = new AgentConsignmentsProductID(temp);
                        searchData(gson.toJson(agentConsignments));
                    }

                } else {
                    Toast.makeText(getContext(), "Enter a number", Toast.LENGTH_SHORT).show();
                }

                break;
            }

            case R.id.fab_scan: {

                intentIntegrator.addExtra("PROMPT_MESSAGE", "Scan the product");
                intentIntegrator.initiateScan();

                break;
            }

        }

    }

    private void searchData(final String data) {

        final ProgressDialog dialog = ProgressDialog.show(getActivity(), "",
                getActivity().getResources().getString(R.string.loading), true);

        dialog.show();

        String url = null;
        RequestQueue queue = null;
        // Instantiate the RequestQueue.
        if (getContext() != null) {
            queue = Volley.newRequestQueue(getContext());
            if (((MainActivity) getContext()).userGroup.equals(API.VALUE_USER_GROUP_MANAGER))
                url = API.baseUrl + API.managerParcelDetail;
            else
                url = API.baseUrl + API.agentParcelDetail;
        }

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        searchBtn.setVisibility(View.VISIBLE);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String code = jsonObject.getString(API.KEY_CODE);

                            if (code.equals(API.VALUE_CODE_SUCCESS)) {

                                Bundle bundle = new Bundle();
                                bundle.putString(API.KEY_JSON_SEARCH_PAYLOAD, data);

                                if (getActivity() != null) {
                                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                    if (imm != null)
                                        imm.hideSoftInputFromWindow(rootView.getWindowToken(), 0);

                                    if (searchTypeSpinner.getSelectedItemPosition() == 0) {

                                        SearchDetailFragment searchDetailFragment = new SearchDetailFragment();
                                        searchDetailFragment.setArguments(bundle);

                                        ((MainActivity) getActivity()).getSupportFragmentManager()
                                                .beginTransaction()
                                                .replace(R.id.content_main, searchDetailFragment)
                                                .addToBackStack(null)
                                                .commit();

                                    } else {

                                        JSONArray jsonArray = jsonObject.getJSONArray(API.KEY_JSON_DATA);

                                        if (jsonArray.length() > 0) {

                                            SearchDetailMultipleFragment searchDetailFragment = new SearchDetailMultipleFragment();
                                            searchDetailFragment.setArguments(bundle);

                                            ((MainActivity) getActivity()).getSupportFragmentManager()
                                                    .beginTransaction()
                                                    .replace(R.id.content_main, searchDetailFragment)
                                                    .addToBackStack(null)
                                                    .commit();

                                        } else {

                                            Toast.makeText(getActivity(), "No data found", Toast.LENGTH_SHORT).show();

                                        }

                                    }

                                }

                            } else {

                                JSONObject error = jsonObject.getJSONObject(API.KEY_JSON_ERROR);

                                if (code.equals(API.VALUE_CODE_AUTHENTICATION_ERROR)) {

                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                } else if (code.equals(API.VALUE_CODE_PARAMETER_ERROR)) {
                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                searchBtn.setVisibility(View.VISIBLE);
                try {

                    if (getContext() != null) {

                        if (error.getMessage().equals("com.android.volley.TimeoutError"))
                            Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_timeout), Toast.LENGTH_SHORT).show();

                        Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_unreachable), Toast.LENGTH_SHORT).show();
                    }

                    Log.v("Volly error : ", "" + error);

                } catch (Exception e) {

                    Log.v("Search E : ", " " + e);

                }
            }

        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                if (getContext() != null) {
                    params.put(API.KEY_JSON_USER_AUTHENTICATION, ((MainActivity) getContext()).authenticationKey);
                    params.put(API.KEY_JSON_USER_GROUP, ((MainActivity) getContext()).userGroup);
                    params.put(API.KEY_USER_ID, ((MainActivity) getContext()).userID);
                    params.put(API.KEY_CONTENT_TYPE, API.VALUE_CONTENT_VALUE_JSON);
                }

                return params;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {

                String your_string_json = data; // put your json
                return your_string_json.getBytes();
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                Config.volley_time_out,
                Config.max_retries,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Add the request to the RequestQueue.
        if (queue != null)
            queue.add(stringRequest);

    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {

        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (scanResult != null) {

            if (resultCode == RESULT_OK) {
                // contents contains whatever the code was
                String contents = scanResult.getContents();

                // Format contains the type of code i.e. UPC, EAN, QRCode etc...
                //String format = intent.getStringExtra("SCAN_RESULT_FORMAT");

                // Handle successful scan. In this example add contents to ArrayList
                if (contents != null) {

                    ArrayList<String> temp = new ArrayList<>();
                    Gson gson = new Gson();

                    temp.clear();
                    temp.add(contents);

                    AgentConsignments agentConsignments = new AgentConsignments(temp);
                    searchData(gson.toJson(agentConsignments));

                }

            } else if (resultCode == RESULT_CANCELED) {


            }


        } else {
            Toast toast = Toast.makeText(getContext(),
                    "No scan data received!", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

}
