package bd.com.ecourier.ecourier.View;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CalendarView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bd.com.ecourier.ecourier.API;
import bd.com.ecourier.ecourier.Config;
import bd.com.ecourier.ecourier.Model.Agent;
import bd.com.ecourier.ecourier.Presenter.ReportAdapter;
import bd.com.ecourier.ecourier.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by User on 10-Dec-17.
 */

public class ReportFragment extends Fragment {

    @BindView(R.id.start_date_text)
    TextView startDateTextView;
    @BindView(R.id.end_date_text)
    TextView endDateTextView;
    @BindView(R.id.date_picker_report)
    CalendarView calendarView;
    @BindView(R.id.recyclerView_list)
    RecyclerView recyclerView;
    @BindView(R.id.start_date_show)
    TextView startDateShowTextView;
    @BindView(R.id.end_date_show)
    TextView endDateShowTextView;
    @BindView(R.id.list_spinner)
    Spinner searchTypeSpinner;

    private View rootView;
    private ArrayList<Agent> agentArrayList = new ArrayList<>();
    private ReportAdapter reportAdapter;
    private boolean startDateSelected = false, endDateSelected = false;
    private String startDate = "empty";
    private String endDate = "empty";
    private ArrayList<String> statusName;
    private ArrayList<String> statusCode;
    private ArrayList<String> statusType;
    private ArrayAdapter<String> searchOptionAdapter;

    public ReportFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.report_layout, container, false);
        ButterKnife.bind(this, rootView);
        String today = null;

        calendarView.setVisibility(View.GONE);
        startDateShowTextView.setText(today);

        searchOptionType();
        searchOptionAdapter = new ArrayAdapter<String>(getContext(), R.layout.spinner_item, R.id.spinner_item_text, statusName);
        searchTypeSpinner.setAdapter(searchOptionAdapter);

        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView calendarView, int year, int month, int day) {

                calendarView.setVisibility(View.GONE);
                if (startDateSelected) {
                    startDate = year + "-" + (month + 1) + "-" + day;
                    startDateShowTextView.setText(startDate);
                    fetchData();
                }
                if (endDateSelected) {
                    endDate = year + "-" + (month + 1) + "-" + day;
                    startDateShowTextView.setText("");
                    endDateShowTextView.setText(endDate);
                    fetchData();
                }

            }
        });

        showFeed(agentArrayList);

        if (getContext() != null) {
            today = getActivity().getResources().getString(R.string.today);
            getActivity().setTitle(getResources().getString(R.string.dashboard_report));

            if (((MainActivity) getContext()).connectedTOInternet)
                fetchData();
        }

        searchTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                fetchData();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        return rootView;
    }

    public void searchOptionType() {

        statusName = new ArrayList<>();
        statusCode = new ArrayList<>();
        statusType = new ArrayList<>();

        statusName.add("Delivered");
        statusCode.add("S4");
        statusType.add("delivery");

        statusName.add("Partial Delivered");
        statusCode.add("S5");
        statusType.add("delivery");

        statusName.add("Hold at Hub");
        statusCode.add("S7");
        statusType.add("delivery");

        statusName.add("Cancelled");
        statusCode.add("S12");
        statusType.add("delivery");

        statusName.add("Sent to Destination Hub");
        statusCode.add("S14");
        statusType.add("delivery");

        statusName.add("In Destination Hub");
        statusCode.add("S15");
        statusType.add("delivery");

        statusName.add("On the way to Delivery");
        statusCode.add("S21");
        statusType.add("delivery");

        statusName.add("Returned to RTN");
        statusCode.add("S22");
        statusType.add("delivery");

        statusName.add("Delivery Agent Assigned");
        statusCode.add("S31");
        statusType.add("delivery");

        statusName.add("Hold by Agent");
        statusCode.add("S71");
        statusType.add("delivery");

        statusName.add("Cancel Requested");
        statusCode.add("S41");
        statusType.add("delivery");

        statusName.add("Received at RTN");
        statusCode.add("S51");
        statusType.add("delivery");

        statusName.add("Hold at Hub (Wrong Routing)");
        statusCode.add("S81");
        statusType.add("delivery");

        statusName.add("Sent to Fulfillment (Wrong Routing)");
        statusCode.add("S82");
        statusType.add("delivery");


        statusName.add("In Destination Hub (Return)");
        statusCode.add("S23");
        statusType.add("return");

        statusName.add("Returned to Merchant");
        statusCode.add("S24");
        statusType.add("return");

        statusName.add("On the way to Return");
        statusCode.add("S25");
        statusType.add("return");

        statusName.add("Sent to Destination Hub (Return)");
        statusCode.add("S52");
        statusType.add("return");

        statusName.add("Delivery Agent Assigned (Return)");
        statusCode.add("S53");
        statusType.add("return");

        statusName.add("Hold by Agent (Return)");
        statusCode.add("S54");
        statusType.add("return");

        statusName.add("Hold at Hub (Return)");
        statusCode.add("S55");
        statusType.add("return");

    }

    @OnClick({R.id.start_date_text, R.id.end_date_text})
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.start_date_text: {

                calendarView.setVisibility(View.VISIBLE);
                startDateSelected = true;
                endDateSelected = false;

                break;
            }

            case R.id.end_date_text: {

                calendarView.setVisibility(View.VISIBLE);
                startDateSelected = false;
                endDateSelected = true;

                break;
            }

        }

    }

    private void showFeed(ArrayList<Agent> agentArrayList) {

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        reportAdapter = new ReportAdapter(getActivity(), agentArrayList, Config.TAG_SEARCH_STATUS_FRAGMENT);
        recyclerView.setAdapter(reportAdapter);

    }

    private void fetchData() {

        RequestQueue queue = null;
        // Instantiate the RequestQueue.
        if (getContext() != null)
            queue = Volley.newRequestQueue(getContext());
        String url = API.baseUrl + API.managerReportSummery;

        /*if (startDateSelected && !endDateSelected)
            url = API.baseUrl + API.managerReportSummeryStartDate + startDate;

        if (!startDateSelected && endDateSelected)
            url = API.baseUrl + API.managerReportSummeryStartDate + endDate;

        if (!startDate.equals("empty") && !endDate.equals("empty")) {
            url = API.baseUrl + API.managerReportSummeryStartDate + startDate + API.managerReportSummeryEndDate + endDate;
            String temp = startDate + " / " + endDate;
            startDateShowTextView.setText("");
            endDateShowTextView.setText("");
            startDateShowTextView.setText(temp);

        }*/

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("search", "onResponse: " + response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String code = jsonObject.getString(API.KEY_CODE);

                            if (code.equals(API.VALUE_CODE_SUCCESS)) {

                                reportAdapter.listUpdated(dataFromServer(jsonObject));

                            } else {

                                JSONObject error = jsonObject.getJSONObject(API.KEY_JSON_ERROR);

                                if (code.equals(API.VALUE_CODE_AUTHENTICATION_ERROR)) {

                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                } else if (code.equals(API.VALUE_CODE_PARAMETER_ERROR)) {
                                    Toast.makeText(getContext(), "Error : " + error.getString(API.KEY_JSON_MESSAGE), Toast.LENGTH_LONG).show();
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                try {

                    if (getContext() != null) {

                        if (error.getMessage().equals("com.android.volley.TimeoutError"))
                            Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_timeout), Toast.LENGTH_SHORT).show();

                        Toast.makeText(getContext(), getContext().getResources().getString(R.string.server_unreachable), Toast.LENGTH_SHORT).show();
                    }

                    Log.v("Volly error : ", "" + error);

                } catch (Exception e) {

                    Log.v("Search detail E : ", " " + e);

                }
            }

        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                if (getContext() != null) {
                    params.put(API.KEY_JSON_USER_AUTHENTICATION, ((MainActivity) getContext()).authenticationKey);
                    params.put(API.KEY_JSON_USER_GROUP, ((MainActivity) getContext()).userGroup);
                    params.put(API.KEY_USER_ID, ((MainActivity) getContext()).userID);
                    params.put(API.KEY_CONTENT_TYPE, API.VALUE_CONTENT_VALUE);
                }

                return params;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put(API.managerReportSummeryType, statusType.get(searchTypeSpinner.getSelectedItemPosition()));
                params.put(API.managerReportSummeryStatus, statusCode.get(searchTypeSpinner.getSelectedItemPosition()));

                Log.d("search", "status type: " + statusType.get(searchTypeSpinner.getSelectedItemPosition()));
                Log.d("search", "status code: " + statusCode.get(searchTypeSpinner.getSelectedItemPosition()));

                if (startDateSelected && !endDateSelected) {

                    params.put(API.managerReportSummeryStartDate, startDate);
                    params.put(API.managerReportSummeryType, statusType.get(searchTypeSpinner.getSelectedItemPosition()));
                    params.put(API.managerReportSummeryStatus, statusCode.get(searchTypeSpinner.getSelectedItemPosition()));

                }

                if (!startDateSelected && endDateSelected) {

                    params.put(API.managerReportSummeryEndDate, endDate);
                    params.put(API.managerReportSummeryType, statusType.get(searchTypeSpinner.getSelectedItemPosition()));
                    params.put(API.managerReportSummeryStatus, statusCode.get(searchTypeSpinner.getSelectedItemPosition()));

                }

                if (!startDate.equals("empty") && !endDate.equals("empty")) {

                    params.put(API.managerReportSummeryStartDate, startDate);
                    params.put(API.managerReportSummeryEndDate, endDate);
                    params.put(API.managerReportSummeryType, statusType.get(searchTypeSpinner.getSelectedItemPosition()));
                    params.put(API.managerReportSummeryStatus, statusCode.get(searchTypeSpinner.getSelectedItemPosition()));

                    String temp = startDate + " / " + endDate;
                    startDateShowTextView.setText("");
                    endDateShowTextView.setText("");
                    startDateShowTextView.setText(temp);

                }

                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                Config.volley_time_out,
                Config.max_retries,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Add the request to the RequestQueue.
        if (queue != null)
            queue.add(stringRequest);

    }

    private Agent[] dataFromServer(JSONObject jsonObject) throws JSONException {

        JSONArray jsonArray = jsonObject.getJSONArray(API.KEY_JSON_DATA);

        Agent[] agents = new Agent[jsonArray.length()];
        ArrayList<String> consignmentList = new ArrayList<>();

        for (int i = 0; i < jsonArray.length(); i++) {

            JSONObject agentData = jsonArray.getJSONObject(i);

            agents[i] = new Agent(agentData.getString(API.KEY_AGENT_DETAIL_AGENT_NAME), agentData.getString(API.KEY_AGENT_DETAIL_AGENT_ID), agentData.getString(API.KEY_AGENT_DETAIL_AGENT_MOBILE_WITH_UNDERSCORE), agentData.getString(API.KEY_TOTAL_PARCEL), consignmentList, agentData.getString(API.KEY_MANAGER_RECEIVE_CASH_TOTAL_PRODUCT_PRICE), agentData.getString(API.KEY_MANAGER_RECEIVE_CASH_TOTAL_COLLECTED_AMOUNT));

        }

        return agents;
    }

}
