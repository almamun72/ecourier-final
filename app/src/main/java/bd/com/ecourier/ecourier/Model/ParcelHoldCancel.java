package bd.com.ecourier.ecourier.Model;

import java.util.ArrayList;

/**
 * Created by User on 26-Dec-17.
 */

public class ParcelHoldCancel {

    private String status;
    private String comments;
    private ArrayList<String> consignment_no;

    public ParcelHoldCancel(ArrayList<String> consignment_no, String status, String comments) {

        this.consignment_no = consignment_no;
        this.status = status;
        this.comments = comments;

    }

    public ArrayList<String> getConsignment_no() {
        return consignment_no;
    }

    public void setConsignment_no(ArrayList<String> consignment_no) {
        this.consignment_no = consignment_no;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
